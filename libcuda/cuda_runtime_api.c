
#if !defined(__CUDA_RUNTIME_API_H__)
#define __CUDA_RUNTIME_API_H__

/** CUDA Runtime API Version */
#define CUDART_VERSION  5000
#include <config.h>
#include <db.h>
#include <warp.h>
#include <host_defines.h>
#include <builtin_types.h>
#include <cuda_device_runtime_api.h>
#include <heading.h>
#include <register_file.h>
#include <virtual.h>
#include <unistd.h>
#include <iostream>
#include <fstream>
#include <sstream>
#include <algorithm>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
using namespace std;

dbout debug;

int yyparse();
int yy_scan_string(const char *); 
extern int yylineno;

/* jasterplus add global variables */
vector< config_args > args_stack;
std::vector<std::string> fatbin_map;
std::map<int64_t*,std::string> function_address_map;
vector< instruction > *ir_buff;
vector<uint64_t> *local_buff = NULL; //
vector< string > function_name;
map<string, int > label_name;
map<string,int> param_map;
map<string,int> local_map; //
map<string, vector< instruction >*> function_map;
dim3 args_grid_dim, args_block_dim;
reg_alloc *kernel_reg;
map<string, reg_alloc*> allocation_map;
map<string, vector<uint64_t>* > local_alloc_map; //
vector<void*> address_map;
printResolver warp::resolve;

/*funcptr_r rno::get_reg_index = NULL;
funcptr_t1 rno::get_type1 = NULL;
funcptr_t2 rno::get_type2 = NULL;
instruction* rno::is = NULL;*/

/* TODO special register */
/* TODO register allocation */
/* TODO memory address   */

/** \cond impl_private */
#if !defined(__dv)
#if defined(__cplusplus)
#define __dv(v) \
            = v
#else /* __cplusplus */
#define __dv(v)
#endif /* __cplusplus */
#endif /* !__dv */

int current_device = 0;
virtual_gpu devices[config::device_num];

extern "C" {

/* /usr/local/cuda-5.0/include/crt/host_runtime.h should export cuda-5.0 for compiler*/

void** CUDARTAPI __cudaRegisterFatBinary( void *fatCubin )
{
    debug << "__cudaRegisterFatBinary\n";

    static unsigned fatbin_count = 1;
    static bool first_call = true;

    /* import gpgpusim code to find the filename */
    /* this part is future work for management GPU device */
    typedef struct {int m; int v; const unsigned long long* d; char* f;} __fatDeviceText __attribute__ ((aligned (8)));
    __fatDeviceText * fatDeviceText = (__fatDeviceText *) fatCubin;
    char *pfatbin = (char*) fatDeviceText->d;
    int offset = *((int*)(pfatbin+48));
    char *filename = (pfatbin+16+offset);

    while(fatbin_map.size() <= fatbin_count)
        fatbin_map.push_back("");
    fatbin_map[fatbin_count++] = string(filename);
    /* this part is future work for management GPU device */

    /* Debug */
    for(int i=0;i<fatbin_map.size();i++)
        debug << fatbin_map[i] << "\n";

    /* import gpgpusim code to get the runtime binary*/
    if(first_call){

        //devices.resize(config::device_num);

        char self_exe_path[1025];
        std::stringstream exec_link;
        exec_link << "/proc/self/exe";

        ssize_t path_length = readlink(exec_link.str().c_str(), self_exe_path, 1024); 
        self_exe_path[path_length] = '\0'; 

        char cmd[1024];
        int found;
        sprintf(cmd,"$CUDA_INSTALL_PATH/bin/cuobjdump -ptx %s > _cuobjdump.ptx",self_exe_path);
        system(cmd);

        fstream fin,fout;
        string line,ptx_str="";
        /* start to extract ptx from execution binary */
        fin.open("_cuobjdump.ptx",ios::in);
        while(getline(fin,line)) {
            if(line.compare("Fatbin ptx code:") == 0) {
                /* TODO : setup the simulator config for sm_version */
                getline(fin,line); getline(fin,line);
                found=-1;
                if(line.compare("arch = sm_10") == 0) {
                    while(found == -1){ 
                        getline(fin,line);
                        found = line.find("identifier");
                    }
                    debug << line.substr(line.find_first_of("=")+2)+".ptx\n";
                    fout.open(string("_"+line.substr(line.find_first_of("=")+2)+".ptx").c_str(),ios::out);
                    while(getline(fin,line)) {
                        if(line.compare("Fatbin elf code:") == 0) break;
                        // TODO: How to handle these?
                        // Debug info is necessary
                        if(line.substr(0, 12).compare("ptxasOptions") == 0) continue;
                        if(line.substr(0, 7).compare("@@DWARF") == 0) continue;
                        fout << line << "\n";
                        ptx_str += line + "\n" ;
                    }
                    fout.close();
                }
            }
        }
        fin.close();
        /* start to parse ptx codes */
        yylineno = 1;
        yy_scan_string(ptx_str.c_str());
        yyparse();

        /* Debug */
        for(int k=0;k<function_name.size();k++){
            debug << function_name[k] << " instruction:\n";
            debug << "rh<" << allocation_map.find(function_name[k])->second->rh_size << ">, " ;
            debug << "r<" << allocation_map.find(function_name[k])->second->r_size << ">, " ;
            debug << "rd<" << allocation_map.find(function_name[k])->second->rd_size << ">, " ;
            debug << "f<" << allocation_map.find(function_name[k])->second->f_size << ">, " ;
            debug << "p<" << allocation_map.find(function_name[k])->second->p_size << "> \n";
            for(int i=0; i<function_map.find(function_name[k])->second->size(); i++){
                debug << setw(3) << i << ": opcode = " << setw(3) << function_map.find(function_name[k])->second->at(i).get_opcode() ;
                debug << ", type1 = " << setw(5) << function_map.find(function_name[k])->second->at(i).get_type1() ;
                debug << ", type2 = " << setw(5) << function_map.find(function_name[k])->second->at(i).get_type2() ; 
                debug << ", state_space = " << setw(5) << function_map.find(function_name[k])->second->at(i).get_state_space() ;
                debug << ", mul_type = " << setw(5) << function_map.find(function_name[k])->second->at(i).get_mul_type();
                debug << ", reg: " << setw(4)<< function_map.find(function_name[k])->second->at(i).get_reg_no(0) << " " 
                    << setw(4)<< function_map.find(function_name[k])->second->at(i).get_reg_no(1) << " "
                    << setw(4)<< function_map.find(function_name[k])->second->at(i).get_reg_no(2) << " "
                    << setw(4)<< function_map.find(function_name[k])->second->at(i).get_reg_no(3) << " " 
                    << " is_p: " << setw(2)<< function_map.find(function_name[k])->second->at(i).get_p_reg();
                debug << ", spec_reg = " << setw(4)<< function_map.find(function_name[k])->second->at(i).get_special_reg();
                debug << ", label = " << function_map.find(function_name[k])->second->at(i).get_label() << "\n";
                debug << "\t address_reg: " << function_map.find(function_name[k])->second->at(i).get_addr_reg();
                debug << ", offset+ " << function_map.find(function_name[k])->second->at(i).get_addr_offset();
                debug << ", address_label: " << function_map.find(function_name[k])->second->at(i).get_addr_label() << "\n\n";
            }
            debug << "\n\n";
        }

        debug << "\n===== label ======\n";
        map<string, int>::iterator it;
        for(it = label_name.begin() ; it != label_name.end() ; it++){
            debug << it->first << " " << it->second << "\n";            
        }

        debug << "\n===== param =====\n";
        for(it = param_map.begin() ; it != param_map.end() ; it++){
            debug << it->first << " " << it->second << "\n";
        }

        /* finish */
        first_call = false;
        debug << "Cuobjdump Done!\n";
    }
    return (void**) (fatbin_count-1);
}

void CUDARTAPI __cudaRegisterFunction(
        void   **fatCubinHandle,
        const char    *hostFun,
        char    *deviceFun,
        const char    *deviceName,
        int      thread_limit,
        uint3   *tid,
        uint3   *bid,
        dim3    *bDim,
        dim3    *gDim
)
{
    /* fatCubinHandle define this function belong to which file, (fatCubinHandle)
     * deviceFun = deviceName, (string) 
     * hostFun is the address of this function, (long long int*) */
    debug << "__cudaRegisterFunction\n";

    function_address_map[(int64_t*) hostFun] = string(deviceFun);

}

void __cudaUnregisterFatBinary(void **fatCubinHandle)
{
    debug << "__cudaUnregisterFatBinary\n";
    //devices.clear();
    //devices.resize(0);
    /* we don't need to unregister the device, it will reset by program termination. */
}

extern void __cudaRegisterVar(
        void **fatCubinHandle,
        char *hostVar,
        char *deviceAddress,
        const char *deviceName,
        int ext,
        int size,
        int constant,
        int global )
{
    debug << "__cudaRegisterVar\n";
}


__host__ cudaError_t CUDARTAPI cudaDeviceReset(void)
{
	return cudaSuccess;
}

__host__ __cudart_builtin__ cudaError_t CUDARTAPI cudaDeviceSynchronize(void)
{
	return cudaSuccess;
}

__host__ cudaError_t CUDARTAPI cudaDeviceSetLimit(enum cudaLimit limit, size_t value)
{
	return cudaSuccess;
}

__host__ __cudart_builtin__ cudaError_t CUDARTAPI cudaDeviceGetLimit(size_t *pValue, enum cudaLimit limit)
{
	return cudaSuccess;
}

__host__ __cudart_builtin__ cudaError_t CUDARTAPI cudaDeviceGetCacheConfig(enum cudaFuncCache *pCacheConfig)
{
	return cudaSuccess;
}

__host__ cudaError_t CUDARTAPI cudaDeviceSetCacheConfig(enum cudaFuncCache cacheConfig)
{
	return cudaSuccess;
}

__host__ __cudart_builtin__ cudaError_t CUDARTAPI cudaDeviceGetSharedMemConfig(enum cudaSharedMemConfig *pConfig)
{
	return cudaSuccess;
}

__host__ cudaError_t CUDARTAPI cudaDeviceSetSharedMemConfig(enum cudaSharedMemConfig config)
{
	return cudaSuccess;
}

__host__ cudaError_t CUDARTAPI cudaDeviceGetByPCIBusId(int *device, char *pciBusId)
{
	return cudaSuccess;
}

__host__ cudaError_t CUDARTAPI cudaDeviceGetPCIBusId(char *pciBusId, int len, int device)
{
	return cudaSuccess;
}

__host__ cudaError_t CUDARTAPI cudaIpcGetEventHandle(cudaIpcEventHandle_t *handle, cudaEvent_t event)
{
	return cudaSuccess;
}

__host__ cudaError_t CUDARTAPI cudaIpcOpenEventHandle(cudaEvent_t *event, cudaIpcEventHandle_t handle)
{
	return cudaSuccess;
}

__host__ cudaError_t CUDARTAPI cudaIpcGetMemHandle(cudaIpcMemHandle_t *handle, void *devPtr)
{
	return cudaSuccess;
}

__host__ cudaError_t CUDARTAPI cudaIpcOpenMemHandle(void **devPtr, cudaIpcMemHandle_t handle, unsigned int flags)
{
	return cudaSuccess;
}

__host__ cudaError_t CUDARTAPI cudaIpcCloseMemHandle(void *devPtr)
{
	return cudaSuccess;
}

__host__ cudaError_t CUDARTAPI cudaThreadExit(void)
{
	return cudaSuccess;
}

__host__ cudaError_t CUDARTAPI cudaThreadSynchronize(void)
{
	return cudaSuccess;
}

__host__ cudaError_t CUDARTAPI cudaThreadSetLimit(enum cudaLimit limit, size_t value)
{
	return cudaSuccess;
}

__host__ cudaError_t CUDARTAPI cudaThreadGetLimit(size_t *pValue, enum cudaLimit limit)
{
	return cudaSuccess;
}

__host__ cudaError_t CUDARTAPI cudaThreadGetCacheConfig(enum cudaFuncCache *pCacheConfig)
{
	return cudaSuccess;
}

__host__ cudaError_t CUDARTAPI cudaThreadSetCacheConfig(enum cudaFuncCache cacheConfig)
{
	return cudaSuccess;
}

__host__ __cudart_builtin__ cudaError_t CUDARTAPI cudaGetLastError(void)
{
	return cudaSuccess;
}

__host__ __cudart_builtin__ cudaError_t CUDARTAPI cudaPeekAtLastError(void)
{
	return cudaSuccess;
}

__host__ __cudart_builtin__ const char* CUDARTAPI cudaGetErrorString(cudaError_t error)
{
	return "";
}

__host__ __cudart_builtin__ cudaError_t CUDARTAPI cudaGetDeviceCount(int *count)
{
    *count = config::device_num;
    if (*count == 0)
	return cudaErrorNoDevice;
    return cudaSuccess;
}

__host__ __cudart_builtin__ cudaError_t CUDARTAPI cudaGetDeviceProperties(struct cudaDeviceProp *prop, int device)
{
	return cudaSuccess;
}

__host__ __cudart_builtin__ cudaError_t CUDARTAPI cudaDeviceGetAttribute(int *value, enum cudaDeviceAttr attr, int device)
{
	return cudaSuccess;
}

__host__ cudaError_t CUDARTAPI cudaChooseDevice(int *device, const struct cudaDeviceProp *prop)
{
	return cudaSuccess;
}

__host__ cudaError_t CUDARTAPI cudaSetDevice(int device)
{
    if (device < config::device_num) {
        if (devices[device].get_status()) {
            current_device = device;
            return cudaSuccess;
        }
        return cudaErrorDeviceAlreadyInUse;
    }
    return cudaErrorInvalidDevice;
}

__host__ __cudart_builtin__ cudaError_t CUDARTAPI cudaGetDevice(int *device)
{
    *device = current_device;
    return cudaSuccess;
}

__host__ cudaError_t CUDARTAPI cudaSetValidDevices(int *device_arr, int len)
{
	return cudaSuccess;
}

__host__ cudaError_t CUDARTAPI cudaSetDeviceFlags( unsigned int flags )
{
	return cudaSuccess;
}

__host__ cudaError_t CUDARTAPI cudaStreamCreate(cudaStream_t *pStream)
{
	return cudaSuccess;
}

__host__ __cudart_builtin__ cudaError_t CUDARTAPI cudaStreamCreateWithFlags(cudaStream_t *pStream, unsigned int flags)
{
	return cudaSuccess;
}

__host__ __cudart_builtin__ cudaError_t CUDARTAPI cudaStreamDestroy(cudaStream_t stream)
{
	return cudaSuccess;
}

__host__ __cudart_builtin__ cudaError_t CUDARTAPI cudaStreamWaitEvent(cudaStream_t stream, cudaEvent_t event, unsigned int flags)
{
	return cudaSuccess;
}
/*
__host__ cudaError_t CUDARTAPI cudaStreamAddCallback(cudaStream_t stream, cudaStreamCallback_t callback, void *userData, unsigned int flags)
{
	return cudaSuccess;
}
*/
__host__ cudaError_t CUDARTAPI cudaStreamSynchronize(cudaStream_t stream)
{
	return cudaSuccess;
}

__host__ cudaError_t CUDARTAPI cudaStreamQuery(cudaStream_t stream)
{
	return cudaSuccess;
}

__host__ cudaError_t CUDARTAPI cudaEventCreate(cudaEvent_t *event)
{
	return cudaSuccess;
}

__host__ __cudart_builtin__ cudaError_t CUDARTAPI cudaEventCreateWithFlags(cudaEvent_t *event, unsigned int flags)
{
	return cudaSuccess;
}

__host__ __cudart_builtin__ cudaError_t CUDARTAPI cudaEventRecord(cudaEvent_t event, cudaStream_t stream __dv(0))
{
	return cudaSuccess;
}

__host__ cudaError_t CUDARTAPI cudaEventQuery(cudaEvent_t event)
{
	return cudaSuccess;
}

__host__ cudaError_t CUDARTAPI cudaEventSynchronize(cudaEvent_t event)
{
	return cudaSuccess;
}

__host__ __cudart_builtin__ cudaError_t CUDARTAPI cudaEventDestroy(cudaEvent_t event)
{
	return cudaSuccess;
}

__host__ cudaError_t CUDARTAPI cudaEventElapsedTime(float *ms, cudaEvent_t start, cudaEvent_t end)
{
	return cudaSuccess;
}

__host__ cudaError_t CUDARTAPI cudaConfigureCall(dim3 gridDim, dim3 blockDim, size_t sharedMem __dv(0), cudaStream_t stream __dv(0))
{
    debug << "cudaConfigureCall\n";

    /* TODO : save the configuration args to arch/kernel structure */
    /* This is temp info storing in heading structure */
    args_grid_dim = gridDim;
    args_grid_dim.x = (args_grid_dim.x == 0) ? 1 : args_grid_dim.x;
    args_grid_dim.y = (args_grid_dim.y == 0) ? 1 : args_grid_dim.y;
    args_grid_dim.z = (args_grid_dim.z == 0) ? 1 : args_grid_dim.z;
    args_block_dim = blockDim;
    args_block_dim.x = (args_block_dim.x == 0) ? 1 : args_block_dim.x;
    args_block_dim.y = (args_block_dim.y == 0) ? 1 : args_block_dim.y;
    args_block_dim.z = (args_block_dim.z == 0) ? 1 : args_block_dim.z;
    virtual_gpu* gpu = &devices[current_device];
    gpu->create_kernel(args_grid_dim, args_block_dim, sharedMem);
	return cudaSuccess;
}

__host__ cudaError_t CUDARTAPI cudaSetupArgument(const void *arg, size_t size, size_t offset)
{
    debug << "cudaSetupArgument\n";
    args_stack.push_back( config_args(arg,size,offset) );
    debug << "Debug: 0x" << hex << *((int64_t*)args_stack[args_stack.size()-1].arg) << "\n";
    debug << "size = " << size << "\n";
    /* TODO : link the args with PTX parameter */
	return cudaSuccess;
}

__host__ cudaError_t CUDARTAPI cudaFuncSetCacheConfig(const void *func, enum cudaFuncCache cacheConfig)
{
	return cudaSuccess;
}

__host__ cudaError_t CUDARTAPI cudaFuncSetSharedMemConfig(const void *func, enum cudaSharedMemConfig config)
{
	return cudaSuccess;
}

__host__ cudaError_t CUDARTAPI cudaLaunch(const void *func)
{
    debug << "cudaLaunch\n";
    debug << "exection function: " << function_address_map.find((int64_t*)func)->second << "\n";
    debug << " " << function_map.find(function_address_map.find((int64_t*)func)->second)->second->size() << "\n";

    /* TODO: create thread to execute simulator */
    virtual_gpu* gpu = &devices[current_device];
    
    gpu->launch_kernel(func);
    
    while(args_stack.size()>0)
        args_stack.pop_back();
    /*
    map<long long int*, std::string>::iterator it;
    for(it = function_map.begin() ; it != function_map.end() ; it++){
        debug << it->first << " " << it->second << "\n";    
    }
    */
	return cudaSuccess;
}

__host__ __cudart_builtin__ cudaError_t CUDARTAPI cudaFuncGetAttributes(struct cudaFuncAttributes *attr, const void *func)
{
	return cudaSuccess;
}

__host__ cudaError_t CUDARTAPI cudaSetDoubleForDevice(double *d)
{
	return cudaSuccess;
}

__host__ cudaError_t CUDARTAPI cudaSetDoubleForHost(double *d)
{
	return cudaSuccess;
}

/* global allocator : base(address) + size(offset) < total global memory limitation */
/* we don't need to consider run out of global memory                               */
/* debug << &ptr << " " << ptr << " " << *ptr << " " << *((int*)*ptr) << "\n"; */
/* stack address, pointer address, content address, content value */
__host__ __cudart_builtin__ cudaError_t CUDARTAPI cudaMalloc(void **devPtr, size_t size)
{
    debug << "cudaMalloc\n";
    *devPtr = (void*) malloc(size);
    address_map.push_back((void*)*devPtr);
    //memcpy((void*)&address_map[address_map.size()-1],devPtr,8);
    debug << "Debug: " << *devPtr << "\n";
    return cudaSuccess;
}

__host__ cudaError_t CUDARTAPI cudaMallocHost(void **ptr, size_t size)
{
    debug << "cudaMallocHost\n";
    *ptr = (void*) malloc(size);
    address_map.push_back((void*)*ptr);
    return cudaSuccess;
}

__host__ cudaError_t CUDARTAPI cudaMallocPitch(void **devPtr, size_t *pitch, size_t width, size_t height)
{
    return cudaSuccess;
}

__host__ cudaError_t CUDARTAPI cudaMallocArray(cudaArray_t *array, const struct cudaChannelFormatDesc *desc, size_t width, size_t height __dv(0), unsigned int flags __dv(0))
{
    return cudaSuccess;
}

__host__ __cudart_builtin__ cudaError_t CUDARTAPI cudaFree(void *devPtr)
{
    debug << "cudaFree\n";
    vector<void*>::iterator iter = find(address_map.begin(), address_map.end(), (void*)devPtr);
    if (iter != address_map.end())
        address_map.erase(iter);
    else 
        debug << "does not find array address\n";
    free(devPtr);
    return cudaSuccess;
}

__host__ cudaError_t CUDARTAPI cudaFreeHost(void *ptr)
{
    return cudaSuccess;
}

__host__ cudaError_t CUDARTAPI cudaFreeArray(cudaArray_t array)
{
    return cudaSuccess;
}

__host__ cudaError_t CUDARTAPI cudaFreeMipmappedArray(cudaMipmappedArray_t mipmappedArray)
{
    return cudaSuccess;
}

__host__ cudaError_t CUDARTAPI cudaHostAlloc(void **pHost, size_t size, unsigned int flags)
{
    return cudaSuccess;
}

__host__ cudaError_t CUDARTAPI cudaHostRegister(void *ptr, size_t size, unsigned int flags)
{
    return cudaSuccess;
}

__host__ cudaError_t CUDARTAPI cudaHostUnregister(void *ptr)
{
    return cudaSuccess;
}

__host__ cudaError_t CUDARTAPI cudaHostGetDevicePointer(void **pDevice, void *pHost, unsigned int flags)
{
    return cudaSuccess;
}

__host__ cudaError_t CUDARTAPI cudaHostGetFlags(unsigned int *pFlags, void *pHost)
{
    return cudaSuccess;
}

__host__ cudaError_t CUDARTAPI cudaMalloc3D(struct cudaPitchedPtr* pitchedDevPtr, struct cudaExtent extent)
{
    return cudaSuccess;
}

__host__ cudaError_t CUDARTAPI cudaMalloc3DArray(cudaArray_t *array, const struct cudaChannelFormatDesc* desc, struct cudaExtent extent, unsigned int flags __dv(0))
{
    return cudaSuccess;
}

__host__ cudaError_t CUDARTAPI cudaMallocMipmappedArray(cudaMipmappedArray_t *mipmappedArray, const struct cudaChannelFormatDesc* desc, struct cudaExtent extent, unsigned int numLevels, unsigned int flags __dv(0))
{
    return cudaSuccess;
}

__host__ cudaError_t CUDARTAPI cudaGetMipmappedArrayLevel(cudaArray_t *levelArray, cudaMipmappedArray_const_t mipmappedArray, unsigned int level)
{
    return cudaSuccess;
}

__host__ cudaError_t CUDARTAPI cudaMemcpy3D(const struct cudaMemcpy3DParms *p)
{
    return cudaSuccess;
}

__host__ cudaError_t CUDARTAPI cudaMemcpy3DPeer(const struct cudaMemcpy3DPeerParms *p)
{
    return cudaSuccess;
}

__host__ __cudart_builtin__ cudaError_t CUDARTAPI cudaMemcpy3DAsync(const struct cudaMemcpy3DParms *p, cudaStream_t stream __dv(0))
{
    return cudaSuccess;
}

__host__ cudaError_t CUDARTAPI cudaMemcpy3DPeerAsync(const struct cudaMemcpy3DPeerParms *p, cudaStream_t stream __dv(0))
{
    return cudaSuccess;
}

__host__ cudaError_t CUDARTAPI cudaMemGetInfo(size_t *free, size_t *total)
{
    return cudaSuccess;
}

__host__ cudaError_t CUDARTAPI cudaArrayGetInfo(struct cudaChannelFormatDesc *desc, struct cudaExtent *extent, unsigned int *flags, cudaArray_t array)
{
    return cudaSuccess;
}

/* Need to understand the meaning of cudaMemcpyKind */
__host__ cudaError_t CUDARTAPI cudaMemcpy(void *dst, const void *src, size_t count, enum cudaMemcpyKind kind)
{
    debug << "cudaMemcpy\n";

    for(int i=0; i<count; i++)
        *((char*)dst+i) = *((char*)src+i);

    return cudaSuccess;
}

__host__ cudaError_t CUDARTAPI cudaMemcpyPeer(void *dst, int dstDevice, const void *src, int srcDevice, size_t count)
{
    return cudaSuccess;
}

__host__ cudaError_t CUDARTAPI cudaMemcpyToArray(cudaArray_t dst, size_t wOffset, size_t hOffset, const void *src, size_t count, enum cudaMemcpyKind kind)
{
    return cudaSuccess;
}

__host__ cudaError_t CUDARTAPI cudaMemcpyFromArray(void *dst, cudaArray_const_t src, size_t wOffset, size_t hOffset, size_t count, enum cudaMemcpyKind kind)
{
    return cudaSuccess;
}

__host__ cudaError_t CUDARTAPI cudaMemcpyArrayToArray(cudaArray_t dst, size_t wOffsetDst, size_t hOffsetDst, cudaArray_const_t src, size_t wOffsetSrc, size_t hOffsetSrc, size_t count, enum cudaMemcpyKind kind __dv(cudaMemcpyDeviceToDevice))
{
    return cudaSuccess;
}

__host__ cudaError_t CUDARTAPI cudaMemcpy2D(void *dst, size_t dpitch, const void *src, size_t spitch, size_t width, size_t height, enum cudaMemcpyKind kind)
{
    return cudaSuccess;
}

__host__ cudaError_t CUDARTAPI cudaMemcpy2DToArray(cudaArray_t dst, size_t wOffset, size_t hOffset, const void *src, size_t spitch, size_t width, size_t height, enum cudaMemcpyKind kind)
{
    return cudaSuccess;
}

__host__ cudaError_t CUDARTAPI cudaMemcpy2DFromArray(void *dst, size_t dpitch, cudaArray_const_t src, size_t wOffset, size_t hOffset, size_t width, size_t height, enum cudaMemcpyKind kind)
{
    return cudaSuccess;
}

__host__ cudaError_t CUDARTAPI cudaMemcpy2DArrayToArray(cudaArray_t dst, size_t wOffsetDst, size_t hOffsetDst, cudaArray_const_t src, size_t wOffsetSrc, size_t hOffsetSrc, size_t width, size_t height, enum cudaMemcpyKind kind __dv(cudaMemcpyDeviceToDevice))
{
    return cudaSuccess;
}

__host__ cudaError_t CUDARTAPI cudaMemcpyToSymbol(const void *symbol, const void *src, size_t count, size_t offset __dv(0), enum cudaMemcpyKind kind __dv(cudaMemcpyHostToDevice))
{
    return cudaSuccess;
}

__host__ cudaError_t CUDARTAPI cudaMemcpyFromSymbol(void *dst, const void *symbol, size_t count, size_t offset __dv(0), enum cudaMemcpyKind kind __dv(cudaMemcpyDeviceToHost))
{
    return cudaSuccess;
}

__host__ __cudart_builtin__ cudaError_t CUDARTAPI cudaMemcpyAsync(void *dst, const void *src, size_t count, enum cudaMemcpyKind kind, cudaStream_t stream __dv(0))
{
    return cudaSuccess;
}

__host__ cudaError_t CUDARTAPI cudaMemcpyPeerAsync(void *dst, int dstDevice, const void *src, int srcDevice, size_t count, cudaStream_t stream __dv(0))
{
    return cudaSuccess;
}

__host__ cudaError_t CUDARTAPI cudaMemcpyToArrayAsync(cudaArray_t dst, size_t wOffset, size_t hOffset, const void *src, size_t count, enum cudaMemcpyKind kind, cudaStream_t stream __dv(0))
{
    return cudaSuccess;
}

__host__ cudaError_t CUDARTAPI cudaMemcpyFromArrayAsync(void *dst, cudaArray_const_t src, size_t wOffset, size_t hOffset, size_t count, enum cudaMemcpyKind kind, cudaStream_t stream __dv(0))
{
    return cudaSuccess;
}

__host__ __cudart_builtin__ cudaError_t CUDARTAPI cudaMemcpy2DAsync(void *dst, size_t dpitch, const void *src, size_t spitch, size_t width, size_t height, enum cudaMemcpyKind kind, cudaStream_t stream __dv(0))
{
    return cudaSuccess;
}

__host__ cudaError_t CUDARTAPI cudaMemcpy2DToArrayAsync(cudaArray_t dst, size_t wOffset, size_t hOffset, const void *src, size_t spitch, size_t width, size_t height, enum cudaMemcpyKind kind, cudaStream_t stream __dv(0))
{
    return cudaSuccess;
}

__host__ cudaError_t CUDARTAPI cudaMemcpy2DFromArrayAsync(void *dst, size_t dpitch, cudaArray_const_t src, size_t wOffset, size_t hOffset, size_t width, size_t height, enum cudaMemcpyKind kind, cudaStream_t stream __dv(0))
{
    return cudaSuccess;
}

__host__ cudaError_t CUDARTAPI cudaMemcpyToSymbolAsync(const void *symbol, const void *src, size_t count, size_t offset, enum cudaMemcpyKind kind, cudaStream_t stream __dv(0))
{
    return cudaSuccess;
}

__host__ cudaError_t CUDARTAPI cudaMemcpyFromSymbolAsync(void *dst, const void *symbol, size_t count, size_t offset, enum cudaMemcpyKind kind, cudaStream_t stream __dv(0))
{
    return cudaSuccess;
}

__host__ cudaError_t CUDARTAPI cudaMemset(void *devPtr, int value, size_t count)
{
    return cudaSuccess;
}

__host__ cudaError_t CUDARTAPI cudaMemset2D(void *devPtr, size_t pitch, int value, size_t width, size_t height)
{
    return cudaSuccess;
}

__host__ cudaError_t CUDARTAPI cudaMemset3D(struct cudaPitchedPtr pitchedDevPtr, int value, struct cudaExtent extent)
{
    return cudaSuccess;
}

__host__ __cudart_builtin__ cudaError_t CUDARTAPI cudaMemsetAsync(void *devPtr, int value, size_t count, cudaStream_t stream __dv(0))
{
    return cudaSuccess;
}

__host__ __cudart_builtin__ cudaError_t CUDARTAPI cudaMemset2DAsync(void *devPtr, size_t pitch, int value, size_t width, size_t height, cudaStream_t stream __dv(0))
{
    return cudaSuccess;
}

__host__ __cudart_builtin__ cudaError_t CUDARTAPI cudaMemset3DAsync(struct cudaPitchedPtr pitchedDevPtr, int value, struct cudaExtent extent, cudaStream_t stream __dv(0))
{
    return cudaSuccess;
}

__host__ cudaError_t CUDARTAPI cudaGetSymbolAddress(void **devPtr, const void *symbol)
{
    return cudaSuccess;
}

__host__ cudaError_t CUDARTAPI cudaGetSymbolSize(size_t *size, const void *symbol)
{
    return cudaSuccess;
}

__host__ cudaError_t CUDARTAPI cudaPointerGetAttributes(struct cudaPointerAttributes *attributes, const void *ptr)
{
    return cudaSuccess;
}

__host__ cudaError_t CUDARTAPI cudaDeviceCanAccessPeer(int *canAccessPeer, int device, int peerDevice)
{
    return cudaSuccess;
}

__host__ cudaError_t CUDARTAPI cudaDeviceEnablePeerAccess(int peerDevice, unsigned int flags)
{
    return cudaSuccess;
}

__host__ cudaError_t CUDARTAPI cudaDeviceDisablePeerAccess(int peerDevice)
{
    return cudaSuccess;
}

__host__ cudaError_t CUDARTAPI cudaGraphicsUnregisterResource(cudaGraphicsResource_t resource)
{
    return cudaSuccess;
}

__host__ cudaError_t CUDARTAPI cudaGraphicsResourceSetMapFlags(cudaGraphicsResource_t resource, unsigned int flags)
{
    return cudaSuccess;
}

__host__ cudaError_t CUDARTAPI cudaGraphicsMapResources(int count, cudaGraphicsResource_t *resources, cudaStream_t stream __dv(0))
{
    return cudaSuccess;
}

__host__ cudaError_t CUDARTAPI cudaGraphicsUnmapResources(int count, cudaGraphicsResource_t *resources, cudaStream_t stream __dv(0))
{
    return cudaSuccess;
}

__host__ cudaError_t CUDARTAPI cudaGraphicsResourceGetMappedPointer(void **devPtr, size_t *size, cudaGraphicsResource_t resource)
{
    return cudaSuccess;
}

__host__ cudaError_t CUDARTAPI cudaGraphicsSubResourceGetMappedArray(cudaArray_t *array, cudaGraphicsResource_t resource, unsigned int arrayIndex, unsigned int mipLevel)
{
    return cudaSuccess;
}

__host__ cudaError_t CUDARTAPI cudaGraphicsResourceGetMappedMipmappedArray(cudaMipmappedArray_t *mipmappedArray, cudaGraphicsResource_t resource)
{
    return cudaSuccess;
}

__host__ cudaError_t CUDARTAPI cudaGetChannelDesc(struct cudaChannelFormatDesc *desc, cudaArray_const_t array)
{
    return cudaSuccess;
}
/*
__host__ struct cudaChannelFormatDesc CUDARTAPI cudaCreateChannelDesc(int x, int y, int z, int w, enum cudaChannelFormatKind f)
{
    return NULL;
}
*/
__host__ cudaError_t CUDARTAPI cudaBindTexture(size_t *offset, const struct textureReference *texref, const void *devPtr, const struct cudaChannelFormatDesc *desc, size_t size __dv(UINT_MAX))
{
    return cudaSuccess;
}

__host__ cudaError_t CUDARTAPI cudaBindTexture2D(size_t *offset, const struct textureReference *texref, const void *devPtr, const struct cudaChannelFormatDesc *desc, size_t width, size_t height, size_t pitch)
{
    return cudaSuccess;
}

__host__ cudaError_t CUDARTAPI cudaBindTextureToArray(const struct textureReference *texref, cudaArray_const_t array, const struct cudaChannelFormatDesc *desc)
{
    return cudaSuccess;
}

__host__ cudaError_t CUDARTAPI cudaBindTextureToMipmappedArray(const struct textureReference *texref, cudaMipmappedArray_const_t mipmappedArray, const struct cudaChannelFormatDesc *desc)
{
    return cudaSuccess;
}

__host__ cudaError_t CUDARTAPI cudaUnbindTexture(const struct textureReference *texref)
{
    return cudaSuccess;
}

__host__ cudaError_t CUDARTAPI cudaGetTextureAlignmentOffset(size_t *offset, const struct textureReference *texref)
{
    return cudaSuccess;
}

__host__ cudaError_t CUDARTAPI cudaGetTextureReference(const struct textureReference **texref, const void *symbol)
{
    return cudaSuccess;
}

__host__ cudaError_t CUDARTAPI cudaBindSurfaceToArray(const struct surfaceReference *surfref, cudaArray_const_t array, const struct cudaChannelFormatDesc *desc)
{
    return cudaSuccess;
}

__host__ cudaError_t CUDARTAPI cudaGetSurfaceReference(const struct surfaceReference **surfref, const void *symbol)
{
    return cudaSuccess;
}

__host__ cudaError_t CUDARTAPI cudaCreateTextureObject(cudaTextureObject_t *pTexObject, const struct cudaResourceDesc *pResDesc, const struct cudaTextureDesc *pTexDesc, const struct cudaResourceViewDesc *pResViewDesc)
{
    return cudaSuccess;
}

__host__ cudaError_t CUDARTAPI cudaDestroyTextureObject(cudaTextureObject_t texObject)
{
    return cudaSuccess;
}

__host__ cudaError_t CUDARTAPI cudaGetTextureObjectResourceDesc(struct cudaResourceDesc *pResDesc, cudaTextureObject_t texObject)
{
    return cudaSuccess;
}

__host__ cudaError_t CUDARTAPI cudaGetTextureObjectTextureDesc(struct cudaTextureDesc *pTexDesc, cudaTextureObject_t texObject)
{
    return cudaSuccess;
}

__host__ cudaError_t CUDARTAPI cudaGetTextureObjectResourceViewDesc(struct cudaResourceViewDesc *pResViewDesc, cudaTextureObject_t texObject)
{
    return cudaSuccess;
}

__host__ cudaError_t CUDARTAPI cudaCreateSurfaceObject(cudaSurfaceObject_t *pSurfObject, const struct cudaResourceDesc *pResDesc)
{
    return cudaSuccess;
}

__host__ cudaError_t CUDARTAPI cudaDestroySurfaceObject(cudaSurfaceObject_t surfObject)
{
    return cudaSuccess;
}

__host__ cudaError_t CUDARTAPI cudaGetSurfaceObjectResourceDesc(struct cudaResourceDesc *pResDesc, cudaSurfaceObject_t surfObject)
{
    return cudaSuccess;
}

__host__ cudaError_t CUDARTAPI cudaDriverGetVersion(int *driverVersion)
{
    *driverVersion = config::driver_version;
    return cudaSuccess;
}

__host__ __cudart_builtin__ cudaError_t CUDARTAPI cudaRuntimeGetVersion(int *runtimeVersion)
{
    *runtimeVersion = config::runtime_version;
    return cudaSuccess;
}

__host__ cudaError_t CUDARTAPI cudaGetExportTable(const void **ppExportTable, const cudaUUID_t *pExportTableId)
{
	return cudaSuccess;
}

}
#endif
