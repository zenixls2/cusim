/* heading.h */
#ifndef _HEADING_H
#define _HEADING_H

#define YY_NO_UNPUT

using namespace std;

#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string>
#include <iomanip>
#include <vector>
#include <map>
#include <instruction.h>
#include <opcode.h>

class config_args;
class instruction;
class reg_alloc;
class address_range;

extern vector< instruction > *ir_buff;                      /* instruction buffer only */
extern vector<uint64_t> *local_buff;                        /* local allocation only */
extern vector< string > function_name;                      /* record for function name */
extern map<string, int > label_name;                        /* record for label name  */
extern map<string, vector< instruction >*> function_map;    /* record the relation between function name and instruction buffer */

extern std::vector<config_args> args_stack;
extern std::vector<std::string> fatbin_map;
extern std::map<int64_t*,std::string> function_address_map;
extern std::map<std::string,int> param_map;
extern std::map<std::string,int> local_map;
extern char* SharePtr;
extern reg_alloc *kernel_reg;
extern map<string, reg_alloc*> allocation_map;
extern map<string, vector<uint64_t>* > local_alloc_map;
extern std::vector<void*> address_map;

class config_args{
    public:
        void *arg;
        size_t size;
        size_t offset;

        config_args(const void* _arg, size_t _size, size_t _offset)
        {   
            arg = (void*)_arg;
            size = _size;
            offset = _offset;
        }   

};

class reg_alloc{
    public:
        reg_alloc(){
           rh_size=0; 
           r_size=0;
           rd_size=0;
           f_size=0;
           p_size=0;
        }
        unsigned rh_size;
        unsigned r_size;
        unsigned rd_size;
        unsigned f_size;
        unsigned p_size;
};

#endif


// how to use vector
//
// a = new vector<int>();
// a->push_back(100);
// a->at(index)
//
// how to use map
//
// map<char,vector<int>*> mymap;
// mymap['a'] = a;                      (set key-value)
// mymap.find('a')->second              (address)
// mymap.find('a')->second->at(index)   (value)
