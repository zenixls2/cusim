%option yylineno
%option noyywrap

%{
    #include <iostream>
    #include <ptx.tab.h>
    #include <opcode.h>
    int ptx_error( const char *s );
%}

%%

".extern"   {   return EXTERN_TAG;  }
".align"    {   return ALIGN_TAG;   }
".local"    {   return LOCAL_TAG;   }
".global"   {   return GLOBAL_TAG;  }
".shared"   {   return SHARED_TAG;  }
".param"    {   return PARAM_TAG;   }
".volatile" {   return VOLATILE_TAG;}

".entry"    {   return ENTRY_TAG;       }
".reg"      {   return REGISTER_TAG;    }
".file"     {   return UNDEFINE_TAG;    }
".loc"      {   return UNDEFINE_TAG;    }
".version"  {   return UNDEFINE_TAG;    }
".target"   {   return UNDEFINE_TAG;    }
".pred"     {   return PREDICATE_TAG;   }

"abs"       {   yylval.int_value = ABS;   return OPCODE;    }
"div"       {   return OPCODE;    }
"or"        {   return OPCODE;    }
"sin"       {   return OPCODE;    }
"ex2"       {   return OPCODE;    }
"pmevent"   {   return OPCODE;    }
"slct"      {   return OPCODE;    }
"vmad"      {   return OPCODE;    }
"vmax"      {   return OPCODE;    }
"fma"       {   return OPCODE;    }
"prefetch"  {   return OPCODE;    }
"mov"       {   yylval.int_value = MOV;   return OPCODE;    } 
"mul"       {   yylval.int_value = MUL;   return OPCODE;    }
"mul24"     {   yylval.int_value = MUL24; return OPCODE;    }
"min"       {   yylval.int_value = MIN;   return OPCODE;    }
"max"       {   yylval.int_value = MAX;   return OPCODE;    }
"add"       {   yylval.int_value = ADD;   return OPCODE;    }
"xor"       {   yylval.int_value = XOR;   return OPCODE;    }
"neg"       {   yylval.int_value = NEG;   return OPCODE;    }
"sub"       {   yylval.int_value = SUB;   return OPCODE;    }
"and"       {   yylval.int_value = AND;   return OPCODE;    }
"cvt"       {   yylval.int_value = CVT;   return OPCODE;    }
"ld"        {   yylval.int_value = LD;    return OPCODE;    }
"st"        {   yylval.int_value = ST;    return OPCODE;    }
"shr"       {   yylval.int_value = SHR;   return OPCODE;    }
"shl"       {   yylval.int_value = SHL;   return OPCODE;    }
"set"       {   yylval.int_value = SET;   return CONTROL;   }
"selp"      {   yylval.int_value = SELP;  return CONTROL;   }
"setp"      {   yylval.int_value = SETP;  return CONTROL;   }
"bra"       {   yylval.int_value = BRA;   return CONTROL;   }
"exit"      {   yylval.int_value = EXIT;  return OPCODE;    }
"bar.sync"  {   yylval.int_value = EXIT;  return BARSYNC;   }

".s8"       {   return TYPE_S8;  }
".s16"      {   return TYPE_S16; }
".s32"      {   return TYPE_S32; }
".s64"      {   return TYPE_S64; }
".u64"      {   return TYPE_U64; }
".u32"      {   return TYPE_U32; }
".u16"      {   return TYPE_U16; }
".u8"       {   return TYPE_U8;  }
".f64"      {   return TYPE_F64; }
".f32"      {   return TYPE_F32; }
".f16"      {   return TYPE_F16; }
".b64"      {   return TYPE_B64; }
".b32"      {   return TYPE_B32; }
".b16"      {   return TYPE_B16; }
".b8"       {   return TYPE_B8;  }

".eq"       {   return CMP_EQ;  }
".ne"       {   return CMP_NE;  }
".gt"       {   return CMP_GT;  }
".ge"       {   return CMP_GE;  }
".le"       {   return CMP_LE;  }
".lt"       {   return CMP_LT;  }
".rz"       {   return RZ_TAG;  }
".rn"       {   return RN_TAG;  }
".rm"       {   return RM_TAG;  }
".rp"       {   return RP_TAG;  }

".uni"      {   return UNI;     }
".wide"     {   return WIDE_TAG;}
".lo"       {   return LO_TAG;  }
".hi"       {   return HI_TAG;  }

"{"         {   return LEFT_BRACE;          }
"}"         {   return RIGHT_BRACE;         }
"["         {   return LEFT_BRACKET;        }
"]"         {   return RIGHT_BRACKET;       }
"("         {   return LEFT_PARENTHESIS;    }
")"         {   return RIGHT_PARENTHESIS;   }
";"         {   return SEMICOLON;           }
"+"         {   return PLUS;                }
"-"         {   return MINUS;               }
"<"         {   return LEFT_ANGLE_BRACKET;  }
">"         {   return RIGHT_ANGLE_BRACKET; }
":"         {   return COLON;               }
"@"         {   return AT;                  }
","         {   return COMMA;               }

"$L"([a-zA-Z0-9_])+                     {   yylval.string_value = strdup(yytext); return LABEL; }

([0-9])+                                {   sscanf(yytext,"%d", &yylval.int_value); return INTEGER;     }
"//"([[:blank:]a-zA-Z0-9:<>,-./_#()])*  {   return COMMENT;     }
([0-9])"."([0-9])                       {   sscanf(yytext,"%lf", &yylval.double_value); return DOUBLE;      }
"sm_"([0-9])+                           {   yylval.string_value = strdup(yytext); return STRING;      }
[a-zA-Z0-9_]+                           {   yylval.string_value = strdup(yytext); return STRING;      }
[\"]([a-zA-Z0-9.+,-/_<>])+[\"]          {   yylval.string_value = strdup(yytext); return STRING;      }

"%r"        {   yylval.int_value = R;  return REGISTER_HEAD;    }
"%rh"       {   yylval.int_value = RH; return REGISTER_HEAD;    }
"%rd"       {   yylval.int_value = RD; return REGISTER_HEAD;    }
"%f"        {   yylval.int_value = F;  return REGISTER_HEAD;    }
"%p"        {   return PREDICATE_HEAD;  }

"%ctaid.x"  {   yylval.int_value = CTAIDX; return SPECIAL_REGISTER; }
"%ctaid.y"  {   yylval.int_value = CTAIDY; return SPECIAL_REGISTER; }
"%ctaid.z"  {   yylval.int_value = CTAIDZ; return SPECIAL_REGISTER; }
"%ntid.x"   {   yylval.int_value = NTIDX;  return SPECIAL_REGISTER; }
"%ntid.y"   {   yylval.int_value = NTIDY;  return SPECIAL_REGISTER; }
"%ntid.z"   {   yylval.int_value = NTIDZ;  return SPECIAL_REGISTER; }
"%tid.x"    {   yylval.int_value = TIDX;   return SPECIAL_REGISTER; }
"%tid.y"    {   yylval.int_value = TIDY;   return SPECIAL_REGISTER; }
"%tid.z"    {   yylval.int_value = TIDZ;   return SPECIAL_REGISTER; }

"\n"
"\t"
" "

%%


int ptx_error( const char *s )
{
    std::cerr << "line " << yylineno << "\n";
    return 0;
}
