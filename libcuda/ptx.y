
%union {
    float   float_value;
    double  double_value;
    int     int_value;
    char*   string_value;
}

%token <int_value> REGISTER_HEAD
%token PREDICATE_HEAD
%token <int_value> SPECIAL_REGISTER
%token <int_value> INTEGER
%token <double_value> DOUBLE
%token <string_value> STRING
%token COMMENT

%token EXTERN_TAG       /* .extern */
%token GLOBAL_TAG       /* .global */
%token SHARED_TAG       /* .shared */
%token LOCAL_TAG        /* .local */
%token PARAM_TAG        /* .param */
%token ALIGN_TAG        /* .align */
%token VOLATILE_TAG     /* .volatile */

%token <int_value> OPCODE           /* instructions */
%token <int_value> CONTROL          /* branch intruction */
%token <int_value> BARSYNC             /* bar.sync */

%token <string_value> LABEL            /* $[a-zA-Z0-9]: */
%token ENTRY_TAG        /* .entry */
%token REGISTER_TAG     /* .reg  */
%token PREDICATE_TAG    /* .pred */
%token UNDEFINE_TAG     /* .file .loc .version .target */

%token LEFT_BRACKET
%token RIGHT_BRACKET
%token LEFT_BRACE
%token RIGHT_BRACE
%token LEFT_PARENTHESIS
%token RIGHT_PARENTHESIS
%token LEFT_ANGLE_BRACKET
%token RIGHT_ANGLE_BRACKET
%token SEMICOLON
%token COMMA
%token COLON
%token AT
%token PLUS
%token MINUS

%token WIDE_TAG
%token HI_TAG
%token LO_TAG
%token UNI

%token RZ_TAG
%token RN_TAG
%token RM_TAG
%token RP_TAG

%token CMP_EQ
%token CMP_NE
%token CMP_LT
%token CMP_LE
%token CMP_GT
%token CMP_GE

%token TYPE_S64
%token TYPE_S32
%token TYPE_S16
%token TYPE_S8
%token TYPE_U64
%token TYPE_U32
%token TYPE_U16
%token TYPE_U8
%token TYPE_F64
%token TYPE_F32
%token TYPE_F16
%token TYPE_B64
%token TYPE_B32
%token TYPE_B16
%token TYPE_B8

/* some header to put */

%{
    #include <iostream>
    #include <config.h>
    #include <db.h>
    #include <stdint.h>
    #include "instruction.h"
    #include "heading.h"

    extern dbout debug;

    #define IR ir_buff->at(ir_buff->size()-1)
    #define NEW_IR ir_buff->push_back( instruction() )

    int yyerror(char *s);
    int yylex(void);
    void select_type(int datatype);
    int datatype_select = -1;
    int register_count = 0;
    int param_count = 0;
    int local_count = 0;
    int data_size = 0;
    bool is_first_entry = true;
    bool is_first_alloc = true;
    bool is_first_local_alloc = true;
%}

%%
input:  /* empty */
        | input directive   { debug << "input directive\n"; }
        | input function    { debug << "input function\n"; }
        | input COMMENT     { debug << "input COMMENT\n"; }
        | input extern      { debug << "input extern\n"; }

directive: UNDEFINE_TAG INTEGER INTEGER INTEGER      { debug << "UNDEFINE_TAG INTEGER INTEGER INTEGER\n"; }
        |  UNDEFINE_TAG INTEGER STRING               { debug << "UNDEFINE_TAG INTEGER STRING\n"; }
        |  UNDEFINE_TAG STRING                       { debug << "UNDEFINE_TAG STRING\n"; }
        |  UNDEFINE_TAG STRING COMMA STRING          { debug << "UNDEFINE_TAG STRING COMMA STRING\n"; }
        |  UNDEFINE_TAG DOUBLE                       { debug << "UNDEFINE_TAG DOUBLE\n"; }

function: ENTRY_TAG STRING LEFT_PARENTHESIS parameters RIGHT_PARENTHESIS blocks          
        {
            debug << "ENTRY_TAG STRING LEFT_PARENTHESIS parameters RIGHT_PARENTHESIS\n";
            ir_buff->pop_back();
            function_name.push_back($2);
            function_map[$2] = ir_buff;
            local_alloc_map[$2] = local_buff;
            local_buff = NULL;
            allocation_map[$2] = kernel_reg;
            is_first_entry = true;
            is_first_alloc = true;
            is_first_local_alloc = true;
            param_count = 0;
            local_count = 0;
        }

extern: EXTERN_TAG SHARED_TAG ALIGN_TAG INTEGER datatype STRING LEFT_BRACKET RIGHT_BRACKET SEMICOLON    
        {
            debug << "EXTERN SHARED ALIGN INTEGER datatype STRING LEFT_BRACKET RIGHT_BRACKET SEMICOLON\n";
        }

blocks: LEFT_BRACE statements RIGHT_BRACE           { debug << "LEFT_BRACE statements RIGHT_BRACE\n"; }

parameters: /* empty */
        | parameters COMMA parameter                { debug << "parameters COMMA parameter\n";}
        | parameter                                 { debug << "parameter\n";}

parameter: PARAM_TAG datatype STRING                
            {
                param_map[$3] = param_count++;
                debug << "PARAM_TAG datatype STRING\n";
            }

statements: /* empty */
        | statements directive                      { debug << "statements directive\n";}
        | statements allocation SEMICOLON           { debug << "statements allocation\n";}
        | statements LABEL COLON                    
            {
                debug << "statements LABEL COLON\n"; 
                if(is_first_entry)
                {
                    ir_buff = new vector< instruction >();
                    NEW_IR;
                    datatype_select = 0;
                    is_first_entry = false;
                }
                else
                    label_name[$2] = ir_buff->size()-1; 
            }

        | statements opcode SEMICOLON               {debug << "statements opcode\n"; register_count = 0;}
        | statements control SEMICOLON              {debug << "statements control\n"; register_count = 0;}
        | statements COMMENT                        {debug << "statements COMMENT\n";}

allocation: REGISTER_TAG datatype alloc_register                                                {debug << "REGISTER_TAG datatype ALLOC_REGISTER\n";}
        |   REGISTER_TAG PREDICATE_TAG alloc_register                                           {debug << "REGISTER_TAG PREDICATE_TAG ALLOC_REGISTER\n";}
        |   SHARED_TAG ALIGN_TAG INTEGER datatype STRING LEFT_BRACKET INTEGER RIGHT_BRACKET     {debug << "SHARED ALIGN INTEGER datatype STRING LEFT_BRACKET INTEGER RIGHT_BRACKET\n";}
        |   SHARED_TAG datatype STRING                                                          {debug << "SHARED datatype STRING\n";}
        |   LOCAL_TAG ALIGN_TAG INTEGER datatype STRING LEFT_BRACKET INTEGER RIGHT_BRACKET      
            {
                if(is_first_local_alloc) {
                    is_first_local_alloc = false;
                    local_buff = new vector<uint64_t>();
                }
                local_buff->push_back($7 * data_size);
                local_map[$5] = local_count++;
                data_size = 0;
                debug << "LOCAL ALIGN INTEGER datatype STRING LEFT_BRACKET INTEGER RIGHT_BRACKET\n";
            }

alloc_register: REGISTER_HEAD LEFT_ANGLE_BRACKET INTEGER RIGHT_ANGLE_BRACKET            
            {
                if(is_first_alloc) {
                    kernel_reg = new reg_alloc();
                    is_first_alloc = false;
                    kernel_reg->r_size = 0;
                    kernel_reg->rh_size = 0;
                    kernel_reg->rd_size = 0;
                    kernel_reg->f_size = 0;
                    kernel_reg->p_size = 0;
                }
                switch($1){
                    case R: kernel_reg->r_size = $3; break;
                    case RH: kernel_reg->rh_size = $3; break;
                    case RD: kernel_reg->rd_size = $3; break;
                    case F: kernel_reg->f_size = $3; break;
                }       
                debug << "REGISTER_HEAD LEFT_ANGLE_BRACKET INTEGER RIGHT_ANGLE_BRACKET\n";
            }

        |       PREDICATE_HEAD LEFT_ANGLE_BRACKET INTEGER RIGHT_ANGLE_BRACKET           
            {
                if(is_first_alloc) {
                    kernel_reg = new reg_alloc();
                    is_first_alloc = false;
                }
                kernel_reg->p_size = $3;
                debug << "PREDICATE_HEAD LEFT_ANGLE_BRACKET INTEGER RIGHT_ANGLE_BRACKET\n";
            }

opcode:   OPCODE datatype register COMMA register COMMA register            
            {
                IR.set_opcode($1); 
                NEW_IR;
                datatype_select = 0; 
                debug << "OPCODE datatype register COMMA register COMMA register\n";
            }

        | OPCODE round datatype datatype register COMMA register                  
            {
                IR.set_opcode($1); 
                NEW_IR;
                datatype_select = 0;
                debug << "OPCODE round datatype datatype register COMMA register\n";
            }

        | OPCODE datatype datatype register COMMA register
            {   
                IR.set_opcode($1); 
                NEW_IR;
                datatype_select = 0;
                debug << "OPCODE datatype datatype register COMMA register\n";
            }   

        | OPCODE datatype register COMMA register                           
            {
                IR.set_opcode($1); 
                NEW_IR;
                datatype_select = 0;
                debug << "OPCODE datatype register COMMA SPECIAL_REGISTER\n";
            }

        | OPCODE PREDICATE_TAG register COMMA register COMMA register       
            {
                IR.set_opcode($1); 
                NEW_IR;
                datatype_select = 0;
                debug <<"OPCODE PREDICATE_TAG register COMMA register COMMA register\n";
            }

        | OPCODE memory datatype register COMMA address                     
            {
                IR.set_opcode($1); 
                NEW_IR;
                datatype_select = 0;
                debug << "OPCODE memory datatype register COMMA address\n";
            }

        | OPCODE memory datatype address COMMA register                     
            {
                IR.set_opcode($1); 
                NEW_IR;
                datatype_select = 0;
                debug << "OPCODE memory datatype address COMMA register\n";
            }

        | OPCODE mul_type datatype register COMMA register COMMA register   
            {
                IR.set_opcode($1); 
                NEW_IR;
                datatype_select = 0;
                debug << "OPCODE mul_type datatype register COMMA register COMMA register\n";
            }
        | AT register OPCODE datatype register COMMA register COMMA register
            {
                IR.set_opcode($3);
                NEW_IR;
                datatype_select = 0;
                debug << "AT register OPCODE datatype register COMMA register COMMA register\n";
            }

        | OPCODE            
            {
                IR.set_opcode($1); 
                NEW_IR;
                datatype_select = 0;
                debug << "EXIT\n";
            }

        | BARSYNC INTEGER      
            {
                IR.set_opcode($1);
                NEW_IR;
                debug << "BARSYNC INTEGER\n";
            } 

mul_type: WIDE_TAG  {IR.set_mul_type(WIDE);}
        | LO_TAG    {IR.set_mul_type(LO);}
        | HI_TAG    {IR.set_mul_type(HI);}

register: REGISTER_HEAD INTEGER         
            {
                IR.set_reg_no(register_count++,$2);
                debug << "REGISTER_HEAD INTEGER\n";
            }

        | PREDICATE_HEAD INTEGER
            {
                IR.set_p_reg(register_count);
                IR.set_reg_no(register_count++,$2);
                debug << "PREDICATE_HEAD INTEGER\n";
            }

        | SPECIAL_REGISTER              
            {
                IR.set_special_reg($1);
                debug << "SPECIAL_REGISTER\n";
            }
        | STRING                        
            {
                IR.set_addr_label($1);
                debug << "STRING\n";
            }
        | INTEGER                       
            {
                IR.set_int($1);
                debug << "INTEGER\n";
            }
        | MINUS INTEGER
            {
                IR.set_int($2*-1);
                debug << "MINUS INTEGER\n";
            }

memory :  SHARED_TAG                    {IR.set_state_space(SHARED); debug << "SHARED_TAG\n";}
        | PARAM_TAG                     {IR.set_state_space(PARAM);  debug << "PARAM_TAG\n";}
        | LOCAL_TAG                     {IR.set_state_space(LOCAL);  debug << "LOCAL_TAG\n";}
        | GLOBAL_TAG                    {IR.set_state_space(GLOBAL); debug << "GLOBAL_TAG\n";}

address:  LEFT_BRACKET REGISTER_HEAD INTEGER PLUS INTEGER RIGHT_BRACKET  
            {
                IR.set_addr_reg($3);
                IR.set_addr_offset($5);
                debug << "LEFT_BRACKET register PLUS INTEGER RIGHT_BRACKET " << $5 << "\n";
            }

        | LEFT_BRACKET REGISTER_HEAD INTEGER PLUS MINUS INTEGER RIGHT_BRACKET 
            {
                IR.set_addr_reg($3);
                IR.set_addr_offset($6 * -1);
                debug << "LEFT_BRACKET register PLUS MINUS INTEGER RIGHT_BRACKET\n";
            }

        | LEFT_BRACKET STRING RIGHT_BRACKET                 
            {
                IR.set_addr_label($2);
                debug << "LEFT_BRACKET STRING RIGHT_BRACKET\n";
            }

        | LEFT_BRACKET STRING PLUS INTEGER RIGHT_BRACKET    
            {
                IR.set_addr_label($2);
                IR.set_addr_offset($4);
                debug << "LEFT_BRACKET STRING PLUS INTEGER RIGHT_BRACKET\n";
            }

control:  AT register CONTROL LABEL                                                     
            {
                IR.set_opcode($3);
                IR.set_label($4);
                NEW_IR;
                datatype_select = 0;
                debug << "AT register CONTROL LABEL\n";
            }

        | CONTROL UNI LABEL                                                             
            {
                IR.set_opcode($1);
                IR.set_label($3); 
                NEW_IR;
                datatype_select = 0;
                debug << "CONTROL UNI LABEL\n";
            }

        | CONTROL compare datatype register COMMA register COMMA register               
            {
                IR.set_opcode($1); 
                NEW_IR;
                datatype_select = 0;
                debug << "CONTROL compare datatype register COMMA register COMMA register\n";
            }

        | CONTROL compare datatype datatype register COMMA register COMMA register      
            {
                IR.set_opcode($1); 
                NEW_IR;
                datatype_select = 0;
                debug << "CONTROL compare datatype datatype register COMMA register COMMA register\n";
            }

        | CONTROL datatype register COMMA register COMMA register COMMA register
            {
                IR.set_opcode($1);
                NEW_IR;
                datatype_select = 0;
                debug << "CONTROL datatype register COMMA register COMMA register COMMA register\n";
            }

compare: CMP_EQ {IR.set_cmp_type(EQ); }
       | CMP_NE {IR.set_cmp_type(NE); }
       | CMP_LT {IR.set_cmp_type(LT); }
       | CMP_LE {IR.set_cmp_type(LE); }
       | CMP_GT {IR.set_cmp_type(GT); }
       | CMP_GE {IR.set_cmp_type(GE); }

datatype: TYPE_S64 {data_size = 8; if(!is_first_entry) select_type(S64); }
        | TYPE_S32 {data_size = 4; if(!is_first_entry) select_type(S32); }
        | TYPE_S16 {data_size = 2; if(!is_first_entry) select_type(S16); }
        | TYPE_S8  {data_size = 1; if(!is_first_entry) select_type(S8 ); }
        | TYPE_U64 {data_size = 8; if(!is_first_entry) select_type(U64); }
        | TYPE_U32 {data_size = 4; if(!is_first_entry) select_type(U32); }
        | TYPE_U16 {data_size = 2; if(!is_first_entry) select_type(U16); }
        | TYPE_U8  {data_size = 1; if(!is_first_entry) select_type(U8 ); }
        | TYPE_F64 {data_size = 8; if(!is_first_entry) select_type(F64); }
        | TYPE_F32 {data_size = 4; if(!is_first_entry) select_type(F32); }
        | TYPE_F16 {data_size = 2; if(!is_first_entry) select_type(F16); }
        | TYPE_B64 {data_size = 8; if(!is_first_entry) select_type(B64); }
        | TYPE_B32 {data_size = 4; if(!is_first_entry) select_type(B32); }
        | TYPE_B16 {data_size = 2; if(!is_first_entry) select_type(B16); }
        | TYPE_B8  {data_size = 1; if(!is_first_entry) select_type(B8 ); }

round: RZ_TAG {}
     | RN_TAG {}
     | RM_TAG {}
     | RP_TAG {}

%%

extern int yylineno;

int yyerror(char *s)
{
    debug << " line " << yylineno << " parse error!" << s << "\n";
    abort();
}

void select_type(int datatype)
{
    /* parameter datatype, local function parameter datatype, instruction datatype */
    switch(datatype_select){
        case 0:
            IR.set_type1(datatype);
            datatype_select++;
            break;
        case 1:
            IR.set_type2(datatype);
            break;
    }
    
}

