/*
ALU 1
MAD 2
CTL 3
SFU 4
MEM 5
*/
#ifndef opcodes_h
#define opcodes_h

enum opcode {
    LD = 0,
    ST,
    MOV,
    ADD,
    SUB,
    AND,    /*5*/
    XOR,
    NEG,
    SHR,
    SHL,
    MIN,    /*10*/
    MAX,
    CVT,
    MUL,
    MUL24,
    ABS,    /*15*/
    SET,
    SELP,
    SETP,
    BRA,
    EXIT,   /*20*/
    SYNC
};

enum mul_type {
    HI=100,
    LO,
    WIDE
};

enum state_space {
    NULLSPACE=0,
    LOCAL=200,
    SHARED,
    PARAM,
    GLOBAL,
    CONSTANT
};

enum type {
    VOIDTYPE=299,
    S64=300,
    S32,
    S16,
    S8,
    U64,
    U32,
    U16,
    U8,
    F64,
    F32,
    F16,
    B64,
    B32,
    B16,
    B8
};

enum thread {
    CTAIDX = 400,   /* blockDim.x */
    CTAIDY,
    CTAIDZ,
    NCTAIDX,
    NCTAIDY,
    NCTAIDZ,
    TIDX,           /* tid.x */
    TIDY,
    TIDZ,
    NTIDX,          /* blockIdx.x */
    NTIDY,
    NTIDZ
};

enum reg_type {
    RH = 500,
    R,
    RD,
    F
};

enum cmp_type {
    EQ = 600,
    NE,
    LT,
    LE,
    GT,
    GE
};

#endif

