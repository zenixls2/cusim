\documentclass[a0paper,portrait]{baposter}

\usepackage{relsize}
\usepackage{url}
\usepackage{epstopdf}

\graphicspath{{pix/}}
\tracingstats=2
\definecolor{bordercol}{RGB}{100, 100, 100}
\definecolor{headercol1}{RGB}{186, 215, 230}
\definecolor{headercol2}{RGB}{80,80,80}
\definecolor{headerfontcol}{RGB}{0, 0, 0}
\definecolor{boxcolor}{RGB}{186, 215, 230}

\newcommand{\compresslist} {
	\setlength{\itemsep}{1pt}
	\setlength{\parskip}{0pt}
	\setlength{\parsep}{0pt}
}
\begin{document}
\typeout{Poster rendering started}

\background{
	\begin{tikzpicture}[remember picture,overlay]%
	\draw (current page.north west)+(-2em,2em) node[anchor=north west]
	{\includegraphics[height=1.1\textheight]{background}};
	\end{tikzpicture}
}

\begin{poster} {
	grid=false,
	borderColor=bordercol,
	headerColorOne=headercol1,
	headerColorTwo=headercol2,
	headerFontColor=headerfontcol,
	boxColorOne=boxcolor,
	headershape=roundedright,
	headerfont=\Large\sf\bf,
	textborder=rectangle,
	background=user,
	headerborder=open,
	boxshade=plain
}
{}
%%Title%%
{\sf\bf
	Warp Interweaving: Using CUSim
}
%%Authors%%
{
	\vspace{1em} Wei-Chao Chen, Han-sheng Huang, Jen-Jung Cheng, Yike Huang\\ 
	{\smaller weichao.chen@gmail.com, r01944053@ntu.edu.tw, r01922054@ntu.edu.tw, b99902032@csie.ntu.edu.tw}
}
%%Logo%%
{
\setlength\fboxsep{0pt}
\setlength\fboxrule{0.5pt}
	\fbox{
		\begin{minipage}{14em}
		\end{minipage}
	}
}
\headerbox{Introduction}
{name=introduction, column=0, row=0} {
Graphics Processing Units (GPUs) run fine-grained threads to amortize the cost of lightweight threads execution. When some threads in warp take divergent execution paths, the warp will execute sequentially, defeating the benefits of SIMT advantage. Therefore, to increase efficiency on parallelism, we use a technique name Simultaneous Branch Interleaving (SBI), which instructions can be co-issued by the same warp, allowing more threads to execute during branch divergence.
}
\headerbox{Baseline Architecture}
{name=baseline, column=0, below=introduction} {
Figure XX shows an overview of the system we simulated. In CUDA, the GPU is functioned as a co-processor which applications can delegate massive parallizable work from CPU. Each work is assigned as a kernel. The programming model should divide work into threads, threads into blocks, and blocks into grids. The compute work distributor allocates blocks to streaming multiprocessors (SMs).  When a block is distributed to a SM, resources for the block are allocated (warps, register files, shared memory, etc) and threads are divided into groups of 32 threads called warps, which are also referred as cooperative thread arrays (CTAs). Each cycle, the warp schedulers picks up warps and dispatch these warps to execution units. Our default configuration is for Nvidia's Tesla C2050.
\includegraphics[width=\textwidth]{architecture.pdf}
Our SM has 5 stages : fetch, decode, execute, memory and writeback. Between the stages are 4 kinds of pipelines: IF\_ID, ID\_EX, EX\_MEM, MEM\_WB. All the pipelines are designed using queues. In the processor, there are several units on it: the Streaming Processors (SP), Special Function Units (SFU), multithreaded instruction fetch and issue unit (MT-IU). The MT-IU is the warp scheduler.
}
\headerbox{Reference}
{name=refer, column=1, span=2, row=0} {
\smaller													% Make the whole text smaller
\vspace{-0.4em} 										% Save some space at the beginning
\bibliographystyle{plain}							% Use plain style
\renewcommand{\section}[2]{\vskip 0.05em}		% Omit "References" title
\begin{thebibliography}{1}							% Simple bibliography with widest label of 1
\itemsep=-0.01em										% Save space between the separation
\setlength{\baselineskip}{0.4em}					% Save space with longer lines
\bibitem{prevWork1} Bakhoda, A.; Yuan, G.L.; Fung, W.W.L.; Wong, H.; Aamodt, T.M.: \emph{Analyzing CUDA workloads using a detailed GPU simulator}, Performance Analysis of Systems and Software, 2009. ISPASS 2009. IEEE International Symposium on , vol., no., pp.163,174, 26-28 April 2009 
\bibitem{prevWork2} Brunie, N.; Collange, S.; Diamos, G., \emph{Simultaneous branch and warp interweaving for sustained GPU performance}, Computer Architecture (ISCA), 2012 39th Annual International Symposium on , vol., no., pp.49,60, 9-13 June 2012
\bibitem{gulya-kampis1} Lindholm, E.; Nickolls, J.; Oberman, S.; Montrym, J.: \emph{NVIDIA Tesla: A Unified Graphics and Computing Architecture}, Micro, IEEE , vol.28, no.2, pp.39,55, March-April 2008
\end{thebibliography}
}
\headerbox{Scheduler}
{name=scheduler, column=1, span=2, below=refer} {
In our implementation, we provide two types of scheduling: the kernel and the warp. For both of the scheduling, we use Round Robin for simplicity. The kernel scheduling gives the possibility of executing multiple kernels concurrently. It schedules kernels to SMs and allocate resources for the warps. For the warp scheduler, each SM has its own one. The warp scheduler schedules from all of its threads to each of the SPs and SFUs. The warp scheduler also peeks what the next instruction's opcode is ``BRA'' to know if warp interweaving should apply.
}
\headerbox{CUDA Runtime API}
{name=runapi, column=1, span=2, below=scheduler} {
\begin{minipage}{0.7\textwidth}
To communicate with host side, we build a CUDA runtime library for the interface defined by CUDA header to redirect the host API to our library. When the CUDA program first calls the CUDA API, it calls the cudaRegisterFatBinary function to register kernel functions in virtual gpu device. Consequence, we use bison and flex to accomplish the task of extracting CUDA PTX information from CUDA PTX assembly code by using cuobjdump. After extracting data from the text format PTX assembly code, we store the PTX execution information in runtime look up tables. Generally, CUDA program set up the arguments to gpu by cudaSetupArgument and cudaConfigureCall functions before running the kernel function. Finally, CUDA program calls the cudaLaunch function to start create the kernel task for our virtual gpu to execute with corresponding look up tables.
\end{minipage}
\begin{minipage}{0.3\textwidth}
\center
\includegraphics[scale=0.2]{cuda_runtime_api.pdf}
\end{minipage}
}
\headerbox{Branch Interweaving} 
{name=branch, column=1, span=2, below=runapi} {
\begin{minipage}{0.5\textwidth}
Each entry of warp containing sets of warp-split. Each warp-split contains the common program counter (CPC) and the activity mask. The Sorter unit is responsible for keeping the all the CPCs sorted and merge them as needed. It receives CPC1 and CPC2 from general operation execution. When anyone of CPCs takes divergent paths by disjoint sets of threads, it will receive additional CPC3 or CPC4 from the branch with new mask and creates new warp-split for additional CPC. The Sorter sorts the CPCs and assigns the smallest CPC to CPC1 and next smallest CPC to CPC2. Therefore, MT IU ensures that it must select two smallest CPC to Stream Processor (SP) for execution. To prevent from anyone of warp-split exiting the program early, we put a synchronization point at the final instruction to guarantee all the mask reconvergence at the end of execution.
\end{minipage}
\begin{minipage}{0.5\textwidth}
\center
\includegraphics[angle=-90,scale=0.25]{MTIU.pdf}
\end{minipage}
}
\headerbox{Benchmark Result}
{name=bench, column=1, span=2, below=branch} {
\begin{minipage}{0.33\textwidth}
\center
testcase 1
\includegraphics[scale=0.2]{testcase1.pdf}
\end{minipage}
\begin{minipage}{0.33\textwidth}
\center
testcase 2
\includegraphics[scale=0.2]{testcase2.pdf}
\end{minipage}
\begin{minipage}{0.33\textwidth}
\center
testcase 3
\includegraphics[scale=0.2]{testcase3.pdf}
\end{minipage}
}
\end{poster}
\end{document}