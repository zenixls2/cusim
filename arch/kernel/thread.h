#ifndef _THREAD_H
#define _THREAD_H
#include <type.h>
#include <config.h>
#include <db.h>
#include <assert.h>
#include <vector>
#include <register_file.h>

#include <heading.h>
#include <iostream>
using namespace std;
class reg_alloc;
extern dbout debug;
/*
   This is private class included by kernel.h , which is also interface of

   abstract programming model.

*/
class thread_stat {
    public:
         dim3     get_id()    { return _tid; }
         unsigned get_x()     { return _tid.x; }
         unsigned get_y()     { return _tid.y; }
         unsigned get_z()     { return _tid.z; }
         void set_tid(dim3 id){  _tid = id; }
    private:
         dim3 _tid;
};

class block {
public:
    block(){}
    block(dim3 d, dim3 bd, int _id);
    thread_stat get_thread(unsigned idx) {
        assert(idx >= 0 && idx < config::threads_per_block);
        return thread[idx];
    }

    dim3 get_block_dim(){ return cta; }
    dim3 get_block_dimid(){ return bid; }
    void set_block_dimid(dim3 bd) {
        bid = bd;
    }

    void set_funcIR(std::vector< instruction > *func){
        assert(func != NULL);
        ir_buf = func;
    }

    void set_kernel_local_alloc(std::vector< uint64_t> *local_alloc){
        kernel_local_alloc = local_alloc;
    }

    std::vector< instruction > *get_funcIR(){ return ir_buf; }
    unsigned int get_numofthreads(){ return thread_num; }
    unsigned int get_id(){ return block_id; }
    void set_kernel_alloc(reg_alloc *alloc){
        kernel_alloc = alloc; 
    }

    void thread_reg_alloc();
    void thread_local_alloc();

    void thread_reg_free(){
        if(rh) delete rh;
        if(r) delete r;
        if(rd) delete rd;
        if(f) delete f;
        if(p) delete p;
    }

    void thread_local_free(){
        if(local)
        {
            for(unsigned i=0; i<thread_num; i++) {
                for(unsigned j=0; j<kernel_local_alloc->size(); j++)
                    delete[] local[i][j];
                delete[] local[i];
            }
            delete[] local;
        }
    }

    void rm_ref_count(){ ref_count--; }
    unsigned int get_ref_count(){ return ref_count; }

    register_file<uint16_t>* get_rh(){ return rh; }
    register_file<uint32_t>* get_r() { return r;  }
    register_file<uint64_t>* get_rd(){ return rd; }
    register_file<float>* get_f() { return f;  }
    register_file<bool>* get_p() { return p;  }
    char* get_local(uint32_t tid,uint32_t ith) { return local[tid][ith]; }

    void print(){
        debug << "block: " << bid.x << " " << bid.y << " " << bid.z << "\n";
        for(unsigned i=0;i<thread_num;i++)
            debug << thread[i].get_x() << " " << thread[i].get_y() << " " << thread[i].get_z() << "\n";
    }

private:
    class thread_stat thread[config::threads_per_block];
    register_file<uint16_t> *rh;
    register_file<uint32_t> *r;
    register_file<uint64_t> *rd;
    register_file<float> *f;
    register_file<bool> *p;
    char ***local;
    unsigned int thread_num;
    unsigned int block_id;
    unsigned int ref_count;
    std::vector< instruction > *ir_buf;
    std::vector< uint64_t > *kernel_local_alloc;
    reg_alloc *kernel_alloc;
    dim3 cta;
    dim3 bid;
};

#endif /* _THREAD_H */
