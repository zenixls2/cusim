#include <thread.h>
block::block(dim3 d, dim3 bd, int _id)
{
    cta = d;
    bid = bd;
    block_id = _id;
    thread_num = d.x * d.y * d.z;
    ref_count = (thread_num+31)/config::warp_size;
    assert(thread_num <= config::threads_per_block);
    for(unsigned i=0; i<thread_num; i++)
        thread[i].set_tid(dim3(i % d.x, (i/d.x)%d.y, (i/d.x/d.y)%d.z));
    r = NULL;
    rh = NULL;
    rd = NULL;
    f = NULL;
    p = NULL;
    local = NULL;
    kernel_local_alloc = NULL;
}
void block::thread_reg_alloc()
{
    if(kernel_alloc->rh_size > 0)
        rh = new register_file<uint16_t>(thread_num, kernel_alloc->rh_size);
    if(kernel_alloc->r_size > 0)
        r = new register_file<uint32_t>(thread_num, kernel_alloc->r_size);
    if(kernel_alloc->rd_size > 0)
        rd = new register_file<uint64_t>(thread_num, kernel_alloc->rd_size);
    if(kernel_alloc->f_size > 0)
        f = new register_file<float>(thread_num, kernel_alloc->f_size);
    if(kernel_alloc->p_size > 0)
        p = new register_file<bool>(thread_num, kernel_alloc->p_size);
}

void block::thread_local_alloc()
{
    if(kernel_local_alloc){
        local = new char**[thread_num];
        for(unsigned i=0; i<thread_num; i++) {
            local[i] = new char*[kernel_local_alloc->size()];
            for(unsigned j=0; j<kernel_local_alloc->size(); j++)
                local[i][j] = new char[kernel_local_alloc->at(j)];
        }
    }
}
