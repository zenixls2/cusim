#ifndef _TYPE_H
#define _TYPE_H

#include <builtin_types.h>
#include <instruction.h>
class instruction;
class register_set {
    register_set(int num);/* {
        reg = new instruction*[num];
        for(int i = 0; i < num; ++i)
            reg[i] = new instruction();
        size = num;
    }*/
    ~register_set();
    bool in(instruction **ins);
    bool out(instruction **ins);
    instruction** get_free();
    instruction** get_ready();
protected:
    instruction **reg;
    int size;
};

#endif /* _TYPE_H */
