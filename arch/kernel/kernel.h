#ifndef _KERNEL_H
#define _KERNEL_H
#include <type.h>
#include <thread.h>
#include <vector>
#include <config.h>

/*
   This class makes connection with abstract programming model
*/

using namespace std;

class kernel_stat {
public:        //functions of current state of kernel
    kernel_stat(dim3 _grid_dim, dim3 _block_dim, int _shmem_size);
    ~kernel_stat();
    dim3 grid_size() {return _grid_dim;}
    dim3 block_size() {return _block_dim;}
    block* get_block(int idx) { return block_list.at(idx); }
    unsigned get_num_of_block(){ return (unsigned)block_list.size(); }
    void set_kernel_func(vector< instruction > *func);
    void set_kernel_alloc(reg_alloc *alloc);
    void set_kernel_local_alloc(vector<uint64_t> *);
    void alloc_ith_block(unsigned int ith);

private:        // private member
    dim3 _grid_dim;
    dim3 _block_dim;
    int _shmem_size;
    unsigned size;
    vector<class block*> block_list;
};

#endif /* _KERNEL_H */
