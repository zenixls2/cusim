#include <type.h>
register_set::register_set(int num)
{
    reg = new instruction*[num];
    for(int i = 0; i < num; ++i)
        reg[i] = new instruction();
    size = num;
}
register_set::~register_set()
{ 
    delete reg;
}
bool register_set::in(instruction **ins)
{
    instruction** free = get_free();
    instruction* tmp = *free;
    *free = *ins;
    tmp->clear();
    return true;
}
bool register_set::out(instruction **ins)
{
    instruction** ready = get_ready();
    instruction* tmp = *ins;
    *ins = *ready;
    tmp->clear();
    return true;
}
instruction** register_set::get_free()
{
    for(int i = 0; i < size; ++i)
        if(!reg[i]->is_set())
            return &reg[i];
    return NULL;
}
instruction** register_set::get_ready()
{
    instruction **now = NULL;
    for(int i = 0; i < size; ++i)
        if(reg[i]->is_set()) {
            if(now && reg[i]->get_uid() > (*now)->get_uid()) {
            } else {
                now = &reg[i];
            }
        }
    return now;
}


