#include <kernel.h>
#include <vector>
#include <iostream>

kernel_stat::kernel_stat(dim3 grid_dim, dim3 block_dim, int shmem_size)
{
    _grid_dim = grid_dim;
    _block_dim = block_dim;
    _shmem_size = shmem_size;
    dim3 tmp(0, 0, 0);
    size = _grid_dim.x * _grid_dim.y * _grid_dim.z;
    for(unsigned i=0; i<size; i++) {
        tmp.x = i%_grid_dim.x;
        tmp.y = (i/_grid_dim.x)%_grid_dim.y; 
        tmp.z = (i/_grid_dim.x/_grid_dim.y)%_grid_dim.z;
        block_list.push_back( new block(block_dim, tmp, i) );
    }
}

kernel_stat::~kernel_stat()
{
    block_list.erase(block_list.begin(), block_list.end());
}

void kernel_stat::set_kernel_func(std::vector< instruction > *func){
    for(unsigned i=0; i<size; i++) {
        block_list[i]->set_funcIR(func);
    }
}

void kernel_stat::set_kernel_alloc(reg_alloc *alloc){
    for(unsigned i=0; i<size; i++){
        block_list[i]->set_kernel_alloc(alloc);
    }
}

void kernel_stat::set_kernel_local_alloc(vector<uint64_t> *alloc){
    for(unsigned i=0; i<size; i++){
        block_list[i]->set_kernel_local_alloc(alloc);
    }
}

void kernel_stat::alloc_ith_block(unsigned int ith){
    block_list[ith]->thread_reg_alloc();
    block_list[ith]->thread_local_alloc();
}


