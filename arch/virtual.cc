#include <virtual.h>
#include <stdint.h>
#include <opcode.h>
#include <queue>
#include <type.h>
#include <stdio.h>


extern dbout debug;
/* Conditional jump or move depends on uninitialised value(s) */
bool stream_multiprocessor::cycle_exec(){
    debug << "pipeline status: " << stall << "\n";
    if(!stall) {
        sched_warp = schedule();
#if WARP_INTERWEAVING
        if(sched_warp) {
            sched_warp_2 = schedule2(sched_warp);
        } else
            sched_warp_2 = NULL;
#endif
        fetch(sched_warp);
        decode();
    }
    stall = exec();
    memory();
    writeback();
    pipe();
    rm_warp();
    return MT_IU.empty();
}

void stream_multiprocessor::fetch(warp *cur_warp){
    if(cur_warp){
        debug << "-------------------------\n";
        debug << "fetch\n";
        unsigned tid, pc;
        instruction *fetch_instruction;
        unsigned round = 1;
#if INTERWEAVING
        if(cur_warp->simt_issuable())
            round = 2;
#endif
#if WARP_INTERWEAVING
        warp* temp_warp = cur_warp;
        if(sched_warp_2)
            round = 2;
        else
            fetch_warp_2 = NULL;
#endif
        for(unsigned j=0; j<round; j++) {
#if INTERWEAVING
            if(j==1) 
                cur_warp->swap_simt();
#endif
#if WARP_INTERWEAVING
            if(j==0) {
#endif
            pc = cur_warp->get_warppc();
            debug << "warp fetch " << j << " pc: " << pc << "\n";
            fetch_instruction = &cur_warp->get_warpparent()->get_funcIR()->at(pc);
            debug << "warp ins: " << j << " " << fetch_instruction->get_opcode() << "\n";
            // do lock the register
#if WARP_INTERWEAVING
            }
            else {
                pc = sched_warp_2->get_warppc();
                cur_warp = sched_warp_2;
                debug << "warp fetch " << j << " pc: " << pc << "\n";
                fetch_instruction = &sched_warp_2->get_warpparent()->get_funcIR()->at(pc);
                debug << "warp ins: " << j << " " << fetch_instruction->get_opcode() << "\n";
            }
#endif
            for(unsigned i=0; i<config::warp_size; i++){
                if(cur_warp->check_ith_thread(i)){
                    tid = cur_warp->get_threadid(i);
                    if(fetch_instruction->get_opcode() != SETP) {
                        switch(fetch_instruction->get_type1()){
                            case U16:
                            case S16:
                                if(fetch_instruction->get_opcode() == MUL) {
                                    debug << tid << " lock r_reg : " << fetch_instruction->get_reg_no(0) << "\n";
                                    cur_warp->get_warpparent()->get_r()->lock_reg(tid, fetch_instruction->get_reg_no(0));
                                } else {
                                    debug << tid << " lock rh_reg : " << fetch_instruction->get_reg_no(0) << "\n";
                                    cur_warp->get_warpparent()->get_rh()->lock_reg(tid, fetch_instruction->get_reg_no(0));
                                }
                                break;
                            case U32:
                            case S32:
                            case B32:
                                if(fetch_instruction->get_opcode() == MUL) {
                                    debug << tid << " lock rd_reg : " << fetch_instruction->get_reg_no(0) << "\n";
                                    cur_warp->get_warpparent()->get_rd()->lock_reg(tid, fetch_instruction->get_reg_no(0));
                                } else {
                                    debug << tid << " lock r_reg : " << fetch_instruction->get_reg_no(0) << "\n";
                                    cur_warp->get_warpparent()->get_r()->lock_reg(tid, fetch_instruction->get_reg_no(0));
                                }
                                break;
                            case U64:
                            case S64:
                            case B64:
                                debug << tid << " lock rd_reg : " << fetch_instruction->get_reg_no(0) << "\n";
                                cur_warp->get_warpparent()->get_rd()->lock_reg(tid, fetch_instruction->get_reg_no(0));
                                break;
                            case F32:
                                debug << tid << " lock f_reg : " << fetch_instruction->get_reg_no(0) << "\n";
                                cur_warp->get_warpparent()->get_f()->lock_reg(tid, fetch_instruction->get_reg_no(0));
                                break;
                            default:
                                break;
                        }
                    } else {
                        debug << tid << " lock p_reg : " << fetch_instruction->get_reg_no(0) << "\n";
                        cur_warp->get_warpparent()->get_p()->lock_reg(tid, fetch_instruction->get_reg_no(0));
                    }
                }
            }
        }
#if INTERWEAVING
        if(round == 2) {
            cur_warp->set_cur_instruction(fetch_instruction);
            cur_warp->set_warppc(pc+1);
            cur_warp->swap_simt();
        }
        pc = cur_warp->get_warppc();
        fetch_instruction = &cur_warp->get_warpparent()->get_funcIR()->at(pc);
#endif
#if WARP_INTERWEAVING
        if(round == 2) {
            sched_warp_2->set_cur_instruction(fetch_instruction);
            sched_warp_2->set_warppc(pc+1);
            fetch_warp_2 = sched_warp_2;
            debug << "fetch warp2: " << cur_warp->get_block_id() << " " << cur_warp->get_warpid() << "\n";
            cur_warp = temp_warp;
            pc = cur_warp->get_warppc();
            fetch_instruction = &cur_warp->get_warpparent()->get_funcIR()->at(pc);
        }
#endif
        cur_warp->set_cur_instruction(fetch_instruction);
#if PRINT_GRAPH
        if(cur_warp->get_block_id() == 0 && cur_warp->get_warpid() == 0)
            cur_warp->printGraph();
#endif
        cur_warp->set_warppc(pc+1);
        fetch_warp = cur_warp;
        debug << "fetch warp: " << cur_warp->get_block_id() << " " << cur_warp->get_warpid() << "\n";


    } else fetch_warp = NULL;
}

void stream_multiprocessor::decode(){
    if(IF_ID_queue.size()>0){
        debug << "-------------------------\n";
        debug << "decode\n";
        warp *next_warp = IF_ID_queue.front();
        
        /* This stage can check the dependency, and then decide to stall or not */
        stall = next_warp->check_dependency();
#if WARP_INTERWEAVING
        warp *next_warp_2 = NULL;
        decode_warp_2 = NULL;
        if(IF_ID_queue_2.size()>0) {
            next_warp_2 = IF_ID_queue_2.front();
            stall |= next_warp_2->check_dependency();
        }
#endif
        debug << "stall : " << stall << "\n";
        if(!stall){
            IF_ID_queue.pop();
            decode_warp = next_warp;
#if WARP_INTERWEAVING
            if(IF_ID_queue_2.size()>0) {
                IF_ID_queue_2.pop();
                decode_warp_2 = next_warp_2;
                debug << "decode warp2: " << next_warp_2->get_block_id() << " " << next_warp_2->get_warpid() << "\n";
            }
#endif
        }
        debug << "decode warp: " << next_warp->get_block_id() << " " << next_warp->get_warpid() << "\n";
    }
    else decode_warp = NULL;
}

bool stream_multiprocessor::exec(){
    if(ID_EX_queue.size() > 0) {
        debug << "-------------------------\n";
        warp *next_warp = ID_EX_queue.front();
        debug << "exec " << hex << next_warp->get_warpmask() << dec << "\n";
        for(unsigned i=0;i<config::sp_num; i++)
            sp_array[i].exec(next_warp,i + config::sp_num * exec_cycle);
#if INTERWEAVING
        if(next_warp->get_execuable()) {
            next_warp->swap_simt();
            debug << "second exec " << hex << next_warp->get_warpmask() << dec << "\n";
            for(unsigned i=0;i<config::sp_num; i++)
                sp_array[i].exec(next_warp,i + config::sp_num * exec_cycle);
            next_warp->swap_simt();
        }
#endif
#if WARP_INTERWEAVING
        if(exec_cycle == 0 && ID_EX_queue_2.size() > 0) get_warp = true;
        warp *next_warp_2 = NULL;
        exe_warp_2 = NULL;
        if(get_warp && ID_EX_queue_2.size() > 0){
            next_warp_2 = ID_EX_queue_2.front();
            debug << "second exec " << hex << next_warp_2->get_warpmask() << dec << "\n";
            assert((next_warp_2->get_warpmask() & next_warp->get_warpmask()) == 0);
            for(unsigned i=0;i<config::sp_num; i++)
                sp_array[i].exec(next_warp_2,i + config::sp_num * exec_cycle);
            debug << "execute warp2: " << next_warp_2->get_block_id() << " " << next_warp_2->get_warpid() << "\n";
        }
#endif
        exec_cycle++;
        if(exec_cycle == 4) {
            exe_warp = next_warp;
            ID_EX_queue.pop();
#if WARP_INTERWEAVING
            if(get_warp && ID_EX_queue_2.size() > 0) {
                exe_warp_2 = next_warp_2;
                ID_EX_queue_2.pop();
                debug << "execute warp2: " << next_warp_2->get_block_id() << " " << next_warp_2->get_warpid() << "\n";
                get_warp = false;
                if(next_warp_2->get_finish()) debug << "warp id: " << next_warp_2->get_block_id() << " " << next_warp_2->get_warpid() << " is finish\n";
            }
#endif
            /* this assume that all operation is single cycle. */
            exec_cycle = 0;
        }
        debug << "execute warp: " << next_warp->get_block_id() << " " << next_warp->get_warpid() << "\n";
        if(next_warp->get_finish()) debug << "warp id: " << next_warp->get_block_id() << " " << next_warp->get_warpid() << " is finish\n";
    } 
    else exe_warp = NULL;

    return !(exec_cycle == 0);
}

void stream_multiprocessor::memory(){
    if(EX_MEM_queue.size() > 0){
        debug << "-------------------------\n";
        debug << "memory\n";
        warp *next_warp = EX_MEM_queue.front();
        /* unlock the register file using fastpass */
#if INTERWEAVING
        unsigned round;
        if(next_warp->get_execuable()){
            round = 2;
        } else {
            round = 1;
        }
        for(unsigned j=0; j<round; j++) {
            if(j==1)
                next_warp->swap_simt();
#endif
#if WARP_INTERWEAVING
        unsigned round = 1;
        mem_warp_2 = NULL;
        if(EX_MEM_queue_2.size() > 0) {
            round = 2;
        }
        for(unsigned j=0; j<round; j++) {
            if(j==1)
                next_warp = EX_MEM_queue_2.front();
#endif
        instruction *inst = next_warp->get_cur_instruction();
        unsigned tid;
        for(unsigned i=0; i<config::warp_size; i++){
            if(next_warp->check_ith_thread(i)){
                tid = next_warp->get_threadid(i);
                if(inst->get_opcode() != SETP) {
                    switch(inst->get_type1()){
                        case U16:
                        case S16:
                            if(inst->get_opcode() == MUL) {
                                debug << tid << " unlock r_reg : " << inst->get_reg_no(0) << "\n";
                                next_warp->get_warpparent()->get_r()->unlock_reg(tid, inst->get_reg_no(0));
                            } else {
                                debug << tid << " unlock rh_reg : " << inst->get_reg_no(0) << "\n";
                                next_warp->get_warpparent()->get_rh()->unlock_reg(tid, inst->get_reg_no(0));
                            }
                            break;
                        case U32:
                        case S32:
                        case B32:
                            if(inst->get_opcode() == MUL) {
                                debug << tid << " unlock rd_reg : " << inst->get_reg_no(0) << "\n";
                                next_warp->get_warpparent()->get_rd()->unlock_reg(tid, inst->get_reg_no(0));
                            } else {
                                debug << tid << " unlock r_reg : " << inst->get_reg_no(0) << "\n";
                                next_warp->get_warpparent()->get_r()->unlock_reg(tid, inst->get_reg_no(0));
                            }
                            break;
                        case U64:
                        case S64:
                        case B64:
                            debug << tid << " unlock rd_reg : " << inst->get_reg_no(0) << "\n";
                            next_warp->get_warpparent()->get_rd()->unlock_reg(tid, inst->get_reg_no(0));
                            break;
                        case F32:
                            debug << tid << " unlock f_reg : " << inst->get_reg_no(0) << "\n";
                            next_warp->get_warpparent()->get_f()->unlock_reg(tid, inst->get_reg_no(0));
                            break;
                        default:
                            break;
                    }
                } else {
                    debug << tid << " unlock p_reg : " << inst->get_reg_no(0) << "\n";
                    next_warp->get_warpparent()->get_p()->unlock_reg(tid, inst->get_reg_no(0));
                }
            }
        }
#if INTERWEAVING
        }
        if(round == 2)
            next_warp->swap_simt();
#endif
#if WARP_INTERWEAVING
        }
        if(round == 2) {
            mem_warp_2 = next_warp;
            EX_MEM_queue_2.pop();
            debug << "memory warp2: " << next_warp->get_block_id() << " " << next_warp->get_warpid() << "\n";
            next_warp = EX_MEM_queue.front();
        }
#endif

        mem_warp = next_warp;
        EX_MEM_queue.pop();
        debug << "memory warp: " << next_warp->get_block_id() << " " << next_warp->get_warpid() << "\n";
    } else mem_warp = NULL;

}

void stream_multiprocessor::writeback(){
    if(MEM_WB_queue.size()>0){
        debug << "-------------------------\n";
        debug << "writeback\n";
        warp *next_warp = MEM_WB_queue.front();
        instruction *cur_instruction = next_warp->get_cur_instruction();
        class block *warp_p = next_warp->get_warpparent();

#if WARP_INTERWEAVING
        unsigned round = 1;
        if(MEM_WB_queue_2.size()>0) {
            round = 2;
        }
        for(unsigned j=0; j<round; j++) {
            if(j==1) {
                next_warp = MEM_WB_queue_2.front();
                cur_instruction = next_warp->get_cur_instruction();
                warp_p = next_warp->get_warpparent();
            }
#endif
        if(next_warp->get_cur_instruction()->get_opcode() == EXIT) {
            assert(next_warp->get_warpmask() == 0xffffffff);
            next_warp->set_finish(true);
        }
#if INTERWEAVING
        instruction *simt_instruction = next_warp->get_simt_instruction();
        if(next_warp->get_execuable() && simt_instruction->get_opcode() == BRA){
            uint32_t  simt_mask, simt_pc, jump_pc, mask1 = 0, mask2 = 0;
            next_warp->get_last_simt(&simt_pc, &simt_mask);
            jump_pc = label_name.find(simt_instruction->get_label())->second;
            if(simt_instruction->get_reg_no(0) != -1) {
                for(unsigned i=0; i<config::warp_size; i++) {
                    if((simt_mask&(1<<i)) && !warp_p->get_p()->get_value(next_warp->get_threadid(i), simt_instruction->get_reg_no(0)))
                        mask1 |= (1<<i);
                }
                mask2 = simt_mask ^ mask1;
                debug << "warp2: " << hex << mask1 << " " << mask2 << dec << " simt_pc = " << simt_pc << " jump_pc = " << jump_pc << "\n";
                if(mask2 != 0)
                    next_warp->split_warp(jump_pc, mask2);
                if(mask1 != 0)
                    next_warp->split_warp(simt_pc, mask1);
            } else {
                next_warp->split_warp(jump_pc, simt_mask);
            }
            next_warp->set_simt_instruction();
            next_warp->simt_sort();
        }
        next_warp->check_merge();
#endif
        /* set true for branch and give the mask of not active*/
        if(cur_instruction->get_opcode() == BRA) {
            if(cur_instruction->get_reg_no(0) != -1) {
                uint32_t _mask1 = 0, _mask2 = 0, pc;
                for(unsigned i=0; i<config::warp_size; i++) {
                    if(next_warp->check_ith_thread(i) && !warp_p->get_p()->get_value(next_warp->get_threadid(i), cur_instruction->get_reg_no(0))) {
                        _mask1 |= (1<<i);
                    }
                }
                /* _mask1 is not jump mask */
                _mask2 = next_warp->get_warpmask()^_mask1;
                pc = label_name.find(cur_instruction->get_label())->second;
                if(pc < next_warp->get_warppc()) {
                    debug << "set > pc = " << pc << " and mask = " << hex << _mask1 << " " << _mask2 << dec << "\n";
                    if(_mask1 != 0 && _mask2 != 0) {
                        next_warp->split_warp(next_warp->get_warppc(), _mask1);
                        next_warp->set_warppc(pc);
                        next_warp->set_warpmask(_mask2);
                    }
                    else if(_mask1 == 0) {
                        next_warp->get_min_simt(&pc, &_mask2);
                        next_warp->set_warppc(pc);
                        next_warp->set_warpmask(_mask2);
                    }
                }   
                else {
                    debug << "set < pc = " << pc << " and mask = " << hex << _mask1 << " " << _mask2 << dec << "\n";
                    if(_mask1 != 0 && _mask2 != 0){
                        next_warp->split_warp(pc, _mask2);
                        next_warp->set_warpmask(_mask1);
                    } else if(_mask1 == 0){
                        next_warp->get_min_simt(&pc, &_mask2);
                        next_warp->set_warppc(pc);
                        next_warp->set_warpmask(_mask2);
                    }
                }
            }
            else {
                uint32_t pc = label_name.find(cur_instruction->get_label())->second;
                uint32_t mask = next_warp->get_warpmask();
                debug << next_warp->get_block_id() << " " << next_warp->get_warpid() << " unconditional branch to " << hex << mask << " " << dec << pc << "\n";
                next_warp->get_min_simt(&pc, &mask);
                next_warp->set_warppc(pc);
                next_warp->set_warpmask(mask);
            }
            debug << next_warp->get_warpid() << " mask " << hex << next_warp->get_warpmask() << dec << " pc " << next_warp->get_warppc() << "\n";
        }
#if INTERWEAVING
        uint32_t pc = next_warp->get_warppc();
        uint32_t mask = next_warp->get_warpmask();
        next_warp->get_min_simt(&pc, &mask);
        next_warp->set_warppc(pc);
        next_warp->set_warpmask(mask);
#endif
#if WARP_INTERWEAVING
        }
        if(round == 2) {
            next_warp->merge_warp();
            next_warp->check();
            next_warp->set_state(false);
            MEM_WB_queue_2.pop();
            debug << "writeback warp2: " << next_warp->get_block_id() << " " << next_warp->get_warpid() << "\n";
            next_warp = MEM_WB_queue.front();;
        }
#endif
        next_warp->merge_warp();
        next_warp->check();
        next_warp->set_state(false);
        MEM_WB_queue.pop();
        debug << "writeback warp: " << next_warp->get_block_id() << " " << next_warp->get_warpid() << "\n";
    }
}


void stream_multiprocessor::clear_pipe(){
    while(!IF_ID_queue.empty()) IF_ID_queue.pop();
    while(!ID_EX_queue.empty()) ID_EX_queue.pop();
    while(!EX_MEM_queue.empty()) EX_MEM_queue.pop();
    while(!MEM_WB_queue.empty()) MEM_WB_queue.pop();
#if WARP_INTERWEAVING
    while(!IF_ID_queue_2.empty()) IF_ID_queue_2.pop();
    while(!ID_EX_queue_2.empty()) ID_EX_queue_2.pop();
    while(!EX_MEM_queue_2.empty()) EX_MEM_queue_2.pop();
    while(!MEM_WB_queue_2.empty()) MEM_WB_queue_2.pop();
#endif
}

void stream_multiprocessor::pipe(){
    if(fetch_warp)
        IF_ID_queue.push(fetch_warp);
    if(decode_warp)
        ID_EX_queue.push(decode_warp);
    if(exe_warp)
        EX_MEM_queue.push(exe_warp);
    if(mem_warp)
        MEM_WB_queue.push(mem_warp);
    fetch_warp = NULL;
    decode_warp = NULL;
    exe_warp = NULL;
    mem_warp = NULL;
#if WARP_INTERWEAVING
    if(fetch_warp_2)
        IF_ID_queue_2.push(fetch_warp_2);
    if(decode_warp_2)
        ID_EX_queue_2.push(decode_warp_2);
    if(exe_warp_2)
        EX_MEM_queue_2.push(exe_warp_2);
    if(mem_warp_2)
        MEM_WB_queue_2.push(mem_warp_2);
    fetch_warp_2 = NULL;
    decode_warp_2 = NULL;
    exe_warp_2 = NULL;
    mem_warp_2 = NULL;
#endif
}

void stream_multiprocessor::rm_warp(){
    /* if declare i as unsigned int, it will give an error of unsigned value of -1 */
    for(int i=(int)MT_IU.size()-1; i>=0; i--) {
        warp* check_warp = MT_IU.get_ith_element(i);
        if(check_warp->get_finish()) {
            debug << "curwarp: " << check_warp->get_warpid() << " b_id=" << check_warp->get_warpparent()->get_id() << "\n";
            for(unsigned j=0;j<MT_IU.size();j++){
                warp* x=MT_IU.get_ith_element(j);
                debug << x->get_warpid() << " ";
            }
            check_warp->rm_ref_count();
            thread_resource += 32;
            debug << "thread res: " << thread_resource << " ";
            debug << "\n----------\n";
            if(check_warp->get_warpparent()->get_ref_count() == 0) {
                check_warp->get_warpparent()->thread_reg_free();
                check_warp->get_warpparent()->thread_local_free();
            }
            MT_IU.rm_ith_element(i);
            for(unsigned j=0;j<MT_IU.size();j++){
                warp* x=MT_IU.get_ith_element(j);
                debug << x->get_warpid() << " ";
            }
            debug << "\n==========\n";
            //delete check_warp;
        }
    }
}

warp* stream_multiprocessor::schedule(){
    debug << "schedule\n";
    warp *next_warp;
    bool find = false;
    for(unsigned i=0; i<MT_IU.size(); i++){
#if WARP_INTERWEAVING
        next_warp = MT_IU.get_element();
#else
        next_warp = MT_IU.get_ith_element(i);
#endif
        //debug << "select: " << next_warp->get_block_id() << " " << next_warp->get_warpid() << " " << next_warp->get_state() << " " << next_warp->get_finish() << "\n";
        if(!next_warp->get_finish() && !next_warp->get_state() && !scoreboard(next_warp)) {
            find=true;
            break;
        }
    }
    if(find){
        next_warp->set_state(true);
        debug << "schedule warp: " << next_warp->get_block_id() << " " << next_warp->get_warpid() << "\n";
        debug << "-------------------------\n";
        return next_warp;
    }
    debug << "no warp to schedule\n";
    return NULL;
}

warp* stream_multiprocessor::schedule2(warp* primary_warp){
    debug << "schedule_2\n";
    warp *next_warp;
    bool find = false;
    for(unsigned i=0; i<MT_IU.size(); i++){
        next_warp = MT_IU.get_ith_element(i);
        //debug << "select: " << next_warp->get_block_id() << " " << next_warp->get_warpid() << " " << next_warp->get_state() << " " << next_warp->get_finish() << "\n";
        if(!next_warp->get_finish() && !next_warp->get_state() && !scoreboard(next_warp) && 
            (next_warp->get_warpmask()&primary_warp->get_warpmask()) == 0) {
            find=true;
            break;
        }   
    }   
    if(find){
        next_warp->set_state(true);
        debug << "schedule warp: " << next_warp->get_block_id() << " " << next_warp->get_warpid() << "\n";
        debug << "-------------------------\n";
        return next_warp;
    }   
    debug << "no warp to schedule_2\n";
    return NULL;
}

bool stream_multiprocessor::scoreboard(warp* w){
    uint32_t tid;
    instruction *inst = &w->get_warpparent()->get_funcIR()->at(w->get_warppc());
    debug << "scoreboard: " << w->get_warppc() <<  " " << inst->get_opcode() << " " << inst->get_type1() << "\n";
    for(unsigned i =0; i<config::warp_size; i++) {
        if(w->check_ith_thread(i)) {
            tid = w->get_threadid(i);
            if(inst->get_opcode() != SETP) {
                switch(inst->get_type1()) {
                    case U16:
                    case S16:
                        if(inst->get_opcode() == MUL) {
                            if(w->get_warpparent()->get_r()->islock_reg(tid, inst->get_reg_no(0)))
                                return true; 
                        }
                        else if(w->get_warpparent()->get_rh()->islock_reg(tid, inst->get_reg_no(0)))
                            return true;
                        break;
                    case U32: 
                    case S32:
                    case B32:
                        if(inst->get_opcode() == MUL) {
                            if(w->get_warpparent()->get_rd()->islock_reg(tid, inst->get_reg_no(0)))
                                return true;
                        }
                        else if(w->get_warpparent()->get_r()->islock_reg(tid, inst->get_reg_no(0)))
                            return true;
                        break;
                    case U64:
                    case S64:
                    case B64: 
                        if(w->get_warpparent()->get_rd()->islock_reg(tid, inst->get_reg_no(0)))
                            return true;
                        break;
                    case F32:
                        if(w->get_warpparent()->get_f()->islock_reg(tid, inst->get_reg_no(0)))
                            return true;
                        break;
                    case 299:  /* doesnot contain datatype */
                        break;
                    default:
                        cerr << "scoreboard does not handle this type " << inst->get_type1() << "\n" ;
                }
            } else if (w->get_warpparent()->get_p()->islock_reg(tid, inst->get_reg_no(0)))
                return true;
        }
    }
    return false;
}


////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
//                              virtual_gpu
//
////////////////////////////////////////////////////////////////////////////////////////////////////////////

virtual_gpu::virtual_gpu()
{
    _num_current_kernel = 0;
    all_exit = false;
    //_num_max_kernel = config::core_num / config::multiprocessor_num * 2; // ?
    // TODO: this is for tpc, not sm
    _num_max_kernel = 16;
}

void virtual_gpu::create_kernel(dim3 grid, dim3 block, int shmem_size)
{
    thread_per_block = block.x * block.y * block.z;
    debug << "thread_per_block " << thread_per_block << endl;
    kernel_state = new kernel_stat(grid, block, shmem_size);
}

void virtual_gpu::launch_kernel(const void *func)
{
    string kernel_name = function_address_map.find((int64_t*)func)->second;
    kernel_state->set_kernel_func(function_map.find( kernel_name )->second);
    kernel_state->set_kernel_alloc(allocation_map.find( kernel_name )->second);

    vector<uint64_t> *local_alloc = local_alloc_map.find( kernel_name )->second;
    if(local_alloc)
        kernel_state->set_kernel_local_alloc(local_alloc);

    kernel_scheduler.push_element(kernel_state);
    cycle_exec();
}

void virtual_gpu::cycle_exec()
{
    //TODO:  should execute other SM concurrently, maybe we can use semphore
    kernel_stat *ks = kernel_scheduler.get_element();
    warp *new_warp;
    class block * next_block;
    unsigned int j=0,counter = 0,thread_num = ks->get_block(0)->get_numofthreads();
    bool finish;
    debug << "start to exec\n";
    if(thread_num%config::warp_size != 0) 
        thread_num += config::warp_size - (thread_num % config::warp_size);

    do{
#if WATCHDOG
        if(counter == config::watchdog_max_num) {
            debug << "turn on CUDA watch dog1\n"; 
            abort();
        }
#endif
        debug << "===============================\n";
        debug << "cycle: " << dec <<  counter << "\n";
        debug << "===============================\n";
        for(unsigned i=0; i<config::multiprocessor_num && j<ks->get_num_of_block(); i++){
            if(_SM[i].get_thread_res() >= thread_num) {
                debug << "SM " << i << " has " << _SM[i].get_thread_res() << " create warp cost " << thread_num << " for " << j << " block\n";
                _SM[i].set_thread_res(thread_num);
                next_block = ks->get_block(j);
                ks->alloc_ith_block(j);
                for(unsigned k=0; k<next_block->get_numofthreads()/config::warp_size; k++) {
                    new_warp = new warp(config::warp_size, k, next_block);
                    _SM[i].push_warp(new_warp);
                }
                if((next_block->get_numofthreads() % config::warp_size) != 0) {
                    new_warp = new warp((next_block->get_numofthreads() % config::warp_size), (next_block->get_numofthreads()/config::warp_size)+1, next_block);
                    _SM[i].push_warp(new_warp);
                }
                j++;
            }
        }
        for(unsigned i = 0; i < config::multiprocessor_num; ++i)
            _SM[i].cycle_exec();
        counter++;
        if(j==ks->get_num_of_block()) { break; }
    }while(true);

    do{
        debug << "===============================\n";
        debug << "cycle: " << dec <<  counter << "\n";
        debug << "===============================\n";
#if WATCHDOG
        if(counter == config::watchdog_max_num) {
            debug << "turn on CUDA watch dog2\n";
            abort();
        }
#endif
        finish = true;
        for(unsigned i = 0; i < config::multiprocessor_num; ++i)
            finish &= _SM[i].cycle_exec();
        counter++;
    }while(!finish);

    cerr << "total cycles: " << counter << "\n";
    debug << "execution finish\n";
    clear();
    free_kernel();
}

void virtual_gpu::clear(){
    debug << "start to free memory\n";
    for(unsigned i = 0; i < config::multiprocessor_num; ++i)
        _SM[i].clear();
    debug << "SM clear\n";
}

void virtual_gpu::free_kernel(){
    debug << "free kernel\n";
    kernel_scheduler.clear();
    debug << "kernel\n";
}

bool virtual_gpu::get_status()
{
    return all_exit;
}
