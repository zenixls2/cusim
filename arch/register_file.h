#ifndef _REGISTER_FILE_H
#define _REGISTER_FILE_H
#include <stdint.h>
#include <vector>
#include <iostream>
#include <instruction.h>
#include <assert.h>

/*typedef int (instruction::*funcptr_r) (int);
typedef int (instruction::*funcptr_t1) (void);
typedef int (instruction::*funcptr_t2) (void);
class rno {
public:
    rno(int order) {
        ri = (is->*get_reg_index)(order);
        if(order == 1)
            t = (is->*get_type2)();
        else
            t = (is->*get_type1)();
    }
    int get_type() {
        return t;
    }
    int get_reg() {
        return ri;
    }
    static instruction* is;
    static funcptr_r get_reg_index;
    static funcptr_t1 get_type1;
    static funcptr_t2 get_type2;
private:
    int t;
    int ri;
};*/

template<class T>
class register_file{
public:
    register_file(int num_of_threads_per_block, int _size){
        size = _size;
        thread_num = num_of_threads_per_block;
        reg = new T*[thread_num];
        lock = new bool*[thread_num];
        addr = new bool*[thread_num];
        for(int i=0; i<thread_num; ++i) {
            reg[i] = new T[size];
            lock[i] = new bool[size];
            addr[i] = new bool[size];
            for(int j=0;j<size;j++){
                reg[i][j] = (T)0;
                lock[i][j] = false;
                addr[i][j] = false;
            }
        }
    }

    ~register_file(){
        for(int j=0; j<thread_num; ++j) {
            delete[] reg[j];
            delete[] lock[j];
            delete[] addr[j];
        }
        delete[] reg;
        delete[] lock;
        delete[] addr;
    }

    T get_value(int thread_id, int reg_no){ 
        assert(thread_id < thread_num && thread_id >= 0);
        assert(reg_no < size && size >= 0);
        return reg[thread_id][reg_no]; 
    }
    void set_value(int thread_id, int reg_no, T value){ 
        assert(thread_id < thread_num && thread_id >= 0);
        assert(reg_no < size && size >= 0);
        reg[thread_id][reg_no] = value; 
    }

    void lock_reg    (int thread_id, int reg_no){ 
        assert(thread_id < thread_num && thread_id >= 0);
        assert(reg_no < size && size >= 0);
        lock[thread_id][reg_no] = true; 
    }
    void unlock_reg  (int thread_id, int reg_no){ 
        assert(thread_id < thread_num && thread_id >= 0);
        assert(reg_no < size && size >= 0);
        lock[thread_id][reg_no] = false; 
    }

    bool islock_reg  (int thread_id, int reg_no){ 
        assert(thread_id < thread_num && thread_id >= 0);
        assert(reg_no < size && size >= 0);
        return lock[thread_id][reg_no]; 
    }
    void set_addr(int thread_id, int reg_no){ 
        assert(thread_id < thread_num && thread_id >= 0);
        assert(reg_no < size && size >= 0);
        addr[thread_id][reg_no] = true; 
    }
    void reset_addr(int thread_id, int reg_no){ 
        assert(thread_id < thread_num && thread_id >= 0);
        assert(reg_no < size && size >= 0);
        addr[thread_id][reg_no] = false; 
    }
    bool is_addr (int thread_id, int reg_no){ 
        assert(thread_id < thread_num && thread_id >= 0);
        assert(reg_no < size && size >= 0);
        return addr[thread_id][reg_no]; 
    }

private:
    int thread_num;
    int size;
    bool **lock;
    bool **addr;
    T **reg;
};

#endif /* _REGISTER_FILE_H */

