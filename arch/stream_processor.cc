#include <db.h>
#include <stream_processor.h>
#include <stdint.h>
#include <heading.h>
#include <assert.h>
#include <string.h>

extern dbout debug;

stream_processor::stream_processor() {}

stream_processor::~stream_processor() {}

void stream_processor::exec(warp *cur_warp, uint32_t ith) {
    assert(ith < 32 && cur_warp != NULL);
    instruction *cur_instruction = cur_warp->get_cur_instruction();
    cur_instruction->set_warp(cur_warp);
    cur_instruction->exec(ith);
}

#if 0
void stream_processor::exec(warp *cur_warp, uint32_t ith) {
    assert(ith < 32 && cur_warp != NULL);
    uint64_t a0,b0; /* rd */
    uint32_t a1,b1; /* r  */
    float a2,b2;    /* f  */
    int16_t a3,b3;  /* rh */
    int32_t a4,b4;  /* r  */
    int64_t a5,b5;  /* rd */
    uint16_t a6,b6; /* rh */

    instruction *cur_instruction = cur_warp->get_cur_instruction();
    cur_instruction->set_warp(cur_warp);
    //cur_instruction->exec(ith);
    block* warp_p = cur_warp->get_warpparent();
    int t1 = cur_instruction->get_type1();
    int t2 = cur_instruction->get_type2();
    if(cur_warp->check_ith_thread(ith)) {
        uint32_t tid = cur_warp->get_threadid(ith);
        switch(cur_instruction->get_opcode()) {                   
            case LD:
                switch(t1) {
                    case S32:
                        if(cur_instruction->get_state_space() == PARAM) {
                            int stack_pos = param_map.find( cur_instruction->get_addr_label() )->second;
                            debug << "LD " << *(int32_t*)args_stack[stack_pos].arg << " to " << cur_instruction->get_reg_no(0) << "\n";
                            warp_p->get_r()->set_value(tid, cur_instruction->get_reg_no(0), *((int32_t*)args_stack[stack_pos].arg));
                        } else if(cur_instruction->get_state_space() == GLOBAL || cur_instruction->get_state_space() == LOCAL){
                            a0 = warp_p->get_rd()->get_value(tid, cur_instruction->get_addr_reg());
                            a0 += cur_instruction->get_addr_offset();
                            debug << "LD " << *((int32_t*)a0) << " to " << cur_instruction->get_reg_no(0) << "\n";
                            warp_p->get_r()->set_value(tid, cur_instruction->get_reg_no(0), *((int32_t*)a0));
                        } else {
                             cerr << "unimplemented LD S32 type\n";
                        }
                        break;
                    case U64: {
                        //assume this parameter is array addressed by 64 bits
                        int stack_pos = param_map.find( cur_instruction->get_addr_label() )->second;
	    			    debug << "LD " << *(uint64_t*)args_stack[stack_pos].arg << " to " << cur_instruction->get_reg_no(0) << "\n";
                        warp_p->get_rd()->set_value(tid, cur_instruction->get_reg_no(0), *(uint64_t*)args_stack[stack_pos].arg);
                        break;
                    }
                    case F32:
                        if(cur_instruction->get_state_space() == PARAM){
                            cerr << "unimplemented LD F32 type\n";
                        } else {         
                            a0 = warp_p->get_rd()->get_value(tid, cur_instruction->get_addr_reg());
                            a0 += cur_instruction->get_addr_offset();
                            debug << "LD " << *((float*)a0) << " to " << cur_instruction->get_reg_no(0) << "\n";
                            warp_p->get_f()->set_value(tid, cur_instruction->get_reg_no(0), *((float*)a0));
                        }
                        break;
                    default:
                        cerr << "unimplemented type " << t1 << " LD\n";
                }
                break;
            case ST: {
                unsigned ss = cur_instruction->get_state_space();
                switch(ss) {
                    case GLOBAL:
                    case LOCAL:
                        switch(t1) {
                            case F32:
                                a2 = warp_p->get_f()->get_value(tid, cur_instruction->get_reg_no(0));
                                a0 = warp_p->get_rd()->get_value(tid, cur_instruction->get_addr_reg());
                                a0 += cur_instruction->get_addr_offset();
                                debug << "ST addr " << a0 << " value = " << a2 << "\n";
                                *((float*)a0) = a2;
                                break;
                            case S32:
                                a4 = warp_p->get_r()->get_value(tid, cur_instruction->get_reg_no(0));
                                a0 = warp_p->get_rd()->get_value(tid, cur_instruction->get_addr_reg());
                                a0 += cur_instruction->get_addr_offset();
                                debug << "ST addr " << a0 << " value = " << a4 << "\n";
                                *((int32_t*)a0) = a4;
                                break;
                            default:
                                cerr << "unimplemented type " << t1 << " ST\n";
                        }
                        break;
                    case SHARED:
                        break;
                    case CONSTANT:
                        cerr << "store to constant error\n";
                        assert(false);
                        break;
                    default:
                        cerr << "unimplemented storage space\n";
                }
                break;
            }
            case MOV:
                if(cur_instruction->get_special_reg() != -1) {
                    uint32_t spec_reg = get_special_reg(cur_warp,ith,cur_instruction->get_special_reg());
                    debug << "MV " << spec_reg << " to " << cur_instruction->get_reg_no(0) << "\n";
                    warp_p->get_rh()->set_value(tid, cur_instruction->get_reg_no(0), (uint16_t) spec_reg);
                }
                else if(cur_instruction->get_reg_no(1) == -1) {
                    switch(t1){
                        case U32:
                            a1 = cur_instruction->get_int();
                            debug << "MV " << a1 << " to " << cur_instruction->get_reg_no(0) << "\n";
                            warp_p->get_r()->set_value(tid, cur_instruction->get_reg_no(0), a1);
                        break;
                        case S32:
                            a4 = cur_instruction->get_int();
                            debug << "MV " << a4 << " to " << cur_instruction->get_reg_no(0) << "\n";
                            warp_p->get_r()->set_value(tid, cur_instruction->get_reg_no(0), a4);
                        break;
                        case U64: {
                            //string a = cur_instruction->get_addr_label();
                            //int index = local_map.find(cur_instruction->get_addr_label())->second;
                            cerr << "MV U64 unimplemented\n";
                        break;
                        }
                        case S64:
                            a5 = cur_instruction->get_int();
                            debug << "MV " << a5 << " to " << cur_instruction->get_reg_no(0) << "\n";
                            warp_p->get_rd()->set_value(tid, cur_instruction->get_reg_no(0), a5);
                        break;
                        default:
                            cerr << "MV unimplemented data type: " << t1 << "\n";
                    }
                }
                else if(cur_instruction->get_reg_no(1) != -1) {
                    switch(t1){
                        case U32:
                            a1 = warp_p->get_r()->get_value(tid, cur_instruction->get_reg_no(1));
                            debug << "MV " << a1 << " to " << cur_instruction->get_reg_no(0) << "\n";
                            warp_p->get_r()->set_value(tid, cur_instruction->get_reg_no(0), a1);
                        break;
                        case S32:
                            a4 = warp_p->get_r()->get_value(tid, cur_instruction->get_reg_no(1));
                            debug << "MV " << a4 << " to " << cur_instruction->get_reg_no(0) << "\n";
                            warp_p->get_r()->set_value(tid, cur_instruction->get_reg_no(0), a4);
                        break;
                        case S64:
                            a5 = warp_p->get_rd()->get_value(tid, cur_instruction->get_reg_no(1));
                            debug << "MV " << a5 << " to " << cur_instruction->get_reg_no(0) << "\n";
                            warp_p->get_rd()->set_value(tid, cur_instruction->get_reg_no(0), a5);
                        break;
                        default:
                            cerr << "MV unimplemented data type: " << t1 << "\n";
                    }
                }
                else
                {
                    cerr << "unimplemented MOV\n";
                }
                break;
            case ADD:
                switch(t1) {
                    case U64:
                        a0 = warp_p->get_rd()->get_value(tid, cur_instruction->get_reg_no(1));
                        if(cur_instruction->get_reg_no(2) != -1)
                            b0 = warp_p->get_rd()->get_value(tid, cur_instruction->get_reg_no(2));
                        else
                            b0 = (uint64_t)cur_instruction->get_int();
                        debug << "ADD " << a0 << "+" << b0 << " : from " << cur_instruction->get_reg_no(1)
                            << " and " << cur_instruction->get_reg_no(2) << " to " << cur_instruction->get_reg_no(0) << "\n";
                        warp_p->get_rd()->set_value(tid, cur_instruction->get_reg_no(0), a0+b0);
                        break;
                    case U32:
                        a1 = warp_p->get_r()->get_value(tid, cur_instruction->get_reg_no(1));
                        if(cur_instruction->get_reg_no(2) != -1)
                            b1 = warp_p->get_r()->get_value(tid, cur_instruction->get_reg_no(2));
                        else
                            b1 = (uint32_t)cur_instruction->get_int();
                        debug << "ADD " << a1 << "+" << b1 << " : from " << cur_instruction->get_reg_no(1)
                            << " and " << cur_instruction->get_reg_no(2) << " to " << cur_instruction->get_reg_no(0) << "\n";
                        warp_p->get_r()->set_value(tid, cur_instruction->get_reg_no(0), a1+b1);
                        break;
                    case S64:
                        a5 = warp_p->get_rd()->get_value(tid, cur_instruction->get_reg_no(1));
                        if(cur_instruction->get_reg_no(2) != -1)
                            b5 = warp_p->get_rd()->get_value(tid, cur_instruction->get_reg_no(2));
                        else
                            b5 = (int64_t)cur_instruction->get_int();
                        debug << "ADD " << a5 << "+" << b5 << " : from " << cur_instruction->get_reg_no(1)
                            << " and " << cur_instruction->get_reg_no(2) << " to " << cur_instruction->get_reg_no(0) << "\n";
                        warp_p->get_rd()->set_value(tid, cur_instruction->get_reg_no(0), a5+b5);
                        break;
                    case S32:
                        a4 = warp_p->get_r()->get_value(tid, cur_instruction->get_reg_no(1));
                        if(cur_instruction->get_reg_no(2) != -1)
                            b4 = warp_p->get_r()->get_value(tid, cur_instruction->get_reg_no(2));
                        else
                            b4 = (int32_t)cur_instruction->get_int();
                        debug << "ADD " << a4 << "+" << b4 << " : from " << cur_instruction->get_reg_no(1)
                            << " and " << cur_instruction->get_reg_no(2) << " to " << cur_instruction->get_reg_no(0) << "\n";
                        warp_p->get_r()->set_value(tid, cur_instruction->get_reg_no(0), a4+b4);
                        break;
                    case F32:
                        a2 = warp_p->get_f()->get_value(tid, cur_instruction->get_reg_no(1));
                        b2 = warp_p->get_f()->get_value(tid, cur_instruction->get_reg_no(2));
                        debug << "ADD " << a2 << "+" << b2 << " : from " << cur_instruction->get_reg_no(1)
                            << " and " << cur_instruction->get_reg_no(2) << " to " << cur_instruction->get_reg_no(0) << "\n";
                        warp_p->get_f()->set_value(tid, cur_instruction->get_reg_no(0), a2+b2);
                        break;
                    default:
                        cerr << "ADD unimplemented data type: " << t1 << "\n";
                }
                break;
            case SUB:
                switch(t1) {
                    case U64:
                        a0 = warp_p->get_rd()->get_value(tid, cur_instruction->get_reg_no(1));
                        if(cur_instruction->get_reg_no(2) != -1)
                            b0 = warp_p->get_rd()->get_value(tid, cur_instruction->get_reg_no(2));
                        else
                            b0 = (uint64_t)cur_instruction->get_int();
                        debug << "SUB " << a0 << "-" << b0 << " : from " << cur_instruction->get_reg_no(1)
                            << " and " << cur_instruction->get_reg_no(2) << " to " << cur_instruction->get_reg_no(0) << "\n";
                        warp_p->get_rd()->set_value(tid, cur_instruction->get_reg_no(0), a0-b0);
                        break;
                    case U32:
                        a1 = warp_p->get_r()->get_value(tid, cur_instruction->get_reg_no(1));
                        if(cur_instruction->get_reg_no(2) != -1)
                            b1 = warp_p->get_r()->get_value(tid, cur_instruction->get_reg_no(2));
                        else
                            b1 = (uint32_t)cur_instruction->get_int();
                        debug << "SUB " << a1 << "-" << b1 << " : from " << cur_instruction->get_reg_no(1)
                            << " and " << cur_instruction->get_reg_no(2) << " to " << cur_instruction->get_reg_no(0) << "\n";
                        warp_p->get_r()->set_value(tid, cur_instruction->get_reg_no(0), a1-b1);
                        break;
                    case S64:
                        a5 = warp_p->get_rd()->get_value(tid, cur_instruction->get_reg_no(1));
                        if(cur_instruction->get_reg_no(2) != -1)
                            b5 = warp_p->get_rd()->get_value(tid, cur_instruction->get_reg_no(2));
                        else
                            b5 = (int64_t)cur_instruction->get_int();
                        debug << "SUB " << a5 << "-" << b5 << " : from " << cur_instruction->get_reg_no(1)
                            << " and " << cur_instruction->get_reg_no(2) << " to " << cur_instruction->get_reg_no(0) << "\n";
                        warp_p->get_rd()->set_value(tid, cur_instruction->get_reg_no(0), a5-b5);
                        break;
                    case S32:
                        if(cur_instruction->get_p_reg() != -1 && !warp_p->get_p()->get_value(tid, cur_instruction->get_reg_no(cur_instruction->get_p_reg())))
                            break;
                        a4 = warp_p->get_r()->get_value(tid, cur_instruction->get_reg_no(1));
                        if(cur_instruction->get_reg_no(2) != -1)
                            b4 = warp_p->get_r()->get_value(tid, cur_instruction->get_reg_no(2));
                        else
                            b4 = (int32_t)cur_instruction->get_int();
                        debug << "SUB " << a4 << "-" << b4 << " : from " << cur_instruction->get_reg_no(1)
                            << " and " << cur_instruction->get_reg_no(2) << " to " << cur_instruction->get_reg_no(0) << "\n";
                        warp_p->get_r()->set_value(tid, cur_instruction->get_reg_no(0), a4-b4);
                        break;
                    case F32:
                        a2 = warp_p->get_f()->get_value(tid, cur_instruction->get_reg_no(1));
                        b2 = warp_p->get_f()->get_value(tid, cur_instruction->get_reg_no(2));
                        debug << "SUB " << a2 << "-" << b2 << " : from " << cur_instruction->get_reg_no(1)
                            << " and " << cur_instruction->get_reg_no(2) << " to " << cur_instruction->get_reg_no(0) << "\n";
                        warp_p->get_f()->set_value(tid, cur_instruction->get_reg_no(0), a2-b2);
                        break;
                    default:
                        cerr << "SUB unimplemented data type: " << t1 << "\n";
                }
                break;
            case CVT:
                if(t1 == U32 && t2 == U16) {
                    if(cur_instruction->get_special_reg() != -1) {
                        uint32_t spec_reg = get_special_reg(cur_warp, ith, cur_instruction->get_special_reg());
                        debug << "CVT " << spec_reg << " to " << cur_instruction->get_reg_no(0) << "\n";
                        warp_p->get_r()->set_value(tid, cur_instruction->get_reg_no(0), (uint32_t)spec_reg);
                    } else {
                        a6 = warp_p->get_rh()->get_value(tid, cur_instruction->get_reg_no(1));
                        debug << "CVT " << cur_instruction->get_reg_no(1) << " to " << cur_instruction->get_reg_no(0) 
                            << " value " << a6 << "\n";
                        warp_p->get_r()->set_value(tid, cur_instruction->get_reg_no(0), (uint32_t)a6);
                    }
                } else if(t1 == S64 && t2 == S32) {
                    a4 = warp_p->get_r()->get_value(tid, cur_instruction->get_reg_no(1));
                    debug << "CVT " << cur_instruction->get_reg_no(1) << " to " << cur_instruction->get_reg_no(0) 
                        << " value " << a4 << "\n";
                    warp_p->get_rd()->set_value(tid, cur_instruction->get_reg_no(0), (int64_t)a4);
                } else {
                    cerr << "CVT undefine exception: " << t1 << ", " << t2 << "\n";
                }
                break;
            case MUL:
                switch(t1) {
                    case U16:
                        a1 = warp_p->get_rh()->get_value(tid, cur_instruction->get_reg_no(1));
                        if(cur_instruction->get_reg_no(2) != -1)
                            b1 = warp_p->get_rh()->get_value(tid, cur_instruction->get_reg_no(2));
                        else
                            b1 = (uint32_t)cur_instruction->get_int();

                        if(cur_instruction->get_mul_type() == WIDE)
                            warp_p->get_r()->set_value(tid, cur_instruction->get_reg_no(0), (uint32_t)a1*b1);
                        else if(cur_instruction->get_mul_type() == LO)
                            warp_p->get_r()->set_value(tid, cur_instruction->get_reg_no(0), (uint32_t)((a1*b1)&0xffff));
                        else
                            warp_p->get_r()->set_value(tid, cur_instruction->get_reg_no(0), (uint32_t)((a1*b1)&0xffff0000 >> 16));
                            debug << "MUL " << a1 << "*" << b1 << " : from " << cur_instruction->get_reg_no(1) << " and " << cur_instruction->get_reg_no(2)
                                << " to " << cur_instruction->get_reg_no(0) << "\n";
                        break;
                    case U32:
                        a0 = warp_p->get_r()->get_value(tid, cur_instruction->get_reg_no(1));
                        if(cur_instruction->get_reg_no(2) != -1)
                            b0 = warp_p->get_r()->get_value(tid, cur_instruction->get_reg_no(2));
                        else
                            b0 = (uint64_t)cur_instruction->get_int();

                        if(cur_instruction->get_mul_type() == WIDE)
                            warp_p->get_rd()->set_value(tid, cur_instruction->get_reg_no(0), (uint64_t)a0*b0);
                        else if(cur_instruction->get_mul_type() == LO) 
                            warp_p->get_rd()->set_value(tid, cur_instruction->get_reg_no(0), (uint64_t)((a0*b0)&0xffffffff));
                        else
                            warp_p->get_rd()->set_value(tid, cur_instruction->get_reg_no(0), (uint64_t)((a0*b0)&0xffffffff00000000 >> 32));
                            debug << "MUL " << a0 << "*" << b0 << " : from " << cur_instruction->get_reg_no(1) << " and " << cur_instruction->get_reg_no(2)
                                 << " to " << cur_instruction->get_reg_no(0) << "\n";

                        break;
                    case S32:
                        a5 = warp_p->get_r()->get_value(tid, cur_instruction->get_reg_no(1));
                        if(cur_instruction->get_reg_no(2) != -1)
                            b5 = warp_p->get_r()->get_value(tid, cur_instruction->get_reg_no(2));
                        else
                            b5 = (int64_t)cur_instruction->get_int();

                        if(cur_instruction->get_mul_type() == WIDE)
                            warp_p->get_rd()->set_value(tid, cur_instruction->get_reg_no(0), (int64_t)a5*b5);
                        else if(cur_instruction->get_mul_type() == LO)
                            warp_p->get_rd()->set_value(tid, cur_instruction->get_reg_no(0), (int64_t)((a5*b5)&0xffffffff));
                        else
                            warp_p->get_rd()->set_value(tid, cur_instruction->get_reg_no(0), (int64_t)((a5*b5)&0xffffffff00000000 >> 32));
                            debug << "MUL " << a5 << "*" << b5 << " : from " << cur_instruction->get_reg_no(1) << " and " << cur_instruction->get_reg_no(2) << " to " << cur_instruction->get_reg_no(0) << "\n";
                        break;
                }
                break;
            case SETP:
                switch(t1) {
                    case S32:
                        a4 = warp_p->get_r()->get_value(tid, cur_instruction->get_reg_no(1));
                        b4 = warp_p->get_r()->get_value(tid, cur_instruction->get_reg_no(2));
                        warp_p->get_p()->set_value(tid, cur_instruction->get_reg_no(0), compare(cur_instruction->get_cmp_type(),a4,b4));
                        debug << "SETP cmp: S32 " << a4 << " " << b4 << ":" << compare(cur_instruction->get_cmp_type(),a4,b4) << " to " << cur_instruction->get_reg_no(0) << "\n";
                        break;
                    case S64:
                        a5 = warp_p->get_rd()->get_value(tid, cur_instruction->get_reg_no(1));
                        b5 = warp_p->get_rd()->get_value(tid, cur_instruction->get_reg_no(2));
                        warp_p->get_p()->set_value(tid, cur_instruction->get_reg_no(0), compare(cur_instruction->get_cmp_type(),a5,b5));
                        debug << "SETP cmp: S64 " << a5 << " " << b5 << ":" << compare(cur_instruction->get_cmp_type(),a5,b5) << " to " << cur_instruction->get_reg_no(0) << "\n";
                        break;
                    case U32:
                        a1 = warp_p->get_r()->get_value(tid, cur_instruction->get_reg_no(1));
                        b1 = warp_p->get_r()->get_value(tid, cur_instruction->get_reg_no(2));
                        warp_p->get_p()->set_value(tid, cur_instruction->get_reg_no(0), compare(cur_instruction->get_cmp_type(),a1,b1));
                        debug << "SETP cmp: U32 " << a1 << " " << b1 << ":" << compare(cur_instruction->get_cmp_type(),a1,b1) << " to " << cur_instruction->get_reg_no(0) << "\n";
                        break;
                    case U64:
                        a0 = warp_p->get_rd()->get_value(tid, cur_instruction->get_reg_no(1));
                        b0 = warp_p->get_rd()->get_value(tid, cur_instruction->get_reg_no(2));
                        warp_p->get_p()->set_value(tid, cur_instruction->get_reg_no(0), compare(cur_instruction->get_cmp_type(),a0,b0));
                        debug << "SETP cmp: U64 " << a0 << " " << b0 << ":" << compare(cur_instruction->get_cmp_type(),a0,b0) << " to " << cur_instruction->get_reg_no(0) << "\n";
                        break;
                    default:
                        cerr << "SETP unimplemented data type: " << t1 << "\n";
                        break;
                    }
                break;
            case SET:
                if(t1 == U32 && t2 == S32) {
                    a4 = warp_p->get_r()->get_value(tid, cur_instruction->get_reg_no(1));
                    b4 = warp_p->get_r()->get_value(tid, cur_instruction->get_reg_no(2));
                    if(compare(cur_instruction->get_cmp_type(),a4,b4)) {
                        warp_p->get_r()->set_value(tid, cur_instruction->get_reg_no(0),(uint32_t)0xffffffff);
                        debug << "SET U32 S32 " << a4 << " " << b4 << " value = 0xffffffff\n";
                    } else {
                        warp_p->get_r()->set_value(tid, cur_instruction->get_reg_no(0),(uint32_t)0x0);
                         debug << "SET U32 S32 " << a4 << " " << b4 << " value = 0x0\n";
                    }
                }
                else
                    cerr << "SET unimplemented data type: " << t1 << " and " << t2 << "\n";
                break;
	    	case BRA:
                debug << "BRA\n";
            case EXIT:
	    		break;
            case SHR:
                switch(t1) {
                    case S32:
                        a4 = warp_p->get_r()->get_value(tid, cur_instruction->get_reg_no(1));
                        b4 = (int32_t)cur_instruction->get_int();
                        warp_p->get_r()->set_value(tid, cur_instruction->get_reg_no(0), (int32_t)a4>>b4);
                        debug << "SHR: " << a4 << ">>" << b4 << " to " << cur_instruction->get_reg_no(0) << "\n";
                        break;
                    case S64:
                        a5 = warp_p->get_rd()->get_value(tid, cur_instruction->get_reg_no(1));
                        b5 = (int64_t)cur_instruction->get_int();
                        warp_p->get_rd()->set_value(tid, cur_instruction->get_reg_no(0), (int64_t)a5>>b5);
                        debug << "SHR: " << a5 << ">>" << b5 << " to " << cur_instruction->get_reg_no(0) << "\n";
                        break;
                    default:
                        cerr << "SHR unimplemented data type: " << t1 << "\n";
                    }
                break;
            case NEG:
                switch(t1) {
                    case S32:
                        a4 = warp_p->get_r()->get_value(tid, cur_instruction->get_reg_no(1));
                        warp_p->get_r()->set_value(tid, cur_instruction->get_reg_no(0), (int32_t)a4 * -1);
                        debug << "NEG: " << a4*-1 << " to " << cur_instruction->get_reg_no(0) << "\n";
                        break;
                    case S64:
                        a5 = warp_p->get_rd()->get_value(tid, cur_instruction->get_reg_no(1));
                        warp_p->get_rd()->set_value(tid, cur_instruction->get_reg_no(0), (int64_t)a5 * -1);
                        debug << "NEG: " << a5*-1 << " to " << cur_instruction->get_reg_no(0) << "\n";
                        break;
                    default:
                        cerr << "NEG unimplemented data type: " << t1 << "\n";
                    }
                break;
            case AND:
                switch(t1) {
                    case B32:
                        a1 = warp_p->get_r()->get_value(tid, cur_instruction->get_reg_no(1));
                        b1 = warp_p->get_r()->get_value(tid, cur_instruction->get_reg_no(2));
                        warp_p->get_r()->set_value(tid, cur_instruction->get_reg_no(0), (uint32_t)a1&b1);
                        debug << "AND: " << a1 << "&" << b1 << "=" << (a1&b1) << " to " << cur_instruction->get_reg_no(0) << "\n";
                    case B64:
                        a0 = warp_p->get_rd()->get_value(tid, cur_instruction->get_reg_no(1));
                        b0 = warp_p->get_rd()->get_value(tid, cur_instruction->get_reg_no(2));
                        warp_p->get_rd()->set_value(tid, cur_instruction->get_reg_no(0), (uint64_t)a0&b0);
                        debug << "AND: " << a0 << "&" << b0 << "=" << (a0&b0) << " to " << cur_instruction->get_reg_no(0) << "\n";
                        break;
                    default:
                        cerr << "AND unimplemented data type: " << t1 << "\n";
                    }
                break;
            case MAX:
                switch(t1) {
                    case S32:
                        a4 = warp_p->get_r()->get_value(tid, cur_instruction->get_reg_no(1));
                        b4 = warp_p->get_r()->get_value(tid, cur_instruction->get_reg_no(2));
                        if(a4 >= b4)
                            warp_p->get_r()->set_value(tid, cur_instruction->get_reg_no(0), (int32_t)a4);
                        else
                            warp_p->get_r()->set_value(tid, cur_instruction->get_reg_no(0), (int32_t)b4);
                        debug << "MAX: " << a4 << " " << b4 << " to " << cur_instruction->get_reg_no(0) << "\n";
                        break;
                    default:
                        cerr << "MAX unimplemented data type: " << t1 << "\n";
                }
                break;
            case MIN:
                switch(t1) {
                    case S32:
                        a4 = warp_p->get_r()->get_value(tid, cur_instruction->get_reg_no(1));
                        b4 = warp_p->get_r()->get_value(tid, cur_instruction->get_reg_no(2));
                        if(a4 >= b4)
                            warp_p->get_r()->set_value(tid, cur_instruction->get_reg_no(0), (int32_t)b4);
                        else
                            warp_p->get_r()->set_value(tid, cur_instruction->get_reg_no(0), (int32_t)a4);
                        debug << "MIN: " << a4 << " " << b4 << " to " << cur_instruction->get_reg_no(0) << "\n";
                        break;
                    default:
                        cerr << "MIN unimplemented data type: " << t1 << "\n";
                }
                break;
            case ABS:
                switch(t1) {
                    case S32:
                        a4 = warp_p->get_r()->get_value(tid, cur_instruction->get_reg_no(1));
                        if(a4 < 0)
                            warp_p->get_r()->set_value(tid, cur_instruction->get_reg_no(0), (int32_t)a4*-1);
                        else
                            warp_p->get_r()->set_value(tid, cur_instruction->get_reg_no(0), (int32_t)a4);
                        break;
                    default:
                        cerr << "ABS unimplemented data type: " << t1 << "\n";
                }
                break;
            default:
                cerr << "unimplemented opcode: " << cur_instruction->get_opcode() << "\n";
        }
    }
}
#endif
template <class T>
bool stream_processor::compare(int cmp_type, T v1, T v2){
    switch(cmp_type){
        case EQ:
            return (v1 == v2);
        case NE:
            return (v1 != v2);
        case LT:
            return (v1 < v2);
        case LE:
            return (v1 <= v2);
        case GT:
            return (v1 > v2);
        case GE:
            return (v1 >= v2);
        default:
            debug << "unknown compare type\n";
            return false;
    }
}

uint32_t stream_processor::get_special_reg(warp* cur_warp, unsigned thread_id, int type){
    uint32_t spec_reg;
    block* warp_p = cur_warp->get_warpparent();
    switch(type){
        case NTIDX:
            spec_reg = warp_p->get_block_dim().x;
            break;
        case NTIDY:
            spec_reg = warp_p->get_block_dim().y;
            break;
        case CTAIDX:
            spec_reg = warp_p->get_block_dimid().x;
            break;
        case CTAIDY:
            spec_reg = warp_p->get_block_dimid().y;
            break;
        case TIDX:
            spec_reg = warp_p->get_thread( cur_warp->get_threadid(thread_id) ).get_x();
            break;
        case TIDY:
            spec_reg = warp_p->get_thread( cur_warp->get_threadid(thread_id) ).get_y();
            break;
    }
    return spec_reg;
}

bool stream_processor::check_address(void* addr)
{
    for(unsigned i=0; i<address_map.size(); i++){
        if(address_map[i] == addr)
            return true;
    }
    return false;
}


