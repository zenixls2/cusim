#ifndef _VIRTUAL_H
#define _VIRTUAL_H
#include <config.h>
#include <db.h>
#include <kernel/kernel.h>
#include <stream_processor.h>
#include <queue>
#include <scheduler.h>

/* temp global header */
#include <heading.h>

/*
  This class implemented virtual gpu. It reads input and execute

  Streaming processor.

*/

class stream_multiprocessor {
public:
    stream_multiprocessor() {
        is_set = false;
        stall = false;
        sched_warp = NULL;
        fetch_warp = NULL;
        decode_warp = NULL;
        exe_warp = NULL;
        mem_warp = NULL;
#if WARP_INTERWEAVING
        get_warp = false;
#endif
        exec_cycle = 0;
        thread_resource = config::warp_size * config::block_size_per_sm;
    }

    bool get_state() {
        bool state = true;
        for(unsigned i = 0; i < MT_IU.size(); ++i)
            state |= MT_IU.get_element()->get_state();
        return state;
    }
    void push_warp(warp *w){ MT_IU.push_element(w); }
    void prepare_execute(){ all_finished = false; }
    void clear(){ 
        clear_pipe();
        MT_IU.clear(); 
    }

    /* implementation in virtual.cc */
    bool cycle_exec();
    warp* schedule();
    warp* schedule2(warp*);
    bool scoreboard(warp*);
    void rm_warp();
    unsigned int get_thread_res(){ return thread_resource; }
    void set_thread_res(unsigned int block_thread){ 
        thread_resource-=block_thread; 
    }
    void clear_pipe();
    void pipe();
    void fetch(warp *);
    void decode();
    bool exec();
    void memory();
    void writeback();

protected:
    stream_processor sp_array[config::sp_num];     // some call shader processor
    stream_processor sfu_array[config::sfu_num];    // special function unit
    char shared_memory[config::shared_mem_size_per_block];
    char i_cache[config::i_cache_size];
    char c_cache[config::c_cache_size];
    scheduler<warp*> MT_IU; // warp scheduler
private:
    unsigned int thread_resource;
    unsigned int exec_cycle;
    bool is_set;
    bool all_finished;
    bool stall;
    warp *sched_warp;
#if WARP_INTERWEAVING
    warp *sched_warp_2;
#endif
    queue< warp* > IF_ID_queue;
    queue< warp* > ID_EX_queue;
    queue< warp* > EX_MEM_queue;
    queue< warp* > MEM_WB_queue;
#if WARP_INTERWEAVING
    queue< warp* > IF_ID_queue_2;
    queue< warp* > ID_EX_queue_2;
    queue< warp* > EX_MEM_queue_2;
    queue< warp* > MEM_WB_queue_2;
#endif
    warp *fetch_warp;
    warp *decode_warp;
    warp *exe_warp;
    warp *mem_warp;
#if WARP_INTERWEAVING
    warp *fetch_warp_2;
    warp *decode_warp_2;
    warp *exe_warp_2;
    warp *mem_warp_2;
    bool get_warp;
#endif
};

class virtual_gpu {
public:
    void create_kernel(dim3, dim3, int);             //create kernel
    void launch_kernel(const void *func);       //launch kernel
    virtual_gpu();
    bool get_status();
    void clear();
    void free_kernel();
    
private:
    void work_distribution();
    void cycle_exec();
    // under this architecture, there's no concurrent execution ?
    // reference : http://www.nvidia.com/docs/IO/43395/NV_DS_Tesla_C2050_C2070_jul10_lores.pdf
    // https://devtalk.nvidia.com/default/topic/472332/c2050-simplestreams-performance-/
    kernel_stat *kernel_state;
    unsigned _num_current_kernel;
    unsigned _num_max_kernel;
    unsigned thread_per_block;
    stream_multiprocessor _SM[config::multiprocessor_num];
    scheduler<kernel_stat*> kernel_scheduler;
    bool all_exit;
};
#endif /* _VIRTUAL_H */
