#ifndef _INSTRUCTION_H
#define _INSTRUCTION_H
#include <opcode.h>
#include <string>
#include <warp.h>
#include <thread.h>
#include <typeinfo>
class warp;

class oper {
public:
    typedef void (oper::*callback_ptr)(uint32_t);
    oper() {
        handler[0] = &oper::voidtype;
        handler[1] = &oper::s64;
        handler[2] = &oper::s32;
        handler[3] = &oper::s16;
        handler[4] = &oper::s8;
        handler[5] = &oper::u64;
        handler[6] = &oper::u32;
        handler[7] = &oper::u16;
        handler[8] = &oper::u8;
        handler[9] = &oper::f64;
        handler[10] = &oper::f32;
        handler[11] = &oper::f16;
        handler[12] = &oper::b64;
        handler[13] = &oper::b32;
        handler[14] = &oper::b16;
        handler[15] = &oper::b8;
    }
    void exec(uint32_t tid) {
        (this->*handler[tp[0] - VOIDTYPE])(tid);
    }
    callback_ptr handler[16];
    type tp[2];        /* datatype 0: 1:*/
    state_space sp;    /* .local .global .param .shared .constant */
    int reg[4];
    int special_reg;    /* ctaid ntid tid */
    int mul_type;       /* .hi .lo .wide */
    int cmp_type;
    int is_p_reg;       /* reg[n] is %p reg header */
    int addr_reg;       /* assume %rd register */
    int int_value;
    std::string *addr_label;
    int addr_offset;
    std::string *label;
    unsigned uid;
    block *b;
    warp *w;
    std::string opername;
protected:
    virtual void assign_opername() { opername = typeid(*this).name()+2; }
    virtual void voidtype(uint32_t tid) {}
    virtual void s64(uint32_t tid) { std::cerr << opername << ": unimplemented type s64\n"; }
    virtual void s32(uint32_t tid) { std::cerr << opername << ": unimplemented type s32\n"; }
    virtual void s16(uint32_t tid) { std::cerr << opername << ": unimplemented type s16\n"; }
    virtual void s8(uint32_t tid) { std::cerr << opername << ": unimplemented type s8\n"; }
    virtual void u64(uint32_t tid) { std::cerr << opername << ": unimplemented type u64\n"; }
    virtual void u32(uint32_t tid) { std::cerr << opername << ": unimplemented type u32\n"; }
    virtual void u16(uint32_t tid) { std::cerr << opername << ": unimplemented type u16\n"; }
    virtual void u8(uint32_t tid) { std::cerr << opername << ": unimplemented type u8\n"; }
    virtual void f64(uint32_t tid) { std::cerr << opername << ": unimplemented type f64\n"; }
    virtual void f32(uint32_t tid) { std::cerr << opername << ": unimplemented type f32\n"; }
    virtual void f16(uint32_t tid) { std::cerr << opername << ": unimplemented type f16\n"; }
    virtual void b64(uint32_t tid) { std::cerr << opername << ": unimplemented type b64\n"; }
    virtual void b32(uint32_t tid) { std::cerr << opername << ": unimplemented type b32\n"; }
    virtual void b16(uint32_t tid) { std::cerr << opername << ": unimplemented type b16\n"; }
    virtual void b8(uint32_t tid) { std::cerr << opername<< ": unimplemented type b8\n"; }
};

class instruction {
public:
    instruction();     
    
    void set_warp(warp* t);
    void exec(int i);
    bool is_set();
    void clear();
    void set_opcode(int _opcode);
    int get_opcode();
    void set_label(std::string _label);
    std::string get_label();
    void set_type1(int _type);
    int get_type1();
    void set_type2(int _type);
    int get_type2();
    void set_state_space(int _state_space);
    int get_state_space();
    void set_mul_type(int _mul_type);
    int get_mul_type();
    void set_reg_no(int n, int value);
    int get_reg_no(int n);
    /* set ith reg is p reg */
    void set_p_reg(int n);
    int get_p_reg();
    void set_addr_reg(int _addr_reg);
    int get_addr_reg();
    void set_addr_offset(int _addr_offset);
    int get_addr_offset();
    void set_addr_label(std::string _addr_label);
    std::string get_addr_label();
    unsigned get_uid();
    int get_special_reg();
    void set_special_reg(int _special_reg);
    int get_int();
    void set_int(int _int_value);
    void set_cmp_type(int _cmp_type){
        cmp_type = _cmp_type;
    }
    int get_cmp_type(){
        return cmp_type;
    }
    static unsigned global_counter;
    static oper* handler[22];
protected:
    opcode op;
    type tp[2];        /* datatype 0: 1:*/
    state_space sp;    /* .local .global .param .shared .constant */
    int reg[4];
    int special_reg;    /* ctaid ntid tid */
    int mul_type;       /* .hi .lo .wide */
    int cmp_type;
    int is_p_reg;       /* reg[n] is %p reg header */
    int addr_reg;       /* assume %rd register */
    int int_value;
    std::string addr_label;
    int addr_offset;
    std::string label;
    bool set;
    unsigned uid;
    block *b;
    warp *w;
};

#endif /* INSTRUCTION_H */

