#ifndef _DB_H
#define _DB_H
#include <iostream>
#include <fstream>
namespace std {
class dbout : public ostream {
public:
#if _DEBUG
    dbout() : ostream(cout.rdbuf()){}
#else
    dbout(): ostream(init()) {
        fb.close();
    }
    filebuf* init() {
        fb.open("/dev/null", ios::out);
        return &fb;
    }
private:
    filebuf fb;
#endif
};
};
#endif /*__DB_H*/
