#ifndef _CONFIG_H
#define _CONFIG_H
namespace config
{
    #define _DEBUG 0
    #define PRINT_GRAPH 0
    #define WARP_INTERWEAVING 0
    #define INTERWEAVING 0
    #define WATCHDOG 0
    #define DUMP_INS 0
    const char device_name[] = "Tesla C2050";
    /* well, this is still under consideration, Tesla C1060 or C2050? */
    typedef const unsigned _cu;
    typedef const float _cf;
    #define undefined_num 0;
    _cu watchdog_max_cycle = 50000;
    _cu device_num = 4;
    _cu driver_version = 5050;
    _cu runtime_version = 3010;
    _cf revision_num = 2.0;
    _cu threads_per_block = 1024;
    _cu sp_num = 8;
    _cu sfu_num = 4;
    //_cu multiprocessor_num = 2;
    _cu multiprocessor_num = 14;
    _cu core_num = 112;
    _cu shared_mem_size_per_block = 49152;
    _cu i_cache_size = undefined_num;
    _cu c_cache_size = undefined_num;
    _cu mem_size = 65536;
    _cu warp_size = 32;
    _cu registers_per_block = 16384;
    _cu block_size_per_sm = 48;
    //_cu block_size_per_sm = 1;
}
#endif /* _CONFIG_H */
