#ifndef _STREAM_PROCESSOR_H
#define _STREAM_PROCESSOR_H
#include <warp.h>
class stream_processor{
public:
    stream_processor();
    ~stream_processor();
    uint32_t get_special_reg(warp *, unsigned, int type);
    void exec(warp*, uint32_t);
    bool check_address(void* addr);

    template <class T>
    bool compare(int cmp_type, T v1, T v2);
};

#endif /* _STREAM_PROCESSOR_H */
