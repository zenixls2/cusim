#include <instruction.h>
#include <assert.h>
#include <db.h>
extern dbout debug;
uint32_t get_special_reg(block* b, unsigned tid, int type)
{
    switch(type) {
        case NTIDX:
            return b->get_block_dim().x;
        case NTIDY:
            return b->get_block_dim().y;
        case CTAIDX:
            return b->get_block_dimid().x;
        case CTAIDY:
            return b->get_block_dimid().y;
        case TIDX:
            return b->get_thread(tid).get_x();
        case TIDY:
            return b->get_thread(tid).get_y();
        default:
            cerr << "undefined special reg type\n";
            assert(false);
            return 0;
    }
}
template <class T>
bool compare(int cmp_type, T v1, T v2){
    switch(cmp_type){
        case EQ:
            return (v1 == v2);
        case NE:
            return (v1 != v2);
        case LT:
            return (v1 < v2);
        case LE:
            return (v1 <= v2);
        case GT:
            return (v1 > v2);
        case GE:
            return (v1 >= v2);
        default:
            debug << "unknown compare type\n";
            return false;
    }
}

class _ld : public oper {
public:
    _ld() {
        oper();
        assign_opername();
    }
protected:
    virtual void s32(uint32_t tid) {
        switch(sp) {
            case PARAM: {
                int stack_pos = param_map[*addr_label];
                b->get_r()->set_value(tid, reg[0], *((int32_t*)args_stack[stack_pos].arg));
#if DUMP_INS
                if(w->get_block_id() == 1 && w->get_warpid() == 7)
                    cerr << "LD " << *(int32_t*)args_stack[stack_pos].arg << " to " << reg[0] << "\n";
#endif
                debug << "LD " << *(int32_t*)args_stack[stack_pos].arg << " to " << reg[0] << "\n";
                break;
            }
            case LOCAL:
            case GLOBAL: {
                uint64_t a = b->get_rd()->get_value(tid, addr_reg);
                a += addr_offset;
                b->get_r()->set_value(tid, reg[0], *(int32_t*)a);
#if DUMP_INS
                if(w->get_block_id() == 1 && w->get_warpid() == 7)
                    cerr << "LD " << *(int32_t*)a << " to " << reg[0] << "\n";
#endif
                debug << "LD " << *(int32_t*)a << " to " << reg[0] << "\n";
                break;
            }
            default:
                cerr << "unimplemented ld state space: " << sp << "\n";
                assert(false);
        }
    }
    virtual void u64(uint32_t tid) {
        switch(sp) {
            case PARAM: {
                int stack_pos = param_map[*addr_label];
                b->get_rd()->set_value(tid, reg[0], *((uint64_t*)args_stack[stack_pos].arg));
#if DUMP_INS
                if(w->get_block_id() == 1 && w->get_warpid() == 7)
                    cerr << "LD " << *(uint64_t*)args_stack[stack_pos].arg << " to " << reg[0] << "\n";
#endif
                debug << "LD " << *(uint64_t*)args_stack[stack_pos].arg << " to " << reg[0] << "\n";
                break;
            }
            default:
                cerr << "unimplemented ld state space: " << sp << "\n";
                assert(false);
        }
    }
    virtual void f32(uint32_t tid) {
        switch(sp) {
            case GLOBAL: {
                uint64_t a = b->get_rd()->get_value(tid, addr_reg);
                a += addr_offset;
                b->get_f()->set_value(tid, reg[0], *(float*)a);
#if DUMP_INS
                if(w->get_block_id() == 1 && w->get_warpid() == 7)
                    cerr << "LD " << *(float*)a << " to " << reg[0] << "\n";
#endif
                debug << "LD " << *(float*)a << " to " << reg[0] << "\n";
                break;
            }
            default:
                cerr << "unimplemented ld state space: " << sp << "\n";
                assert(false);
        }
    }
};
class _st : public oper {
public:
    _st() {
        oper();
        assign_opername();
    }
protected:
    virtual void f32(uint32_t tid) {
        float value = b->get_f()->get_value(tid, reg[0]);
        switch(sp) {
            case LOCAL:
            case GLOBAL: {
                uint64_t a = b->get_rd()->get_value(tid, addr_reg);
                a += addr_offset;
                *(float*)a = value;
#if DUMP_INS
                 if(w->get_block_id() == 1 && w->get_warpid() == 7)
                    cerr << "ST addr " << a << " value = " << value << "\n";
#endif
                debug << "ST addr " << a << " value = " << value << "\n";
                break;
            }
            case PARAM: {
                int stack_pos = param_map[*addr_label];
                *((float*)args_stack[stack_pos].arg) = value;
#if DUMP_INS
                if(w->get_block_id() == 1 && w->get_warpid() == 7)
                    cerr << "ST addr " << args_stack[stack_pos].arg << " value = " << value << "\n";
#endif
                debug << "ST addr " << args_stack[stack_pos].arg << " value = " << value << "\n";
                break;
            }
            case CONSTANT:
                cerr << "store to constant error\n";
                assert(false);
                break;
            default:
                cerr << "unimplemented st state space: " << sp << "\n";
        }
    }
    virtual void s32(uint32_t tid) {
        int32_t value = b->get_r()->get_value(tid, reg[0]);
        switch(sp) {
            case LOCAL:
            case GLOBAL: {
                uint64_t a = b->get_rd()->get_value(tid, addr_reg);
                a += addr_offset;
                *(int32_t*)a = value;
#if DUMP_INS
                if(w->get_block_id() == 1 && w->get_warpid() == 7)
                    cerr << "ST addr " << a << " value = " << value << "\n";
#endif
                debug << "ST addr " << a << " value = " << value << "\n";
                break;
            }
            case PARAM: {
                int stack_pos = param_map[*addr_label];
                *((int32_t*)args_stack[stack_pos].arg) = value;
#if DUMP_INS
                if(w->get_block_id() == 1 && w->get_warpid() == 7)
                    cerr << "ST addr " << args_stack[stack_pos].arg << " value = " << value << "\n";
#endif
                debug << "ST addr " << args_stack[stack_pos].arg << " value = " << value << "\n";
                break;
            }
            case CONSTANT:
                cerr << "store to constant error\n";
                assert(false);
                break;
            default:
                cerr << "unimplemented st state space: " << sp << "\n";
        }
    }
};
class _mov : public oper {
public:
    _mov() {
        oper();
        assign_opername();
    }
protected:
    virtual void f32(uint32_t tid) {
        uint32_t spec_reg;
        if(special_reg != -1) {
            spec_reg = (uint32_t)get_special_reg(b, tid, special_reg);
            b->get_rh()->set_value(tid, reg[0], spec_reg);
#if DUMP_INS
            if(w->get_block_id() == 1 && w->get_warpid() == 7)
                cerr << "MV " << spec_reg << " to " << reg[0] << "\n";
#endif
            debug << "MV " << spec_reg << " to " << reg[0] << "\n";
        } else {
            cerr << "MV unimplemented type f32\n";
        }
    }
    virtual void u16(uint32_t tid) {
        uint16_t value;
        uint32_t spec_reg;
        if(special_reg != -1) {
            spec_reg = (uint32_t)get_special_reg(b, tid, special_reg);
            b->get_rh()->set_value(tid, reg[0], spec_reg);
#if DUMP_INS
            if(w->get_block_id() == 1 && w->get_warpid() == 7)
                cerr << "MV " << spec_reg << " to " << reg[0] << "\n";
#endif
            debug << "MV " << spec_reg << " to " << reg[0] << "\n";
        } else if(reg[1] == -1) {
            value = (uint16_t)int_value;
            b->get_rh()->set_value(tid, reg[0], value);
#if DUMP_INS
            if(w->get_block_id() == 1 && w->get_warpid() == 7)
                cerr << "MV " << value << " to " << reg[0] << "\n";
#endif
            debug << "MV " << value << " to " << reg[0] << "\n";
        } else {
            value = b->get_rh()->get_value(tid, reg[1]);
            b->get_rh()->set_value(tid, reg[0], value);
#if DUMP_INS
            if(w->get_block_id() == 1 && w->get_warpid() == 7)
                cerr << "MV " << value << " to " << reg[0] << "\n";
#endif
            debug << "MV " << value << " to " << reg[0] << "\n";
        }
    }
    virtual void u32(uint32_t tid) {
        uint32_t value;
        if(special_reg != -1) {
            value = (uint32_t)get_special_reg(b, tid, special_reg);
            b->get_r()->set_value(tid, reg[0], value);
#if DUMP_INS
            if(w->get_block_id() == 1 && w->get_warpid() == 7)
                cerr << "MV " << value << " to " << reg[0] << "\n";
#endif
            debug << "MV " << value << " to " << reg[0] << "\n";
        } else if(reg[1] == -1) {
            value = int_value;
            b->get_r()->set_value(tid, reg[0], value);
#if DUMP_INS
            if(w->get_block_id() == 1 && w->get_warpid() == 7)
                cerr << "MV " << value << " to " << reg[0] << "\n";
#endif
            debug << "MV " << value << " to " << reg[0] << "\n";
        } else {
            value = b->get_r()->get_value(tid, reg[1]);
            b->get_r()->set_value(tid, reg[0], value);
#if DUMP_INS
            if(w->get_block_id() == 1 && w->get_warpid() == 7)
                cerr << "MV " << value << " to " << reg[0] << "\n";
#endif
            debug << "MV " << value << " to " << reg[0] << "\n";
        }
    }
    virtual void u64(uint32_t tid) {
        uint64_t value;
        if(addr_label->compare("") != 0) {
            int index_pos = local_map[*addr_label];
            b->get_rd()->set_value(tid, reg[0], (uint64_t) b->get_local(tid, index_pos));
#if DUMP_INS
            if(w->get_block_id() == 1 && w->get_warpid() == 7)
                cerr << "MV " << (uint64_t) b->get_local(tid, index_pos) << " to " << reg[0] << "\n";
#endif
            debug << "MV " << (uint64_t) b->get_local(tid, index_pos) << " to " << reg[0] << "\n";
        } else if(reg[1] == -1) {
            value = int_value;
            b->get_rd()->set_value(tid, reg[0], value);
#if DUMP_INS
            if(w->get_block_id() == 1 && w->get_warpid() == 7)
                cerr << "MV " << value << " to " << reg[0] << "\n";
#endif
            debug << "MV " << value << " to " << reg[0] << "\n";
        } else {
            value = b->get_rd()->get_value(tid, reg[1]);
            b->get_rd()->set_value(tid, reg[0], value);
#if DUMP_INS
            if(w->get_block_id() == 1 && w->get_warpid() == 7)
                cerr << "MV " << value << " to " << reg[0] << "\n";
#endif
            debug << "MV " << value << " to " << reg[0] << "\n";
        }
    }
    virtual void s64(uint32_t tid) {
        int64_t value;
        if(special_reg != -1) {
            value = (int32_t)get_special_reg(b, tid, special_reg);
            b->get_rh()->set_value(tid, reg[0], value);
#if DUMP_INS
            if(w->get_block_id() == 1 && w->get_warpid() == 7)
                cerr << "MV " << value << " to " << reg[0] << "\n";
#endif
            debug << "MV " << value << " to " << reg[0] << "\n";
        } else if (reg[1] == -1) {
            value = (int64_t)int_value;
            b->get_rd()->set_value(tid, reg[0], value);
#if DUMP_INS
            if(w->get_block_id() == 1 && w->get_warpid() == 7)
                cerr << "MV " << value << " to " << reg[0] << "\n";
#endif
            debug << "MV " << value << " to " << reg[0] << "\n";
        } else {
            value = b->get_rd()->get_value(tid, reg[1]);
            b->get_rd()->set_value(tid, reg[0], value);
#if DUMP_INS
            if(w->get_block_id() == 1 && w->get_warpid() == 7)
                cerr << "MV " << value << " to " << reg[0] << "\n";
#endif
            debug << "MV " << value << " to " << reg[0] << "\n";
        }
    }
    virtual void s32(uint32_t tid) {
        int32_t value;
        if(special_reg != -1) {
            value = (int32_t)get_special_reg(b, tid, special_reg);
            b->get_r()->set_value(tid, reg[0], value);
#if DUMP_INS
            if(w->get_block_id() == 1 && w->get_warpid() == 7)
                cerr << "MV " << value << " to " << reg[0] << "\n";
#endif
            debug << "MV " << value << " to " << reg[0] << "\n";
        } else if(reg[1] == -1) {
            value = int_value;
            b->get_r()->set_value(tid, reg[0], value);
#if DUMP_INS
            if(w->get_block_id() == 1 && w->get_warpid() == 7)
                cerr << "MV " << value << " to " << reg[0] << "\n";
#endif
            debug << "MV " << value << " to " << reg[0] << "\n";
        } else {
            value = b->get_r()->get_value(tid, reg[1]);
            b->get_r()->set_value(tid, reg[0], value);
#if DUMP_INS
            if(w->get_block_id() == 1 && w->get_warpid() == 7)
                cerr << "MV " << value << " to " << reg[0] << "\n";
#endif
            debug << "MV " << value << " to " << reg[0] << "\n";
        }
    }
};
class _add : public oper {
public:
    _add() {
        oper();
        assign_opername();
    }
protected:
    virtual void u64(uint32_t tid) {
        uint64_t v1 = b->get_rd()->get_value(tid, reg[1]);
        uint64_t v2 = (uint64_t)int_value;
        if(reg[2] != -1)
            v2 = b->get_rd()->get_value(tid, reg[2]);
        b->get_rd()->set_value(tid, reg[0], v1+v2);
#if DUMP_INS
        if(w->get_block_id() == 1 && w->get_warpid() == 7)
            cerr <<  "ADD " << v1 << "+" << v2 << " : from " << reg[1]
                << " and " << reg[2] << " to " << reg[0] << "\n";
#endif
        debug <<  "ADD " << v1 << "+" << v2 << " : from " << reg[1]
            << " and " << reg[2] << " to " << reg[0] << "\n";
    }
    virtual void u32(uint32_t tid) {
        uint32_t v1 = b->get_r()->get_value(tid, reg[1]);
        uint32_t v2 = (uint32_t)int_value;
        if(reg[2] != -1)
            v2 = b->get_r()->get_value(tid, reg[2]);
        b->get_r()->set_value(tid, reg[0], v1+v2);
#if DUMP_INS
        if(w->get_block_id() == 1 && w->get_warpid() == 7)
            cerr <<  "ADD " << v1 << "+" << v2 << " : from " << reg[1]
                << " and " << reg[2] << " to " << reg[0] << "\n";
#endif
        debug <<  "ADD " << v1 << "+" << v2 << " : from " << reg[1]
            << " and " << reg[2] << " to " << reg[0] << "\n";
    }
    virtual void u16(uint32_t tid) {
        uint16_t v1 = b->get_rh()->get_value(tid, reg[1]);
        uint16_t v2;
        if(reg[2] != -1)
            v2 = b->get_rh()->get_value(tid, reg[2]);
        else
            v2 = (uint16_t)int_value;
        b->get_rh()->set_value(tid, reg[0], v1+v2);
#if DUMP_INS
        if(w->get_block_id() == 1 && w->get_warpid() == 7)
            cerr <<  "ADD " << v1 << "+" << v2 << " : from " << reg[1]
                << " and " << reg[2] << " to " << reg[0] << "\n";
#endif
        debug <<  "ADD " << v1 << "+" << v2 << " : from " << reg[1]
            << " and " << reg[2] << " to " << reg[0] << "\n";
    }
    virtual void s64(uint32_t tid) {
        int64_t v1 = b->get_rd()->get_value(tid, reg[1]);
        int64_t v2;
        if(reg[2] != -1)
            v2 = b->get_rd()->get_value(tid, reg[2]);
        else
            v2 = (int64_t)int_value;
        b->get_rd()->set_value(tid, reg[0], v1+v2);
#if DUMP_INS
        if(w->get_block_id() == 1 && w->get_warpid() == 7)
            cerr <<  "ADD " << v1 << "+" << v2 << " : from " << reg[1]
                << " and " << reg[2] << " to " << reg[0] << "\n";
#endif
        debug <<  "ADD " << v1 << "+" << v2 << " : from " << reg[1]
            << " and " << reg[2] << " to " << reg[0] << "\n";
    }
    virtual void s32(uint32_t tid) {
        int32_t v1 = b->get_r()->get_value(tid, reg[1]);
        int32_t v2;
        if(reg[2] != -1)
            v2 = b->get_r()->get_value(tid, reg[2]);
        else
            v2 = (int32_t)int_value;
        b->get_r()->set_value(tid, reg[0], v1+v2);
#if DUMP_INS
        if(w->get_block_id() == 1 && w->get_warpid() == 7)
            cerr <<  "ADD " << v1 << "+" << v2 << " : from " << reg[1]
                << " and " << reg[2] << " to " << reg[0] << "\n";
#endif
        debug <<  "ADD " << v1 << "+" << v2 << " : from " << reg[1]
            << " and " << reg[2] << " to " << reg[0] << "\n";
    }
    virtual void s16(uint32_t tid) {
        int16_t v1 = b->get_rh()->get_value(tid, reg[1]);
        int16_t v2;
        if(reg[2] != -1)
            v2 = b->get_rh()->get_value(tid, reg[2]);
        else
            v2 = (int16_t)int_value;
        b->get_rh()->set_value(tid, reg[0], v1+v2);
#if DUMP_INS
        if(w->get_block_id() == 1 && w->get_warpid() == 7)
            cerr <<  "ADD " << v1 << "+" << v2 << " : from " << reg[1]
                << " and " << reg[2] << " to " << reg[0] << "\n";
#endif
        debug <<  "ADD " << v1 << "+" << v2 << " : from " << reg[1]
            << " and " << reg[2] << " to " << reg[0] << "\n";
    }
    virtual void f32(uint32_t tid) {
        float v1 = b->get_f()->get_value(tid, reg[1]);
        float v2 = b->get_f()->get_value(tid, reg[2]);
        b->get_f()->set_value(tid, reg[0], v1+v2);
#if DUMP_INS
        if(w->get_block_id() == 1 && w->get_warpid() == 7)
            cerr <<  "ADD " << v1 << "+" << v2 << " : from " << reg[1]
                << " and " << reg[2] << " to " << reg[0] << "\n";
#endif
        debug <<  "ADD " << v1 << "+" << v2 << " : from " << reg[1]
            << " and " << reg[2] << " to " << reg[0] << "\n";
    }
};
class _sub : public oper {
public:
    _sub() {
        oper();
        assign_opername();
    }
protected:
    virtual void u64(uint32_t tid) {
        uint64_t v1 = b->get_rd()->get_value(tid, reg[1]);
        uint64_t v2;
        if(reg[2] != -1)
            v2 = b->get_rd()->get_value(tid, reg[2]);
        else
            v2 = (uint64_t)int_value;
        b->get_rd()->set_value(tid, reg[0], v1-v2);
#if DUMP_INS
        if(w->get_block_id() == 1 && w->get_warpid() == 7)
            cerr <<  "SUB " << v1 << "-" << v2 << " : from " << reg[1]
                << " and " << reg[2] << " to " << reg[0] << "\n";
#endif
        debug <<  "SUB " << v1 << "-" << v2 << " : from " << reg[1]
            << " and " << reg[2] << " to " << reg[0] << "\n";
    }
    virtual void u32(uint32_t tid) {
        uint32_t v1 = b->get_r()->get_value(tid, reg[1]);
        uint32_t v2;
        if(reg[2] != -1)
            v2 = b->get_r()->get_value(tid, reg[2]);
        else
            v2 = (uint32_t)int_value;
        b->get_r()->set_value(tid, reg[0], v1-v2);
#if DUMP_INS
        if(w->get_block_id() == 1 && w->get_warpid() == 7)
            cerr <<  "SUB " << v1 << "-" << v2 << " : from " << reg[1]
                << " and " << reg[2] << " to " << reg[0] << "\n";
#endif
        debug <<  "SUB " << v1 << "-" << v2 << " : from " << reg[1]
            << " and " << reg[2] << " to " << reg[0] << "\n";
    }
    virtual void u16(uint32_t tid) {
        uint16_t v1 = b->get_rh()->get_value(tid, reg[1]);
        uint16_t v2;
        if(reg[2] != -1)
            v2 = b->get_rh()->get_value(tid, reg[2]);
        else
            v2 = (uint16_t)int_value;
        b->get_rh()->set_value(tid, reg[0], v1-v2);
#if DUMP_INS
        if(w->get_block_id() == 1 && w->get_warpid() == 7)
            cerr <<  "SUB " << v1 << "-" << v2 << " : from " << reg[1]
                << " and " << reg[2] << " to " << reg[0] << "\n";
#endif
        debug <<  "SUB " << v1 << "-" << v2 << " : from " << reg[1]
            << " and " << reg[2] << " to " << reg[0] << "\n";
    }
    virtual void s64(uint32_t tid) {
        int64_t v1 = b->get_rd()->get_value(tid, reg[1]);
        int64_t v2;
        if(reg[2] != -1)
            v2 = b->get_rd()->get_value(tid, reg[2]);
        else
            v2 = (int64_t)int_value;
        b->get_rd()->set_value(tid, reg[0], v1-v2);
#if DUMP_INS
        if(w->get_block_id() == 1 && w->get_warpid() == 7)
            cerr <<  "SUB " << v1 << "+" << v2 << " : from " << reg[1]
                << " and " << reg[2] << " to " << reg[0] << "\n";
#endif
        debug <<  "SUB " << v1 << "+" << v2 << " : from " << reg[1]
            << " and " << reg[2] << " to " << reg[0] << "\n";
    }
    virtual void s32(uint32_t tid) {
        if(is_p_reg != -1 && !b->get_p()->get_value(tid, reg[is_p_reg]))
            return;
        int32_t v1 = b->get_r()->get_value(tid, reg[1]);
        int32_t v2;
        if(reg[2] != -1)
            v2 = b->get_r()->get_value(tid, reg[2]);
        else
            v2 = (int32_t)int_value;
        b->get_r()->set_value(tid, reg[0], v1-v2);
#if DUMP_INS
        if(w->get_block_id() == 1 && w->get_warpid() == 7)
            cerr <<  "SUB " << v1 << "-" << v2 << " : from " << reg[1]
                << " and " << reg[2] << " to " << reg[0] << "\n";
#endif
        debug <<  "SUB " << v1 << "-" << v2 << " : from " << reg[1]
            << " and " << reg[2] << " to " << reg[0] << "\n";
    }
    virtual void s16(uint32_t tid) {
        int16_t v1 = b->get_rh()->get_value(tid, reg[1]);
        int16_t v2;
        if(reg[2] != -1)
            v2 = b->get_rh()->get_value(tid, reg[2]);
        else
            v2 = (int16_t)int_value;
        b->get_rh()->set_value(tid, reg[0], v1-v2);
#if DUMP_INS
        if(w->get_block_id() == 1 && w->get_warpid() == 7)
            cerr <<  "SUB " << v1 << "-" << v2 << " : from " << reg[1]
                << " and " << reg[2] << " to " << reg[0] << "\n";
#endif
        debug <<  "SUB " << v1 << "-" << v2 << " : from " << reg[1]
            << " and " << reg[2] << " to " << reg[0] << "\n";
    }
    virtual void f32(uint32_t tid) {
        float v1 = b->get_f()->get_value(tid, reg[1]);
        float v2 = b->get_f()->get_value(tid, reg[2]);
        b->get_rh()->set_value(tid, reg[0], v1-v2);
#if DUMP_INS
        if(w->get_block_id() == 1 && w->get_warpid() == 7)
            cerr <<  "SUB " << v1 << "-" << v2 << " : from " << reg[1]
                << " and " << reg[2] << " to " << reg[0] << "\n";
#endif
        debug <<  "SUB " << v1 << "-" << v2 << " : from " << reg[1]
            << " and " << reg[2] << " to " << reg[0] << "\n";
    }
};
class _and : public oper {
public:
    _and() {
        oper();
        assign_opername();
    }
protected:
    virtual void b32(uint32_t tid) {
        uint32_t v1 = b->get_r()->get_value(tid, reg[1]);
        uint32_t v2;
        if(reg[2] != -1)
            v2 = b->get_r()->get_value(tid, reg[2]);
        else
            v2 = (uint32_t)int_value;
        b->get_r()->set_value(tid, reg[0], (uint32_t)v1 & v2);
#if DUMP_INS
        if(w->get_block_id() == 1 && w->get_warpid() == 7)
            cerr << "AND: " << v1 << "&" << v2 << "=" << (v1&v2) << " to " << reg[0] << "\n";
#endif
        debug << "AND: " << v1 << "&" << v2 << "=" << (v1&v2) << " to " << reg[0] << "\n";
    }
    virtual void b64(uint32_t tid) {
        uint64_t v1 = b->get_rd()->get_value(tid, reg[1]);
        uint64_t v2;
        if(reg[2] != -1)
            v2 = b->get_rd()->get_value(tid, reg[2]);
        else
            v2 = (uint64_t)int_value;
        b->get_rd()->set_value(tid, reg[0], (uint64_t)v1 & v2);
#if DUMP_INS
        if(w->get_block_id() == 1 && w->get_warpid() == 7)
            cerr << "AND: " << v1 << "&" << v2 << "=" << (v1&v2) << " to " << reg[0] << "\n";
#endif
        debug << "AND: " << v1 << "&" << v2 << "=" << (v1&v2) << " to " << reg[0] << "\n";
    }
};
class _xor : public oper {
public:
    _xor() {
        oper();
        assign_opername();
    }
protected:
    virtual void b32(uint32_t tid) {
        uint32_t v1 = b->get_r()->get_value(tid, reg[1]);
        uint32_t v2;
        if(reg[2] != -1)
            v2 = b->get_r()->get_value(tid, reg[2]);
        else
            v2 = (uint32_t)int_value;
        b->get_r()->set_value(tid, reg[0], (uint32_t)v1 ^ v2);
#if DUMP_INS
        if(w->get_block_id() == 1 && w->get_warpid() == 7)
            cerr << "xor " << v1 << ", " << v2 << " to " << reg[0] << "\n";
#endif
    }
    virtual void b64(uint32_t tid) {
        uint64_t v1 = b->get_rd()->get_value(tid, reg[1]);
        uint64_t v2;
        if(reg[2] != -1)
            v2 = b->get_rd()->get_value(tid, reg[2]);
        else
            v2 = (uint64_t)int_value;
        b->get_rd()->set_value(tid, reg[0], (uint64_t)v1 ^ v2);
#if DUMP_INS
        if(w->get_block_id() == 1 && w->get_warpid() == 7)
            cerr << "xor " << v1 << ", " << v2 << " to " << reg[0] << "\n";
#endif
    }
};
class _neg : public oper {
public:
    _neg() {
        oper();
        assign_opername();
    }
protected:
    virtual void s32(uint32_t tid) {
        int32_t value = b->get_r()->get_value(tid, reg[1]);
        b->get_r()->set_value(tid, reg[0], value * -1);
#if DUMP_INS
        if(w->get_block_id() == 1 && w->get_warpid() == 7)
            cerr << "NEG " << value << " to " << reg[0] << "\n";
#endif
        debug << "NEG " << value << " to " << reg[0] << "\n";
    }
    virtual void s64(uint32_t tid) {
        int64_t value = b->get_rd()->get_value(tid, reg[1]);
        b->get_rd()->set_value(tid, reg[0], value * -1);
#if DUMP_INS
        if(w->get_block_id() == 1 && w->get_warpid() == 7)
            cerr << "NEG " << value << " to " << reg[0] << "\n";
#endif
        debug << "NEG " << value << " to " << reg[0] << "\n";
    }
};
class _shr : public oper {
public:
    _shr() {
        oper();
        assign_opername();
    }
protected:
    virtual void s32(uint32_t tid) {
        int32_t value = b->get_r()->get_value(tid, reg[1]);
        int32_t sh = (int32_t)int_value;
        b->get_r()->set_value(tid, reg[0], (int32_t) value >> sh);
#if DUMP_INS
        if(w->get_block_id() == 1 && w->get_warpid() == 7)
            cerr << "SHR " << value << " >> " << sh << " to " << reg[0] << "\n";
#endif
        debug << "SHR " << value << " >> " << sh << " to " << reg[0] << "\n";
    }
    virtual void s64(uint32_t tid) {
        int64_t value = b->get_rd()->get_value(tid, reg[1]);
        int64_t sh = (int64_t)int_value;
        b->get_rd()->set_value(tid, reg[0], (int64_t) value >> sh);
#if DUMP_INS
        if(w->get_block_id() == 1 && w->get_warpid() == 7)
            cerr << "SHR " << value << " >> " << sh << " to " << reg[0] << "\n";
#endif
        debug << "SHR " << value << " >> " << sh << " to " << reg[0] << "\n";
    }
};
class _shl : public oper {
public:
    _shl() {
        oper();
        assign_opername();
    }
protected:
};
class _min : public oper {
public:
    _min() {
        oper();
        assign_opername();
    }
protected:
    virtual void s32(uint32_t tid) {
        int32_t v1 = b->get_r()->get_value(tid, reg[1]);
        int32_t v2 = b->get_r()->get_value(tid, reg[2]);
        if(v1 >= v2)
            b->get_r()->set_value(tid, reg[0], (int32_t)v2);
        else
            b->get_r()->set_value(tid, reg[0], (int32_t)v1);
#if DUMP_INS
        if(w->get_block_id() == 1 && w->get_warpid() == 7)
            cerr << "MIN: " << v1 << " " << v2 << " to " << reg[0] << "\n";
#endif
        debug << "MIN: " << v1 << " " << v2 << " to " << reg[0] << "\n";
    }
};
class _max : public oper {
public:
    _max() {
        oper();
        assign_opername();
    }
protected:
    virtual void s32(uint32_t tid) {
        int32_t v1 = b->get_r()->get_value(tid, reg[1]);
        int32_t v2 = b->get_r()->get_value(tid, reg[2]);
        if(v1 >= v2)
            b->get_r()->set_value(tid, reg[0], (int32_t)v1);
        else
            b->get_r()->set_value(tid, reg[0], (int32_t)v2);
#if DUMP_INS
        if(w->get_block_id() == 1 && w->get_warpid() == 7)
            cerr << "MAX: " << v1 << " " << v2 << " to " << reg[0] << "\n";
#endif
        debug << "MAX: " << v1 << " " << v2 << " to " << reg[0] << "\n";
    }
};
class _abs : public oper {
public:
    _abs() {
        oper();
        assign_opername();
    }
protected:
    virtual void s32(uint32_t tid) {
        int32_t value = b->get_r()->get_value(tid, reg[1]);
        if (value < 0)
            value = -1 * value;
        b->get_r()->set_value(tid, reg[0], value);
#if DUMP_INS
        if(w->get_block_id() == 1 && w->get_warpid() == 7)
            cerr << "abs " << value << " to " << reg[0] << "\n";
#endif
    }
};
class _cvt : public oper {
public:
    _cvt() {
        oper();
        assign_opername();
    }
protected:
    virtual void u16(uint32_t tid) {
        uint16_t value;
        if(special_reg != -1) {
            value = get_special_reg(b, tid, special_reg);
        } else {
            switch (tp[1]) {
                case U32:
                    value = b->get_r()->get_value(tid, reg[1]);
                    break;
                default:
                    cerr << "cvt U16 not implemented type: " << tp[1] << "\n";
            }
        }
        b->get_rh()->set_value(tid, reg[0], value);
#if DUMP_INS
        if(w->get_block_id() == 1 && w->get_warpid() == 7)
            cerr << "CVT " << reg[1] << " to " << reg[0] << " value " << value << "\n";
#endif
        debug << "CVT " << reg[1] << " to " << reg[0] << " value " << value << "\n";
    }
    virtual void u32(uint32_t tid) {
        uint32_t value;
        if(special_reg != -1) {
            value = get_special_reg(b, tid, special_reg);
        } else {
            switch (tp[1]) {
                case U16:
                    value = b->get_rh()->get_value(tid, reg[1]);
                    break;
                default:
                    cerr << "cvt U32 not implemented type: " << tp[1] << "\n";
            }
        }
        b->get_r()->set_value(tid, reg[0], value);
#if DUMP_INS
        if(w->get_block_id() == 1 && w->get_warpid() == 7)
            cerr << "CVT " << reg[1] << " to " << reg[0] << " value " << value << "\n";
#endif
        debug << "CVT " << reg[1] << " to " << reg[0] << " value " << value << "\n";
    }
    virtual void s32(uint32_t tid) {
        int32_t value;
        if(special_reg != -1) {
            value = get_special_reg(b, tid, special_reg);
        } else {
            switch (tp[1]) {
                case U16:
                    value = b->get_rh()->get_value(tid, reg[1]);
                    break;
                default:
                    cerr << "cvt S32 not implemented type: " << tp[1] << "\n";
            }
        }
        b->get_r()->set_value(tid, reg[0], value);
#if DUMP_INS
        if(w->get_block_id() == 1 && w->get_warpid() == 7)
            cerr << "CVT " << reg[1] << " to " << reg[0] << " value " << value << "\n";
#endif
        debug << "CVT " << reg[1] << " to " << reg[0] << " value " << value << "\n";
    }
    virtual void s64(uint32_t tid) {
        int64_t value;
        if(special_reg != -1) {
            value = get_special_reg(b, tid, special_reg);
        } else {
            switch (tp[1]) {
                case S32:
                    value = b->get_r()->get_value(tid, reg[1]);
                    break;
                case S16:
                    value = b->get_rh()->get_value(tid, reg[1]);
                    break;
                default:
                    cerr << "cvt S64 not implemented type: " << tp[1] << "\n";
            }
        }
        b->get_rd()->set_value(tid, reg[0], value);
#if DUMP_INS
        if(w->get_block_id() == 1 && w->get_warpid() == 7)
            cerr << "CVT " << reg[1] << " to " << reg[0] << " value " << value << "\n";
#endif
        debug << "CVT " << reg[1] << " to " << reg[0] << " value " << value << "\n";
    }
    
};
class _mul : public oper {
public:
    _mul() {
        oper();
        assign_opername();
    }
protected:
    virtual void u16(uint32_t tid) {
        uint32_t v1 = b->get_rh()->get_value(tid, reg[1]);
        uint32_t v2;
        if(reg[2] != -1)
            v2 = b->get_rh()->get_value(tid, reg[2]);
        else
            v2 = (uint32_t)int_value;
        if(mul_type == WIDE) {
            b->get_r()->set_value(tid, reg[0], (uint32_t)v1 * v2);
#if DUMP_INS
            if(w->get_block_id() == 1 && w->get_warpid() == 7)
                cerr << "MUL " << v1 << "*" << v2 << "=" << b->get_r()->get_value(tid, reg[0]) << "from " << reg[1] << " and " << reg[2] << " to " << reg[0] << "\n";
#endif
        } else if(mul_type == LO) {
        //FIXME: b->get_rh() or b->get_r()
            b->get_rh()->set_value(tid, reg[0], (uint32_t)((v1 * v2) & 0xffff));
#if DUMP_INS
            if(w->get_block_id() == 1 && w->get_warpid() == 7)
                cerr << "MUL " << v1 << "*" << v2 << "=" << b->get_rh()->get_value(tid, reg[0]) << "from " << reg[1] << " and " << reg[2] << " to " << reg[0] << "\n";
#endif
        } else {
        //FIXME: b->get_rh() or b->get_r()
            b->get_rh()->set_value(tid, reg[0], (uint32_t)((v1 * v2) & 0xffff0000 >> 16));
#if DUMP_INS
            if(w->get_block_id() == 1 && w->get_warpid() == 7)
                cerr << "MUL " << v1 << "*" << v2 << "=" << b->get_rh()->get_value(tid, reg[0]) << "from " << reg[1] << " and " << reg[2] << " to " << reg[0] << "\n";
#endif
        }
        debug << "MUL " << v1 << " * " << v2 << "from " << reg[1] << " and " << reg[2] << " to " << reg[0] << "\n";
    }
    virtual void u32(uint32_t tid) {
        uint64_t v1 = b->get_r()->get_value(tid, reg[1]);
        uint64_t v2;
        if(reg[2] != -1)
            v2 = b->get_r()->get_value(tid, reg[2]);
        else
            v2 = (uint64_t)int_value;
        if(mul_type == WIDE) {
            b->get_rd()->set_value(tid, reg[0], (uint64_t)v1 * v2);
#if DUMP_INS
            if(w->get_block_id() == 1 && w->get_warpid() == 7)
                cerr << "MUL " << v1 << "*" << v2 << "=" << b->get_rd()->get_value(tid, reg[0]) << " from " << reg[1] << " and " << reg[2] << " to " << reg[0] << "\n";
#endif
        } else if(mul_type == LO) {
        //FIXME: b->get_rd() or b->get_r()
            b->get_r()->set_value(tid, reg[0], (uint64_t)((v1 * v2) & 0xffffffff));
#if DUMP_INS
            if(w->get_block_id() == 1 && w->get_warpid() == 7)
                cerr << "MUL " << v1 << "*" << v2 << "=" << b->get_r()->get_value(tid, reg[0]) << " from " << reg[1] << " and " << reg[2] << " to " << reg[0] << "\n";
#endif
        } else {
        //FIXME: b->get_rd() or b->get_r()
            b->get_r()->set_value(tid, reg[0], (uint64_t)((v1 * v2) & 0xffffffff00000000) >> 32);
#if DUMP_INS
            if(w->get_block_id() == 1 && w->get_warpid() == 7)
                cerr << "MUL " << v1 << "*" << v2 << "=" << b->get_r()->get_value(tid, reg[0]) << " from " << reg[1] << " and " << reg[2] << " to " << reg[0] << "\n";
#endif
        }
        debug << "MUL " << v1 << " * " << v2 << "from " << reg[1] << " and " << reg[2] << " to " << reg[0] << "\n";
    }
    virtual void s32(uint32_t tid) {
        int64_t v1 = b->get_r()->get_value(tid, reg[1]);
        int64_t v2;
        if(reg[2] != -1)
            v2 = b->get_r()->get_value(tid, reg[2]);
        else
            v2 = (int64_t)int_value;
        if(mul_type == WIDE) {
            b->get_rd()->set_value(tid, reg[0], (int64_t)v1 * v2);
#if DUMP_INS
            if(w->get_block_id() == 1 && w->get_warpid() == 7)
                cerr << "MUL " << v1 << "*" << v2 << "=" << b->get_rd()->get_value(tid, reg[0]) << "from " << reg[1] << " and " << reg[2] << " to " << reg[0] << "\n";
#endif
        } else if(mul_type == LO) {
        //FIXME: b->get_rd() or b->get_r()
            b->get_r()->set_value(tid, reg[0], (int64_t)((v1 * v2) & 0xffffffff));
#if DUMP_INS
            if(w->get_block_id() == 1 && w->get_warpid() == 7)
                cerr << "MUL " << v1 << "*" << v2 << "=" << b->get_r()->get_value(tid, reg[0]) << "from " << reg[1] << " and " << reg[2] << " to " << reg[0] << "\n";
#endif
        } else {
        //FIXME: b->get_rd() or b->get_r()
            b->get_r()->set_value(tid, reg[0], 
                ((int64_t)((v1 * v2) & 0xffffffff00000000)) * (1 - 2 * ((v1 < 0 && v2 > 0) || (v1 > 0 && v2 < 0))) >> 32);
#if DUMP_INS
            if(w->get_block_id() == 1 && w->get_warpid() == 7)
                cerr << "MUL " << v1 << "*" << v2 << "=" << b->get_r()->get_value(tid, reg[0]) << "from " << reg[1] << " and " << reg[2] << " to " << reg[0] << "\n";
#endif
        }
        debug << "MUL " << v1 << " * " << v2 << "from " << reg[1] << " and " << reg[2] << " to " << reg[0] << "\n";
    }
    
}; 
class _mul24 : public oper {
public:
    _mul24() {
        oper();
        assign_opername();
    }
};
class _set : public oper {
public:
    _set() {
        oper();
        assign_opername();
    }
protected:
    virtual void u32(uint32_t tid) {
        uint32_t v1 = b->get_r()->get_value(tid, reg[1]);
        uint32_t v2 = b->get_r()->get_value(tid, reg[2]);
        if(compare(cmp_type, v1, v2)) {
            b->get_r()->set_value(tid, reg[0], (uint32_t)0xffffffff);
#if DUMP_INS
            if(w->get_block_id() == 1 && w->get_warpid() == 7)
                cerr << "SET " << v1 << " " << v2 << " value 0xffffffff\n";
#endif
            debug << "SET " << v1 << " " << v2 << " value 0xffffffff\n";
        } else {
            b->get_r()->set_value(tid, reg[0], (uint32_t)0x0);
#if DUMP_INS
            if(w->get_block_id() == 1 && w->get_warpid() == 7)
                cerr << "SET " << v1 << " " << v2 << " value 0x0\n";
#endif
            debug << "SET " << v1 << " " << v2 << " value 0x0\n";
        }
    }
};
class _selp : public oper {
public:
    _selp() {
        oper();
        assign_opername();
    }
protected:
};
class _setp : public oper {
public:
    _setp() {
        oper();
        assign_opername();
    }
protected:
    virtual void s32(uint32_t tid) {
        int32_t v1 = b->get_r()->get_value(tid, reg[1]);
        int32_t v2 = b->get_r()->get_value(tid, reg[2]);
        b->get_p()->set_value(tid, reg[0], compare(cmp_type, v1, v2));
#if DUMP_INS
        if(w->get_block_id() == 1 && w->get_warpid() == 7)
            cerr << "SETP type" << cmp_type << " cmp " << v1 << " " << v2 << ": " << compare(cmp_type, v1, v2) << "\n";
#endif
        debug << "SETP cmp " << v1 << " " << v2 << ": " << compare(cmp_type, v1, v2) << "\n";
    }
    virtual void s64(uint32_t tid) {
        int64_t v1 = b->get_rd()->get_value(tid, reg[1]);
        int64_t v2 = b->get_rd()->get_value(tid, reg[2]);
        b->get_p()->set_value(tid, reg[0], compare(cmp_type, v1, v2));
#if DUMP_INS
        if(w->get_block_id() == 1 && w->get_warpid() == 7)
            cerr << "SETP type" << cmp_type << " cmp " << v1 << " " << v2 << ": " << compare(cmp_type, v1, v2) << "\n";
#endif
        debug << "SETP cmp " << v1 << " " << v2 << ": " << compare(cmp_type, v1, v2) << "\n";
    }
    virtual void u32(uint32_t tid) {
        uint32_t v1 = b->get_r()->get_value(tid, reg[1]);
        uint32_t v2 = b->get_r()->get_value(tid, reg[2]);
        b->get_p()->set_value(tid, reg[0], compare(cmp_type, v1, v2));
#if DUMP_INS
        if(w->get_block_id() == 1 && w->get_warpid() == 7)
            cerr << "SETP type" << cmp_type << " cmp " << v1 << " " << v2 << ": " << compare(cmp_type, v1, v2) << "\n";
#endif
        debug << "SETP cmp " << v1 << " " << v2 << ": " << compare(cmp_type, v1, v2) << "\n";
    }
    virtual void u64(uint32_t tid) {
        uint64_t v1 = b->get_rd()->get_value(tid, reg[1]);
        uint64_t v2 = b->get_rd()->get_value(tid, reg[2]);
        b->get_p()->set_value(tid, reg[0], compare(cmp_type, v1, v2));
#if DUMP_INS
        if(w->get_block_id() == 1 && w->get_warpid() == 7)
            cerr << "SETP type" << cmp_type << " cmp " << v1 << " " << v2 << ": " << compare(cmp_type, v1, v2) << "\n";
#endif
        debug << "SETP cmp " << v1 << " " << v2 << ": " << compare(cmp_type, v1, v2) << "\n";
    }
};
class _bra : public oper {
public:
    _bra() {
        oper();
        assign_opername();
    }
    virtual void voidtype(uint32_t tid) {
#if DUMP_INS
        if(w->get_block_id() == 1 && w->get_warpid() == 7)
            cerr << "BRA " << label;
#endif
    }
protected:
};
class _exit : public oper {
public:
    _exit() {
        oper();
        assign_opername();
    }
    virtual void voidtype(uint32_t tid) {
#if DUMP_INS
        if(w->get_block_id() == 1 && w->get_warpid() == 7)
            cerr << "EXIT \n";
#endif
    }
protected:
};
class _sync : public oper {
public:
    _sync() {
        oper();
        assign_opername();
    }
protected:
};

unsigned instruction::global_counter = 0;
oper* instruction::handler[] = {
    new _ld(),
    new _st(),
    new _mov(),
    new _add(),
    new _sub(),
    new _and(),
    new _xor(),
    new _neg(),
    new _shr(),
    new _shl(),
    new _min(),
    new _max(),
    new _cvt(),
    new _mul(),
    new _mul24(),
    new _abs(),
    new _set(),
    new _selp(),
    new _setp(),
    new _bra(),
    new _exit(),
    new _sync()
};
instruction::instruction()
{
    tp[0] = VOIDTYPE;
    tp[1] = VOIDTYPE;
    reg[0] = -1;
    reg[1] = -1;
    reg[2] = -1;
    reg[3] = -1;
    special_reg = -1;
    sp = NULLSPACE;
    mul_type = 0;
    is_p_reg = -1;
    set = false;
    addr_reg = -1;
    addr_offset = 0;
    b = NULL;
    w = NULL;
}
void instruction::exec(int i)
{
    if(w->check_ith_thread(i)) {
        handler[op]->tp[0] = tp[0];
        handler[op]->tp[1] = tp[1];
        handler[op]->reg[0] = reg[0];
        handler[op]->reg[1] = reg[1];
        handler[op]->reg[2] = reg[2];
        handler[op]->reg[3] = reg[3];
        handler[op]->special_reg = special_reg;
        handler[op]->int_value = int_value;
        handler[op]->cmp_type = cmp_type;
        handler[op]->sp = sp;
        handler[op]->mul_type = mul_type;
        handler[op]->is_p_reg = is_p_reg;
        handler[op]->addr_label = &addr_label;
        handler[op]->label = &label;
        handler[op]->addr_reg = addr_reg;
        handler[op]->addr_offset = addr_offset;
        handler[op]->b = b;
        handler[op]->w = w;
        uint32_t tid = w->get_threadid(i);
        handler[op]->exec(tid);
    }
}
void instruction::set_warp(warp* t)
{
    w = t;
    b = w->get_warpparent();
}
bool instruction::is_set()
{
    return set;
}
void instruction::clear()
{
    set = false;
}

void instruction::set_opcode(int _opcode)
{
    op = (opcode)_opcode;
    set = true; 
}
int instruction::get_opcode()
{
    return op;
}
void instruction::set_label(std::string _label)
{
    label = _label;
}
std::string instruction::get_label()
{
    return label;
}
void instruction::set_type1(int _type)
{
    tp[0] = (type)_type;
}
int instruction::get_type1()
{
    return tp[0];
}
void instruction::set_type2(int _type)
{
    tp[1] = (type)_type;
}
int instruction::get_type2()
{
    return tp[1];
}
void instruction::set_state_space(int _state_space)
{
    sp = (state_space)_state_space;
}
int instruction::get_state_space()
{
    return sp;
}
void instruction::set_mul_type(int _mul_type)
{
    mul_type = _mul_type;
}
int instruction::get_mul_type()
{
    return mul_type;
}
void instruction::set_reg_no(int n, int value)
{
    reg[n] = value;
}
int instruction::get_reg_no(int n)
{
    return reg[n];
}
void instruction::set_p_reg(int n)
{
    is_p_reg = n;
}
int instruction::get_p_reg()
{
    return is_p_reg;
}
void instruction::set_addr_reg(int _addr_reg)
{
    addr_reg = _addr_reg;
}
int instruction::get_addr_reg()
{
    return addr_reg;
}
void instruction::set_addr_offset(int _addr_offset)
{
    addr_offset = _addr_offset;
}
int instruction::get_addr_offset()
{
    return addr_offset;
}
void instruction::set_addr_label(std::string _addr_label)
{
    addr_label = _addr_label;
}
std::string instruction::get_addr_label()
{
    return addr_label;
}
unsigned instruction::get_uid()
{
    return uid;
}
void instruction::set_special_reg(int _special_reg)
{
    special_reg = _special_reg;
}
int instruction::get_special_reg()
{
    return special_reg;
}
int instruction::get_int()
{
    return int_value;
}
void instruction::set_int(int _int_value)
{
    int_value = _int_value;
}
