#ifndef _SCHEDULER_H
#define _SCHEDULER_H
#include <vector>
#include <assert.h>
#include <iostream>
template<class T>
class scheduler {
public:
    scheduler() {
        index = 0;
    }
    ~scheduler() {
        
    }
    unsigned size() {
        return elements.size();
    }
    void push_element(T e) {
        elements.push_back(e);
    }
    /* use RR */
    T get_element() {
        assert(elements.size() > 0);
        if(index >= elements.size())
            index = 0;
        T e = elements[index++];
        return e;
    }
    /* use for warp with execution problem */
    T get_ith_element(unsigned ith) {
        assert(ith < elements.size());
        T e = elements[ith];
        return e;
    }

    void clear(){
        for(int i=0; i<elements.size(); ++i)
            if(elements[i])
                delete elements[i];
        elements.erase(elements.begin(), elements.end());
    }

    int empty(){
        return elements.empty();
    }
    void rm_ith_element(unsigned ith) {
        assert(ith>=0 && ith<elements.size());
        if(elements[ith])
            delete elements[ith];
        elements.erase(elements.begin()+ith);
    }

private:
    std::vector<T> elements;
    unsigned index;
};
#endif /* _SCHEDULER_H */
