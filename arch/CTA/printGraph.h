#ifndef _GRAPH_H_
#define _GRAPH_H_
#include <config.h>
#include <warp.h>
#include <vector>
#if INTERWEAVING
    #define PCNUM 2
#else
    #define PCNUM 1
#endif
class warp;
class simt;
class instruction;
class warpState{
public:
    warpState(){ isBranch = false; enable=true;}
    bool isBranch;
    bool enable;
    static const int length = PCNUM;
    unsigned branch[2],pc[2]; // for next issuing
    int Label[3];
    char color[9];
    void insertLabel(int num){
        for(int i = 0;i < length+1;i++)
            Label[i] = num + i;
    }
    void insertPC(unsigned a, unsigned b){
        pc[0] = a;
        pc[1] = b;
    }
    void insertBranch(unsigned num, unsigned pc){
        isBranch = true;
        branch[num] = pc;
    }
    void closeBranch(int num){
        branch[num] = -1;
    }
    void setenable(bool b){
        enable = b;
    }
};

class printHelper{
public:
    printHelper(){
        lenSIMT = init = labelCount = 0;
        current = new warpState();
        previous = new warpState();
    }
    ~printHelper(){
         delete current;
         delete previous;
         lenSIMT = init = 0;
    }
    int labelCount;
    int init;
    FILE* fp;
    std::vector<int> first;
    std::vector<int> second;
    std::vector<char> resultColor;

    int lenSIMT;
    char colors[10];
    uint32_t prevSIMT[64];
    warpState *current,*previous;

    void pushLabel(bool result[], int len){
        const int offset = PCNUM + 1;
        for(int i=0;i<len;i++)
            if(result[i]){
                first.push_back(previous->Label[i / offset]);
                second.push_back(current->Label[i % offset]);
                resultColor.push_back(colors[i]);
            }
    }
    void swapState(){
        warpState *tmp = previous;
        previous = current;
        current = tmp;
    }

};


class printResolver{
public:
    ~printResolver(){ endProgram(); }
    printHelper printHelp;
    void initialize(FILE**fp, warp*);
    void printGraph(warp* cur_warp);
    void printStack(FILE** fp, std::vector<simt>& simt_stack, int& labelCount, bool);
    int printInstruction(FILE**fp, instruction* ins, int _pc, int num);
    void copySIMT(std::vector<simt>&, uint32_t*, int*);
    int compareSIMT(unsigned, std::vector<simt>&);
    //int compareSIMT(unsigned, std::vector<simt>&,bool);
    int comparePC(unsigned _pc, unsigned *s, int len, int offset);
    void checkArrow(bool arrow[], std::vector<simt>& simt_stack, bool,bool);
    void endProgram();
private:
    warp* _warp;
    int stackTrace;
    bool arrow[9];
};
#endif
