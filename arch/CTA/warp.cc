#include <warp.h>
using namespace std;
/* input active mask */
//extern class printResolver resolve;
void warp::split_warp(uint32_t pc, uint32_t mask)
{
    debug << "push " << pc << " mask " << hex << mask << dec << "\n";
    bool find = false;
    for(unsigned i=0; i<simt_stack.size(); i++) {
        if(pc == simt_stack[i].pc) {
            simt_stack[i].mask |= mask;
            find = true;
            break;
        }
    }
    if(!find)
        simt_stack.push_back(simt(pc, mask));
}

void warp::get_min_simt(uint32_t *pc, uint32_t *mask) {
    if(simt_stack.size() > 0) {
    split_warp(*pc, *mask);
    int index = 0;
    for(unsigned i=1; i<simt_stack.size(); i++)
        if(simt_stack[index].pc > simt_stack[i].pc)
            index = i;
    *pc = simt_stack[index].pc;
    *mask = simt_stack[index].mask;
    simt_stack.erase (simt_stack.begin() + index);
    simt_sort();
    debug << "simt stack:\n" ;
    for(unsigned i=0; i<simt_stack.size(); i++)
        debug << i << ": pc " << simt_stack[i].pc << " mask " << hex << simt_stack[i].mask << dec << "\n"; 
    }
}

void warp::get_last_simt(uint32_t *pc, uint32_t *mask) {
    *pc = simt_stack[simt_stack.size()-1].pc;
    *mask = simt_stack[simt_stack.size()-1].mask;
    simt_stack.pop_back();
}

void warp::check(){
    for(unsigned i=0; i<simt_stack.size(); i++)
        for(unsigned j=i+1; j<simt_stack.size(); j++)
            assert(simt_stack[i].pc > simt_stack[j].pc);
}

void warp::swap_simt() {
    swap(_pc, simt_stack[simt_stack.size()-1].pc);
    swap(_mask, simt_stack[simt_stack.size()-1].mask);
    swap(cur_instruction, simt_instruction);
}

void warp::simt_sort(){
    sort(simt_stack.begin(), simt_stack.end());
}

bool warp::simt_issuable(){
    coissue = false;
    if(simt_stack.size() == 0)
        return false;
    instruction *inst;
    inst = &_parent->get_funcIR()->at(simt_stack[simt_stack.size()-1].pc);
    if(inst->get_opcode() != EXIT) {
        coissue = true;
        return true;
    }
    return false;
}

void warp::merge_warp()
{
    for(int i=simt_stack.size()-1; i>=0; i--)
        if(simt_stack[i].pc == _pc) {
            debug << "merge " << _pc << " mask " << hex << simt_stack[i].mask << dec << "\n";
            _mask |= simt_stack[i].mask;
            simt_stack.pop_back();
        }
        else break;
    debug << _id << " : " << hex << _mask << dec << " pc " << _pc << "\n";
}

void warp::check_merge()
{
    for(int i=simt_stack.size()-2; i>=0; i--)
        if(simt_stack[simt_stack.size()-1].pc == simt_stack[i].pc) {
            simt_stack[i].mask |= simt_stack[simt_stack.size()-1].mask;
            simt_stack.pop_back();
            break;
        }
}

void warp::rm_ref_count()
{
    _parent->rm_ref_count();
}

/* TODO: warp dependency function by checking the lock of register file when some opcode is not single cycle (future ILP)*/
bool warp::check_dependency(){
    return false;
}

void  warp::set_cur_instruction(instruction *ins)
{
    cur_instruction = ins;
}

void warp::set_simt_instruction(){
    simt_instruction = NULL;
}

instruction* warp::get_cur_instruction()
{
    return cur_instruction;
}

instruction* warp::get_simt_instruction()
{
    return simt_instruction;
}

unsigned warp::get_block_id()
{
    return _parent->get_id();
}

void warp::printGraph(){
#if PRINT_GRAPH
    resolve.printGraph(this);
#endif
}
