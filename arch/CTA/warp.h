#ifndef _CTA_WARP_
#define _CTA_WARP_

#include <iostream>
#include <vector>
#include <algorithm>
#include <config.h>
#include <instruction.h>
#include <stdint.h>
#include <thread.h>
#include <assert.h>
#include <printGraph.h>
class instruction;
class block;
class printResolver;
class simt{
    public:
    simt(uint32_t _pc,uint32_t _mask){
        pc =_pc;
        mask = _mask;
    }
    simt(){}
    bool operator<(const simt &s) { return pc >= s.pc; }
    bool operator==(const simt &s){ return pc == s.pc; }
    uint32_t pc;
    uint32_t mask;
};

class warp {
public:
    warp(int num_threads, int id, block *parent){
        _mask = 0;
        for(int i=0;i<num_threads;i++)
            _mask |= (1<<i);
        _pc = 0;
        _id = id;
        _parent = parent;
        state = false;
        finish = false;
        coissue = false;
    }
    bool get_state() { return state; }
    bool get_finish() { return finish; }
    bool get_execuable() { return coissue; }
    uint32_t get_warpid(){ return _id; }
    uint32_t get_warpmask(){ return _mask; }
    uint32_t get_warppc(){ return _pc; }
    uint32_t get_warpoffset(){  return _offset; }
    class block* get_warpparent(){ return _parent; }
    instruction* get_cur_instruction();
    instruction* get_simt_instruction();

    void set_state(bool status) { state = status; }
    void set_finish(bool status) { finish = status; }
    void set_warppc(uint32_t pc){ _pc = pc; }
    void set_warpmask(uint32_t mask){ _mask = mask; }
    void set_warpid(uint32_t id){ _id = id; }
    void set_warpoffset(uint32_t offset){ _offset = offset; }
    void set_cur_instruction(instruction *);
    void set_simt_instruction();

    void get_min_simt(uint32_t *pc, uint32_t *mask);
    void get_last_simt(uint32_t *pc, uint32_t *mask);
    void merge_warp();
    void split_warp(uint32_t, uint32_t);

    void check();
    void check_merge();
    bool check_dependency();

    bool simt_issuable();
    void simt_sort();
    void swap_simt();
    std::vector<simt>& get_simt_stack(){ return simt_stack; }

    void rm_ref_count();
    unsigned get_block_id();
    /* 0-31 */
    uint32_t get_threadid(uint32_t ith_bit){
        assert(ith_bit < 32);
        //cout << _id << "+" << ith_bit << endl;
        return 32*_id + ith_bit;
    } //should use check_ith_thread to check

    bool check_ith_thread(int ith){
        assert(ith < 32);
        if(_mask & (1<<ith))
            return true;
        return false;
    }

    void printGraph();
private:
    uint32_t _pc, _mask, _id, _offset;
    class block *_parent;
    instruction *cur_instruction;
    instruction *simt_instruction;
    bool state, finish, coissue;
    std::vector<simt> simt_stack;
    static printResolver resolve;
};

#endif
