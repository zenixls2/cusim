#include <printGraph.h>
#include <stdlib.h>
#if INTERWEAVING
    #define END_SIMT 1
    #define ARROW_LENGTH 9
#else
    #define END_SIMT 0
    #define ARROW_LENGTH 4
#endif

void printResolver::initialize(FILE**fp, warp* cur_warp){
    *fp = fopen("graph2.gv","w");
    fprintf(*fp,"digraph structs {\n\tnode [shape=record];\n\tgroup=\"pc:simt\"\n");
    _warp = cur_warp;
}

void printResolver::endProgram(){
    if(!printHelp.init) return;
    FILE**   fp              = &printHelp.fp;
    std::vector<int> &first  = printHelp.first;
    std::vector<int> &second = printHelp.second;
    std::vector<char> &color  = printHelp.resultColor;
    fprintf(*fp,"\ts1 [group=\"pc\",shape=record,label=PC_1,style=filled,color=lightgray]\n");
    if(PCNUM==2)
    fprintf(*fp,"\ts2 [group=\"pc\",shape=record,label=PC_2,style=filled,color=lightgray]\n");
    fprintf(*fp,"\ts3 [group=\"simt\",shape=record,label=SIMT,style=filled,color=lightgray]\n");

    fprintf(*fp,"\tsubgraph cluster%d{\n\tstyle=filled;\n\tcolor=white;\n",0);
    for(int m = 0; m < PCNUM + 1; m++){
        for(int i=m;i<(int)printHelp.labelCount;(i+=PCNUM+1)){
            fprintf(*fp,"x%d; ",i);
        }
        if(m!=2)
            fprintf(*fp,"\t}\n\tsubgraph cluster%d{\n\tstyle=filled;\n\tcolor=white;\n",m+1);
    }
    fprintf(*fp,"\t}\n");
    fprintf(*fp,"\ts1 -> x0[style=\"invis\"]\n");
    if(PCNUM==2){
    	fprintf(*fp,"\ts2 -> x1[style=\"invis\"]\n");
    	fprintf(*fp,"\ts3 -> x2[style=\"invis\"]\n");
    }else
    	fprintf(*fp,"\ts3 -> x1[style=\"invis\"]\n");

    for(unsigned i=0;i<first.size();i++){
        if(color[i] == 'b')
            fprintf(*fp,"\tx%d -> x%d\n",first[i], second[i]);
        else if(color[i] == 'r')
            fprintf(*fp,"\tx%d -> x%d[color=\"red\"]\n",first[i], second[i]);
        else if(color[i] == 'c')
            fprintf(*fp,"\tx%d -> x%d[color=\"blue\"]\n",first[i], second[i]);
        else
            fprintf(*fp,"\tx%d -> x%d[style=\"invis\"]\n",first[i], second[i]);
    }

    fprintf(*fp,"}");
    fclose(*fp);
    cerr << system("dot -Tpdf graph2.gv -o graph.pdf");
}

void printResolver::printGraph(warp* cur_warp){
    int branchCount = 0;
    int&   init                  = printHelp.init;
    int _pc                      = cur_warp->get_warppc();
    FILE**   fp                  = &printHelp.fp;
    vector<simt>& simt_stack     = cur_warp->get_simt_stack();

    if(init==0) initialize(fp, cur_warp);
    printHelp.current->insertLabel( printHelp.labelCount );

    int Stacksize = simt_stack.size();
    int PC2 = ((Stacksize <= 0) || (Stacksize > 64)) ? -1 : simt_stack[ Stacksize - 1 ].pc;
    if(PCNUM==1)
        printHelp.current->insertPC( _pc, 0 );
    else{
        printHelp.current->insertPC( _pc, PC2 );
    }

    branchCount += printInstruction( fp, cur_warp->get_cur_instruction(), _pc, 1);
    if(PCNUM==2) 
        branchCount += printInstruction(fp, cur_warp->get_simt_instruction(), PC2 , 2);
    if(branchCount == 0)
        printHelp.current->isBranch = false;
   
    instruction* tmp = cur_warp->get_simt_instruction();
    bool empty = (tmp!=NULL) && (printHelp.previous->enable);
    printStack(fp, simt_stack, printHelp.labelCount, empty);

   if(init==0){
       init++;
   }else{
       checkArrow(arrow, simt_stack, empty, tmp!=NULL&&tmp->get_opcode()==ST);
       printHelp.pushLabel(arrow, ARROW_LENGTH);
   }
   copySIMT(simt_stack, printHelp.prevSIMT, &printHelp.lenSIMT);
   printHelp.swapState();
}

int printResolver::printInstruction(FILE**fp, instruction* ins, int _pc, int num){
    printHelp.current->closeBranch(num - 1);
    if(num==1)
        fprintf(*fp,"\tx%d [group=\"pc\",shape=record,label=\"%d: ",printHelp.labelCount++,_pc);
    else if(_pc==-1)
        fprintf(*fp,"\"]\n\tx%d [group=\"pc\",shape=record,label=\"",printHelp.labelCount++);
    else if(ins==NULL)
        fprintf(*fp,"\"]\n\tx%d [group=\"pc\",shape=record,label=\"%d:",printHelp.labelCount++,_pc);
    else
        fprintf(*fp,"\"]\n\tx%d [group=\"pc\",shape=record,label=\"%d:",printHelp.labelCount++,_pc-1);
    if(ins == NULL){
        printHelp.current->setenable(false);
	return 0;
    }else 
        printHelp.current->setenable(true);
    int BRA_Target = label_name.find(ins->get_label())->second;
    fprintf(*fp, instruction::handler[ins->get_opcode()]->opername.c_str());
    if(ins->get_opcode()==BRA){
        fprintf(*fp," %d",BRA_Target);
        printHelp.current->insertBranch(num - 1, BRA_Target);
        return 1;
    }
    return 0;
}

void printResolver::printStack(FILE** fp, vector<simt>& simt_stack, int& labelCount, bool branchPC){
   int start = (branchPC) ? simt_stack.size()-1-END_SIMT : simt_stack.size()-1;
   fprintf(*fp,"\"]\n\tx%d [group=\"simt\"shape=record,label=\" ",labelCount++);
   for(int i=start; i>=0; i--)
       if(i != 0)
           fprintf(*fp,"%d | ",simt_stack[i].pc);
       else
           fprintf(*fp,"%d",simt_stack[i].pc);
   fprintf(*fp,"\"]\n");
}

void printResolver::checkArrow(bool arrow[], vector<simt>& simt_stack, bool BranchPC, bool stall){
   warpState* prev = printHelp.previous;
   warpState* curr = printHelp.current;
   char *colors    = printHelp.colors;
   for(int i=0;i<9;i++) colors[i] = 'w';
   if(PCNUM==1){
       arrow[0] = arrow[3] = true;
       arrow[1] = arrow[2] = false;
       if(prev->isBranch){
           arrow[1] = compareSIMT(prev->branch[0],simt_stack);
           arrow[2] = comparePC(curr->pc[0],printHelp.prevSIMT,printHelp.lenSIMT,0);
       }
       for(int i=0;i<9;i++) colors[i] = (arrow[i]) ? 'b' : 'w';
       colors[1] = (arrow[1]) ? 'r' : 'w';
       colors[2] = (arrow[2]) ? 'r' : 'w';
   }else{
       for(int i=0;i<8;i++) arrow[i] = false;
       arrow[8] = arrow[0] = arrow[4] =true;
       if(prev->isBranch){
           arrow[1] = (comparePC(prev->branch[0], &(curr->pc[1]),1,1) || comparePC(prev->pc[0], &(curr->pc[1]),1,2));
           arrow[2] = (compareSIMT(prev->pc[0],simt_stack) || compareSIMT(prev->branch[0],simt_stack));
           arrow[3] = !stall && BranchPC && (comparePC(prev->branch[1], &(curr->pc[0]),1,0) || comparePC(prev->pc[1], &(curr->pc[0]),1,0));
           arrow[5] = !stall && BranchPC && (compareSIMT(prev->pc[1],simt_stack) || compareSIMT(prev->branch[1],simt_stack));
           arrow[6] = comparePC(curr->pc[0],printHelp.prevSIMT,printHelp.lenSIMT,0);
           arrow[7] = comparePC(curr->pc[1]-1,printHelp.prevSIMT,printHelp.lenSIMT,0);
       }
       for(int i=0;i<=5;i++) colors[i] = (arrow[i]) ? 'r' : 'w';
       for(int i=6;i<=8;i++) colors[i] = (arrow[i]) ? 'c' : 'w';
       colors[0] = ((prev->pc[0])==curr->pc[0]-1 || (prev->branch[0])==curr->pc[0]) ? (((prev->branch[0])==curr->pc[0]) ? 'r' : 'b') : 'w';
       colors[4] = ((prev->pc[1])==curr->pc[1]-1 || (prev->branch[1])==curr->pc[1]-1) ? (((prev->branch[1])==curr->pc[1]-1) ? 'r' : 'b') : 'w' ;
       colors[4] = (!(BranchPC)) ? (((prev->branch[1])==curr->pc[1] && ((int)curr->pc[1]) >0) ? 'r' : 'w') : colors[4];
       colors[4] = (stall) ? 'b' : colors[4];
       colors[8] = (arrow[8]) ? 'b' : 'w';
   }
}

void printResolver::copySIMT(vector<simt>& SIMT, uint32_t* s, int*length){
    int offset = (PCNUM==1) ? 0 : 1;
    *length = (int)(SIMT.size()) - offset;
    for(int i=0;i<*length;i++)
        s[i] = SIMT[i].pc;
}

int printResolver::compareSIMT(unsigned _pc, vector<simt>& SIMT){
    int offset = (PCNUM==1) ? 0 : 1;
    int end = ((int)(SIMT.size())) - offset;
    for(int idx=0;idx<end;idx++){
        if(SIMT[idx].pc == _pc){
            return 1;
        }
    }
    return 0;
}

/*int printResolver::compareSIMT(unsigned _pc, vector<simt>& SIMT,bool PC){
    int offset = (PCNUM==1 || !PC) ? 0 : 1;
    int end = ((int)(SIMT.size())) - offset;
    printf("offset = %d (size = %d)\n",offset,end);
    for(int idx=0;idx<end;idx++){
        if(SIMT[idx].pc == _pc){
            return 1;
        }
    }
    return 0;
}*/

int printResolver::comparePC(unsigned _pc, unsigned *s, int len, int offset){
    //std::cout << _pc << " "  << "( " << printHelp.labelCount << " " << printHelp.previous->isBranch << " )" << std::endl;
    for(int idx=0;idx<len;idx++){
        if(s[idx]-offset == _pc){
            return 1;
        }
    }
    return 0;
}

