all:
	@make -C arch
	@make -C libcuda

clean:
	@make -C arch clean
	@make -C libcuda clean
